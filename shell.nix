with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "node";
  buildInputs = [
    hugo
  ];
  shellHook = ''
        echo "We now are in a shell that has hugo installed:"
        hugo version
    '';
}
