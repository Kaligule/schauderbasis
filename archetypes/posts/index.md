---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: false
image: featured.jpeg
---

<!-- image link: ??? -->
<!-- image author: ??? -->

Aliquam erat volutpat. Nunc eleifend leo vitae magna. In id erat non
orci commodo lobortis. Proin neque massa, cursus ut, gravida ut,
lobortis eget, lacus. Sed diam. Praesent fermentum tempor tellus.
Nullam tempus. Mauris ac felis vel velit tristique imperdiet. Donec at
pede. Etiam vel neque nec dui dignissim bibendum. Vivamus id enim.
Phasellus neque orci, porta a, aliquet quis, semper a, massa.
Phasellus purus. Pellentesque tristique imperdiet tortor. Nam euismod
tellus id erat.

The image once again:

![something](featured.jpeg)
