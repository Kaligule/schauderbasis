---
title: "Annoyed with Sir Arthur Conan Doyle"
image: silhouette.jpg
---

<!-- Image: -->
<!-- Craig Whitehead -->
<!-- source: https://unsplash.com/photos/lbekri_riMg -->
<!-- license: unsplash -->
<!-- cropped by me -->

My wife and I read stories to each other.
It is a great way to go to sleep.
At some point we started to do the Sherlock Holmes books by Sir Arthur Conan Doyle.
A faithful translation of the originals.

The books are fun.

- The mysteries are interesting.
- Having read the originals (or a good translation) shines a new light on the many many adaptations that we are out there.
  A lot of them play with the original texts.
- The moments where Holmes goes "I know everything about that person just from having a close look" are always fun.
- Sherlock Holmes is in public domain.
  This gives me a feeling of opportunity - I could do everything with the material.
  (It is more of a theoretical opportunity though, I am not in the business of adapting literary figures.)

It is clear that crime novels have come a long way since 1887.
- Some of the situations/conversation feel a bit set up.
  Watson is constantly super-surprised about Holmes deductions.
  One would expect that he would learn to expect them (even when he can't do them himself).
- Some of the solutions feel trivial.
  This is probably unfair against those stories,
  but by today's standards I expect a clever and complex solution to a mysterious problem.
  "It was some guy who wanted the money, so he just did the next best thing" is just not interesting.
- Some ideas have been reiterated on so often that they feel old and predictable.
  Once again, this is not the originals fault, more of an achievement.

But there is one thing about the story structure that really annoyed us.

It happens at the end of some cases.
The culprit has been found and arrested, it is more or less clear what happened.
Holmes has had all his moments of cleverness.
Now all we need is something to conclude the story.

But then the culprit gets a backstory.
And that backstory is told in very much detail.
Either by himself or just by the narrator.
That's fine and all, but it doesn't add to the action.
It is just another plot, nearly completely unrelated to what the book is about.

Examples would be:
- Chapter 12 of ["The Sign of the Four"](https://www.gutenberg.org/ebooks/2097) (1887)
- Chapter 1 to 5 of the second part of ["A Study in Scarlet"](https://www.gutenberg.org/ebooks/244) (1890)

(I will admit that this seems to be only a problem of the earlier books.)

It feels like the author had thought of this backstory for the baddie
and - when he finished writing - noticed that he did never use it in the tale.
So instead of working it into the text he just pasted it in at the end, like an appendix.

This takes away from the books flow, and it makes me sad because I like them a lot.

It's fine though, the later books don't have the problem (or at least to a lesser extend).
