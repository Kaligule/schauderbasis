---
title: "RSS Software"
---

> __tldr:__ RSS gehört für mich zu den wichtigsten Bausteinen des Internets.
> Ich finde jeder sollte davon wissen.
> Es gibt große Auswahl an guter Software.

Eine Website besteht aus zwei Teilen:
Der __Inhalt__ _(RSS)_ und das __Design__ _(CSS)_.
Bei vielen Websiten interessiert mich nur der Inhalt und eigentlich auch nur,
wenn etwas neues dazu kommt.
Bei Blogs zum Beispiel.

Ein RSS-Reader ist wie eine Websiten-Überwachungsstation,
die Websiten beobachtet,
schaut was sich verändert und mir das wichtige (den neuen Inhalt) zuliefert.
Sehr praktisch ist das zum Beispiel für

* __Blogs,__ weil da selten neue Artikel auftauchen, die dann aber meist sehr interessant sind
* __Nachrichtenseiten,__ weil die sich permanent ändern und man per RSS nur den Inhalt übersichtlich aufgelistet bekommt
* __totgesglaubte Seiten,__ weil man sie ohne Mehraufwand weiterverfolgen kann und ein plötzliches wiederaufleben sofort mitbekommt
* __Podcasts,__ denn das sind (technisch gesehen) Audioblogs, die sich sehr ähnlich verhalten
* __Social Networks__ die einen Newsstream anbieten, den man oft auch per RSS abonieren kann (zB: Github, App.net, reddit)

## Workflow

* Ich finde eine interessante Website im Internet und aboniere sie.
  Ab jetzt beobachtet mein Feedreader die Website.
* Ein Autor schreibt einen Artikel auf der Website.
* Mein Feedreader erkennt den neuen Inhalt,
  befreit ihn von dem Websitedesign (so gut wie möglich) und legt ihn zum lesen bereit
* Ich öffne den RSS-Client auf meinem Telefon oder Rechner.
  Er fragt meinen Feedreader was es neues gibt und bekommt einen Haufen neuer Artikel.
* Ich lese die neuen Artikel von verschiedenen Websiten, alle in einem Programm gebündelt.

# Feed Reader

Die Websiten-Überwachungsstation.

### Früher...

Es gab eine Zeit, da gab es in erster Linie einen Feed Reader:
Den [Google Reader](https://www.google.com/reader/about/).
Er war wirklich toll und fast jeder hat ihn benutzt.
Kaum ein Client hat etwas anderen unterstützt, wozu auch?
Doch dann hat Google den Reader eingestampft und das war ein echter Schock damals.
Keiner wusste wirklich warum und niemand wusste, was man stat dessen benutzen sollte.

### Heute...

Im Nachhinein betrachtet hat es dem Ökosystem aber gut getan.
Viele neue Dienste wurden aus dem Boden gestampft oder aus der Versenkung hervorgehohlt.
Besonders auffällig waren dabei (nach meiner Zuneigung sortiert)

* __[tiny tiny RSS](https://tt-rss.org/):__ (kurz: ttrss)
  Open Source, muss man selbst hosten, Multiuser Support, konfigurierbar, langsame aber stabile Entwicklung - benutze ich selbst und kann es nur empfehlen ![ttrss im Browser mit dem Google-Reader Theme](ttrss-1.png)
* __[Fever](https://feedafever.com/):__
  einmal kaufen, für immer besitzen, selbst hosten, fancy Features...
  der Entwickler antwortet leider nicht auf Emails, also hab ichs nicht weiter getestet
* __[Feed Wrangler](https://feedwrangler.net/):__
  19$/Jahr, ich habe nur Gutes gehört
* __[Feedbin](https://feedbin.com/):__
  30$/Jahr, sieht ganz schnuffig aus, kann aber nicht viel dazu sagen
* __[Feedly](https://feedly.com/):__
  freemium, sehr viele User, schlechtes Userinterface, da war ich schnell wieder weg.
  Vielleicht sind sie inzwischen besser geworden.

Die werden inzwischen von allen gängigen Rss-Clients unterstützt,
die Ausnahme ist leider ausgerechnet ttrss.
Deswegen hat sich jemand ein [Plugin](https://github.com/dasmurphy/tinytinyrss-fever-plugin) ausgedacht,
dass die Fever-Api nachimplementiert.
Das funktioniert hervorragend und deswegen kann man auch als Opensourceler alle schicken Clients voll ausnutzen.
Eine gute Anleitung für das Plugin findet man zum Beispiel [hier](https://blog.renem.net/2013/07/24/mr-reader-and-reeder-sprechen-mit-tt-rss/).

# Clients

### Verbingung zur Überwachungsstation

Ich lese meine Feeds zu 95% auf dem Telefon
(Es geht zur Not auch auf der Website des Feedreaders).
Da der Client das Programm ist, das ich letztlich benutzte (oft mehrmals täglich) und auf dem ich auch lange Artikel lese zahlt sich hier gut designte Software besonders aus.

### Getestet und für gut befunden

...
habe ich diese drei:

* __[Reeder](https://www.reederapp.com/ios/):__
  für iOS, einfach, schick, schnell zu bedienen.
  Diese App benutze ich jeden Tag am häufigsten.
* __[unread](https://jaredsinclair.com/unread/):__
  für iOS, fancy und schick, von einem [Hardcoredesigner](https://blog.jaredsinclair.com/) gemacht.
  Unread fühlt sich irendwie undgewöhlich an.
* __[Reeder](https://www.reederapp.com/mac/):__
  für OSX, mit der Tastatur superschnell bedienbar und angenehm ![extrem schick und leicht zu bedienen. Wenn nur jeder Desktop Client so wäre wie Reeder...](ReederOSX-1.png)

Sie alle funktionieren mit den oben genannten Feedreadern.

### Weitere Clients

... für verschiedene Betriebssysteme:


* __[Liferea](https://lzone.de/liferea/):__
  für Linux, mit GUI. Da scheint es mir wenig gute Alternativen zu geben.
  Mir gefällt das Design nur so mittel und ich habe noch nicht rausgefunden wie ich es ändern kann,
  aber eigentlich tut der schon das Richtige.
  ![Liferea](Liferea-1.png)
* __[der offizielle ttrss-Client](https://play.google.com/store/apps/details?id=org.fox.ttrss)__
  für Android,
* __[Mr Reader](https://www.curioustimes.de/mrreader/):__
  fürs iPad, mit netten Themes und einer ordentlichen Website.
  Ich kenne Leute die sehr zufrieden damit sind.
  Sieht gut aus.
* __[Press](https://twentyfivesquares.com/press/):__
  für Android, der Till benutzt das und ist zufrieden
* __[ReadKit](https://readkitapp.com/):__
  für OSX, Client für alle möglichen Dienste (unter anderem Pocket und eben Fever), ganz nett und eigentlich auch zuverlässig, offline nur mittelmäßig, irgendwie konnte ich mich nie so richtig damit anfreunden
  ![Leider kann man mit ReadKit offline nichts in Pocket reinschieben.](ReadKit.png)

# Fazit

Mein Leben hat sich durch RSS ernsthaft vereinfacht.
Ich bin besser informiert und muss weniger Aufwand treiben.
Wenn jemand das auch mal ausprobieren möchte und Hilfe braucht kann er/sie mich gerne anschreiben.
