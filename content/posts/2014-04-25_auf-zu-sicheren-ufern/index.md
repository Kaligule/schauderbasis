---
title: "Auf zu sicheren Ufern"
---

Es wird Zeit, proprietärer Software den Rücken zuzukehren. Endlich. Die Gründe dafür sind einfach:

* __Sicherheit:__ Seit den Snowdenleaks (spätestens) sollte klar sein, warum man Software nicht mehr trauen kann, deren Quellcode man nicht sehen darf. Zeit, ein bisschen Courage zu zeigen.
* __Linux:__ Das Linux Universum hat durchaus seine Reize. Paketmanagment, Freaksoftware und irgendwie waren die coolen Leute auch schon immer irgendwie bei Linux.
* __Proof of Concept:__ Ich bin jung, halbwegs intelligent und beschäftige mich gerne mit Computern. Wenn ich das nicht schaffe, ist Linux für den Otto-Normaluser wohl noch nicht reif.

Welche Bereiche meines digitalen Lebens betrifft das nun? Wie sich herausstellt, beinahe alle:

* __Webdienste:__ Google, Dropbox, iCloud, Facebook…
* __Kommunikation:__ Verschlüsseln. Und selber hosten. Und ganz viel verschlüsseln.
* __Handy:__ kein iPhone mehr, alles wird anders
* __Desktop-PC:__ Linux. Beinahe alle Workflows, die ich bisher benutzt habe werden neu überdacht werden müssen…

Das kann man natürlich nicht alles auf einmal lösen. Aber vielleicht Schritt für Schritt. Und darum soll es hier gehen.