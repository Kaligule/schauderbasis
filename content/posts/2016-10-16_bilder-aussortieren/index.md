---
title: "Bilder aussortieren"
image: bilder_sortieren.jpg
---

<!-- Bilder von [hier](https://pixabay.com/en/photo-photographer-old-photos-256887/) und [hier](https://pixabay.com/en/photo-album-photographer-old-256889/). -->

Es gab eine Feier, jemand hat Fotos gemacht und rumgeschickt. Oder vielleicht habe ich selbst hundert mal den selbe Essen fotografiert. Jedenfalls sind da ein Haufen Bilder und ich will nur die guten behalten.

Dieses Aussortieren macht wenig Spaß und soll deshalb schnell gehen. Wenn man es gut macht hat man danach nur noch 5-10% der Bilder. Der Lohn: Man kann den wenigen guten Bildern viel mehr Beachtung schenken.

Erstaunlicherweise bietet der einfachste (und schnellste) mir bekannte Imageviewer dafür das beste Interface bereit: ```feh```

## Vorbereitung

Wir bilden den Sortierprozess auf das Filesystem ab. Wärend dem sortieren haben wir 3 Haufen von Fotos:

* die noch unsortierten Fotos (```~/Pictures/feier```)
* die guten Fotos (```~/Pictures/feier/good```)
* die schlechten Fotos (```~/Pictures/feier/bad```)

```bash
mkdir ~/Pictures/feier
cd ~/Pictures/feier
mv ~/Downloads/feier.zip .
mkdir good bad
unzip feier.zip
```

Wir werden die schlechten Bilder erst wegwerfen wenn wir mit dem sortieren ganz fertig sind (so vermeiden wir unglückliche Versehen). 

## Sortieren

Hier steckt die ganze Magie drin:

```
feh --auto-zoom --scale-down --action1 "mv %f good/" --action2 "mv %f bad/" .
```

Übersetzt bedeutet das: "Zeige alle Bilder im aktuellen Verzeichnis (```.```) nacheinander (als Slideshow) an, so gezoomt das man immer das ganze Foto sehen kann. Wenn ich die Taste ```1``` drücke, verschiebe das aktuelle Bild ins Verzeichnis ```good```, wenn ich ```2``` rücke verschiebe es nach ```bad```."

Nette Annehmlichkeiten sind dabei:

* Wenn man gerade nicht sicher ist, kann man mit den Pfeiltasten einfach zum nächsten Bild springen und die Entscheidung verschieben.
* Ein Bild, bei dem man eine Wahl getroffen hat wird nicht mehr angezeigt. Sind keine Bilder mehr übrig, wird das Programm beendet
* Will man unterbrechen, hilft die Taste ```q```, was man bisher sortiert hat bleibt auch sortiert.
* feh ist zwar schlank und klein, aber zum zoomen und Bilder richtig herum drehen reicht es noch.
* Wie die meisten Unix-workflows ist auch dieser sehr flexibel. Ideen, die man damit schnell verwirklichen könnte:
  * einen dritten Ordner (```bin_nicht_sicher```)
  * mehrere Sortierdurchläufe.
  * Photos taggen
* Das Interface ist so einfach wie nur irgend möglich: Man sieht nur das Bild und hat genau 2 Tasten für 2 Möglichkeiten.
* Es geht schnell. Wenn man mal ein bisschen drin ist hat man schnell einige Hundert Bilder sortiert.

![](bilder_sortieren_2.jpg)

## Nachbereitung

Zum Schluss (und wenn man sich sicher ist) wirft man die schlechten Fotos weg und behält die guten:

```
rm bad/* -f
mv good/* .
rmdir good bad
```
