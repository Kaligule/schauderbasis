---
title: "Blogging on mobile"
image: write.jpg
---

<!---
Source: https://unsplash.com/photos/gzz1RJXibKM
Photographer: Giorgio Trovato
License: Unsplash
-->

I want to write and publish blog posts from my mobile (which is running Android).

#  what is needed

Since this blog is being generated using a SSG in a CI from markdown files which life in a repo, this means I need to do the following:

1. Clone a private repo from gitlab
2. Add markdown/image files to the existing ones and edit them.
3.  `git add` those files to the repo and `git push` it to origin. 

The CI will do the  building and publishing itself then. 

I want to use only free software if possible.
Especially I don't want to use any google services like the Playstore. 

Let's go through the points inorder:

## 1. Clone a (private) repo from gitlab

I was optimistic that there would be a capable git client on fdroid, but I couldn't find anything suitable.
There are some vendor specific clients (like [GitNex for Gitea](https://f-droid.org/packages/org.mian.gitnex/))
and [git-touch](https://github.com/git-touch/git-touch) looked promising (but turned out to be a tool for managing accounts, not repos)

So I went for tmux (basically a terminal with a shell for Android) and installed git by hand.
It doesn't feel very polished for mobile but at least it is a shell and I can do what I want there. 

To authenticate against gitlab I created a personal access token with the scope "read_repository" and "write_repository".
Then I could use special [gitlab-magic-syntax](https://stackoverflow.com/a/47471056) to use this token in the branches path:

```
git clone https://gitlab-ci-token:{TOKEN}@gitlab.com/Kaligule/schauderbasis.git
```

There I had my repo.

## 2. Add images and markdown files

There are plenty of useful image-/markdown-editors on fdroid.
[Notes](https://billthefarmer.github.io/notes) seems to fit my usecase very well.

I had some difficulties getting the paths right in tmux.
I chose to blame android and java for this.
[Long story](https://android.stackexchange.com/questions/166538/where-is-the-folder-that-termux-defaults-to#185949) short: I had to call

```termux-setup-storage```

in termux and move the repo into what android seems to use instead of a home directory.
Strange.
But now all the apps can agree on where the repo is. 

## 3. Perform a `git add` and `git push`

We basically did all the work in step 1.
Just fire up tmux and type.
I didn't find an app that could even do these simple tasks.

![screenshot](screenshot.png)

## this post is a proof of concept

Literally.
I did this whole post on my mobile.

## it doesn't feel great

Honestly, it works but it still sucks.
Working with a touch keyboard as the main driver on every step is no fun.
But at least everything is under control and I can replace the tools until I am happy with them.