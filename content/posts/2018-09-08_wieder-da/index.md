---
title: "Wieder da"
image: dog_bark.jpg
---

<!--
credit: Robert Gramner
creditlink: https://unsplash.com/photos/N6YQfPn_9N4
-->

Ich bin wieder da. Der Blog hat lange geruht und jetzt habe ich Lust,
wieder zu schreiben.

Der Blog war auch und vor allem deshalb fort, weil wir unseren
geliebten Server Gipfelsturm verloren haben. Da sind auch Sandras und
mein Ghostblog drauf gewesen und ich wollte erst wieder schreiben,
wenn ich beide Blogs wieder so weit habe, dass sie benutzt werden
können (Es wäre unfair gewesen, selbst weiter zu bloggen bevor ihr
Blog wieder läuft).

Dazu musste ich aber aus schlechten Backups die meisten Artikel wieder
herzaubern (viel habe ich von archive.org und dem google-cache
gezogen, Viele Bilder hatten wir noch und einige konnten wir wieder
zuordnen) und eine ganz neue Bloginfrastrucktur aufbauen. Das war eine
lange Reise, aber jetzt bin ich ganz zufrieden. Vielleicht schreibe
ich später mehr zu den Details. Die Hauptsache ist, dass ich jetzt
große Kontrolle über den Blog habe (wenn was nicht geht kann ich es
wahrscheinlich reparieren) und es trotzdem einfach ist, zu bloggen.

Es läuft wieder was in der Schauderbasis.

PS: Der RSS-Feed funktioniert auch: https://schauderbasis.de/index.xml
