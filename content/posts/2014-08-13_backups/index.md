---
draft: true
title: "Backups"
---

Wer einen Computer regelmäßig benutzt, sollte Backups machen. Wie macht man Backups?

## Ausstattung

Mein Laptop (Linux) und der meiner Freundin (Mac) sollen beide gebackupt werden. Dazu habe ich als NAS (eine Festplatte, die man übers Netz erreichen kann) ein QNAP TS-112 mit genügend Speicherplatz. Das hat eine GUI, die man über den Browser erreicht und die versucht, wie ein Betriebsystem auszusehen.

Dazu eine Wohnung mit Internet in der Luft, in der sich beide Laptops regelmäßig, aber nicht jederzeit aufhalten.

## Anforderung

Mir ist wichtig dass keine Daten verloren gehen. Systeme und Einstellungen für Apps oder ähnliches kann man leichter wieder herstellen (und meistens ist alles schöner und schlanker, wenn man es mal neu gemacht hat).

## Zuerst der Mac

Am Mac laufen Backups mit TimeMachine. Alles andere wäre unnötig aufwendig. Die TimeMachine ist integriert und läuft auf allen Festplatten, die der Mac so sieht. Auf dem QNAP ist es auch schon vorangelegt, so das man wenig falsch machen kann.

Nach wenigen relativ selbsterklärenden Schritten funktioniert alles wie gewollt.

## Linux, was will ich genau sichern?

Welche Daten will ich auf jeden Fall sichern? Eigentlich sind auf dem Laptop nur drei Arten von Dingen, die es sich zu erhalten lohnt:

* __die Liste meiner installierten Pakete,__ denn die Pakete selbst kann ich ja jederzeit wieder herunterladen. Die Liste bekomme ich einfach mit ```yum list installed```
* __config files__ an denen ich lange gearbeitet habe. Das betrifft in erster Linie die i3_config, aber auch einige andere. Diese Arbeit möchte ich nicht nochmal machen müssen
* __mein Homeverzeichnis__ mit allem, was da drinnen liegt:
	* Bilder
    * Videos
    * Code den ich mal geschrieben habe
    * meine Passwortdatenbank
    * Dateien in der ownCloud
    * alles was man so wegspeichert

Der letzte Punkt ist offensichtlich der wichtigste, aber wahrscheinlich auch der schwierigste.

Fangen wir klein an.

### Die Liste der installierten Pakete

Da kann man sich ja noch selbst eine Lösung ausdenken. Ich richte einen Cronjob ein, der die fragliche Liste einmal am Tag (um 18:00) in meine ownCloud legt. Er sieht so aus:

```
00 18 * * * yum list installed > ~/ownCloud/backup/paketliste.txt
```

Über Cronjobs müssen wir später ohnehin nochmal reden.

Im Moment ist mir nur wichtig, dass die Liste jetzt in meinem home liegt und somit (wenn ich die restlichen Punkte abgearbeitet habe) auch gesichert wird. Erledigt.

### meine config files

Weil die ohnehin Open Source sein sollen sind die einfach auf Github, das ist mir Datensicherung genug. [Ich hab das neulich schon verbloggt.](https://www.schauderbasis.de/meine-configs/)

### mein home

Schwierig. Um ehrlich zu sein, ich habs noch nicht gelößt. Teil der Lösung könnte sein:

* rsync
* cronjobs
* irgendeines von den tausen programmen die man findet wenn man ```yum search backup``` eingibt.

