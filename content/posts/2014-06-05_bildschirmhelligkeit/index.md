---
title: "Bildschirmhelligkeit"
---

(Alles was in diesem Eintrag steht habe ich übrigends von Jonathan Krebs, danke.)

__Aufgabenstellung:__ Wir wollen die Bildschirmhelligkeit einstellen und dabei nur das Terminal verwenden.

## Einfach

Das Programm xbacklight macht alles was wir wollen. Die Helligkeit wird in Prozent gemessen. Wir können sie abfragen:

```
$ xbacklight -get
70
```

Und natürlich kann man den Wert selbst festlegen:

```
$ xbacklight -set 80
```

Mehr will man ja meistens garnicht.

__Nachteil:__ Die Helligkeitsstufen sind recht grob. Außerdem ist die niedrigste Helligkeit immer noch recht hell.

## Schwer

Es geht auch über das Filesystem:

```
$ cd /sys/class/backlight/intel_backlight/
```

Wer den Pfad nicht findet kann es mal mit ```find``` versuchen. Bei mir sah das so aus:

```
$ find -name 'backlight' 2>/dev/null~
./var/lib/systemd/backlight
./sys/devices/pci0000:00/0000:00:02.0/backlight
./sys/class/backlight
./usr/lib/modules/3.14.2-200.fc20.x86_64/kernel/drivers/video/backlight
./usr/lib/modules/3.11.10-301.fc20.x86_64/kernel/drivers/video/backlight
./usr/lib/modules/3.13.10-200.fc20.x86_64/kernel/drivers/video/backlight
```

Irgendeiner der Ordner wird schon der sein, den wir brauchen, bei mir wars das dritte Ergebnis (ausprobieren).

Hier angekommen schauen wir uns um (```ls -l```) und sehen 2 wichtige Dateien:

* __max\_brightness__ Hier steht genau eine Zahl drin (bei mir 851). Die Datei ist read only, auch für den root, hier kann man also nichts machen. Da es soetwas wie _min_brightness_ nicht gibt kann man annehmen, das die Untergerenze 0 sein wird.
* __brightness__ Die Zahl die hier drin steht bestimmt die Helligkeit. Heureka! Wir können sie als superuser leicht ändern.

```
$ su
Password: 
# echo 400 > brightness 
```

Die Helligkeit ändert sich sofot. Wunderbar.
Und wenn wir mit xbacklight die Helligkeit neu einstellen, ändert sich die Zahl. Alles macht Sinn.

__Achtung:__ Wer glaubt, er muss die brightness hier einfach auf 0 setzten sollte sich _vorher_ überlegen, ob er das wieder rückgängig machen kann wenn der Bildschirm völlig schwarz ist.

# Verspielt

Wir schreiben in kurzes Python Skript (sinus.py) und legen es auf dem Desktop ab.

```
#!/usr/bin/python
# Gibt alle 0.05 Sekunden einen Wert auf einer Sinuskurve aus.
import math
import time, sys

# max_brightness
Max=851

while True:
	s=math.sin(time.time())
	print(int((s*Max/2)+Max/2))
	sys.stdout.flush()
```

Dann gehen wir zurück zu unseren magischen Dateien _brightness_ und _max_brightness_ und führen als superuser aus:

```
python /home/johannes/Desktop/sinus.py  > brightness 
```

__Ergebnis__: Die Helligkeit oszilliert mit einer Periode von etwa 6 Sekunden (Für die Besserwisser: 2*pi). Wenn man das Programm mit ```Str```+```C``` abbricht, bleibt die Helligkeit wie sie gerade ist.

Und mal ganz ehrlich: Das ist es doch was wir die ganze Zeit wollten.