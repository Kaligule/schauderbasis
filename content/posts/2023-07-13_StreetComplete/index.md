---
title: "Trying out StreetComplete"
image: map.png
---

The other day I found that [StreetComplete](https://f-droid.org/packages/de.westnordost.streetcomplete/) is on F-Droid.
I had heard of that app when it came out but had totally forgotten about it.
Time to give it a try.

## How it works

The goal is to go outside and add information to OpenStreetMap from your phone.

The app is probably the most modern app on my phone, it looks like it anyway.
You need to login with your OpenStreetMap account (I barely remembered having one).
Then you get a map and select an area where you would like to do quests.
Those are scattered around in the area and you can choose which one to take.
(You can also specify which kinds of quests you would like to do.
Personally I don't like to decide on types of houses and don't want to measure the width of roads, so I don't.)


After choosing one you get a simple question with some answers to choose from.
Sometimes you are asked to enter some text (a house number or the name of a shop).
But mostly the questions are multiple choice, with illustrated answers.
This makes deciding on the right answer *much* easier.

![A quest with illustrated answers to choose from.](quest.png)

Common questions in my area (gnihihi) are:

- Is there a sidewalk on that street?
- What kind of surface is here?
- Is there a bench/trashcan/streetlight at this bus stop?
- Is that sign still there?

Once you have answered the question the answer is directly uploaded to OpenStreetMap.
You can even do it without mobile data!
Then the answers are uploaded when you have WiFi next time.
Awesome.

![A simple yes or no question: Is there a bench at this bus stop?](yes_or_no.png)

## How it feels

It reminds me of those augmented reality games
([Pokemon Go](https://en.wikipedia.org/wiki/Pokemon_Go) from 2016,
[Ingress](https://en.wikipedia.org/wiki/Ingress_(video_game)) from 2014)
that were very in a few years ago.
I never played those, but I imagine they felt similar to this.

Everything about the app is fun.
Answering simple questions is fun,
getting outside to clear your area from quests is fun,
walking into random streets for some quest is fun.
The kids like it, too.
We were having a lot of fun with it.

Once you uploaded your answers you can have a look on [OpenStreetMap](https://www.openstreetmap.org) and see them in the map right away.
This in turn made me do some editing on the Map in the browser as well.
OpenStreetMap's Browser Editor (called [_iD_](https://wiki.openstreetmap.org/wiki/ID)) is really good these days,
and so is the [wiki](https://wiki.openstreetmap.org/wiki/Key:bollard?uselang=en) where all the tags are explained in detail.
And iD will even link those wikipages on within the editor so you can look them up effortless.

When I started contributing to OpenStreetMap I often was afraid that I would label something wrong and ruin the map for everyone.
But in StreetComplete the quests are well chosen.
The questions are direct and the options are clear, so you can answer confidently.
And if you think you did something wrong anyways you can undo your answer.

![an undo diaolg being specific about what will be undone](undo.png)

## Conclusion

After 3 days with StreetComplete I now have cleaned up the small area where I commute regularly.
Some tasks can even be done from the bus, if you are quick.
On occasion I will have to explore other areas.

StreetComplete does a good job at lowering the barrier of entry for mappers.
I love it.
