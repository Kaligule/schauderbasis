---
title: "Meine Configs"
---

Viele Programme besonders unter Unix kann man sogenannten configs ("Konfigurationsdateien") tunen. Es sind einfache Textfiles, in die man Anweisungen für das Programm aufschreibt und die man dann an eine bestimmte stelle im Dateisystem legt, wo sie das Programm dann auch findet.

Es gibt ein paar Dinge, die einem recht schnell klar werden wenn man sich mit configs beschäftigt:

* In einer config kann sehr viel Arbeit drin stecken.
* Wenn man sich mit dem Programm noch nicht auskennt ist es oft nützlich Beispielconfigs zu haben.
* Die Syntax ist nicht einheitlich festgelegt. Zumindest kann man _meistens_ mit ```#``` auskommentieren.
* configs können beliebig komplex und kompliziert werden.
* Bei manchen Programmen sind sehr nützliche Beispielconfigs gleich mitgeliefert.
* Es ist nicht immer klar, wo die config zu einem Programm liegen soll. Oft (naja, eher ab und zu) liegt sie unter ~/.config/_programmname_ oder direct im home. Dann heißt sie oft ._programmname_rc (das rc steht [angeblich](https://kb.iu.edu/d/abdr) für "run commands", aber das halte ich für keinen guten Namen).

Wer ein bestimmtes Programm oft verwenden will hat wahrscheinlich einen Vorteil, wenn er ein bisschen in dessen config rumspielt.

## Beispiel!

Meine bashrc (die die Kommandozeile an meine Bedürfnisse anpasst) sieht im Moment so aus:

```
# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions

# ls: als detailierte Liste mit Indikatoren (*/=>@|) anzeigen
alias ls='ls -F -l'

# bei rm und mv immer nachfragen, bevor man was kaput macht
alias rm='rm -i'
alias mv='mv -i'

# bei less soll die tablaenge immer 4 sein
alias less='less --tabs=4'

# gramps soll auf Deutsch laufen
alias gramps='LANG=de_DE.UTF-8 gramps'

# dump .o and .hi files in an extra directory
alias ghc='ghc -outputdir=ghc_outputdir'

# rm .o and .hi files
alias ghcleanup='rm *.o *.hi'

# very simple promt.
export PS1="\[\e[00;31m\]\W\[\e[0m\]\[\e[00;37m\] \[\e[0m\]\[\e[00;31m\]\\$\[\e[0m\]\[\e[00;37m\] \[\e[0m\]"

# my editor
export EDITOR="subl -n -w"

```


## Versionskontrolle für configs

Mir ist relativ schnell bewusst geworden, dass ich meine configs mit git versionieren möchte. Deswegen liegt neben jeder config, mit der ich mich beschäfftigt habe eine .git Datei. (Wer sich mit git nicht auskennt, sollte sich mal damit beschäftigen. Wirklich)

Das hat sich für mich schon das eine oder andere mal ausgezahlt, wie es halt mit Verionskontrolle so ist.

## Freiheit für configfiles!

Ich finde, dass noch zu wenig configs im Internet stehen. Oft einmal hätte ich das eine oder andere Beispiel brauchen können und habe keines gefunden.

Deshalb stelle ich alle meine configs öffentlich ins Netz und zwar [auf Github](https://github.com/Kaligule).

# Configs auf Github

## Warum github?

Dafür gibt es ein paar einfache Gründe:

* Ich versioniere mit git
* Ich mag die Website von Github
* Man bekommt dort beliebig viele öffentliche Repositorys

## Wie macht man das?

Es ist wirklich einfach, Github selbst liefert eine gute Anleitung (siehe Bild).

![add remote](remote.png)

Ich schreibe die Schritte hier trotzdem nochmal übersichtlich auf.

### config auf Github hochladen

* Schreibe und bearbeite ein configfile, hier als Beispiel die _.bashrc_
* Versioniere es mit git
* Lege ein Konto auf Github an. Wenn es ok für dich ist, dass dein Code öffentlich ist reicht ein kostenloses Konto absolut aus.
![Neues Repo](CreateRepo.png)
* Erstelle auf Github ein neues Repository (idealerweise gleich mit einer freien Lizens)
* Füge mit
```
git remote add origin [PfadZuDeinemRepository]
```
dein neues Repository zu git hinzu.

* Schiebe deinen aktuellen Stand zum Remote mit
```
git push origin master
```
oder (noch kürzer) mit
```
git push
```
zu Github.

* Gib deinen Username und dein Passwort an.

### configs auf Github aktualisieren

Wenn man später nocheinmal etwas an seiner Config ändert, so comittet man mit git und führt dannach noch einmal den Befehl

```
git push
```
aus. Fertig.

## Wo liegen die configs denn jetzt?

[https://github.com/USERNAME](https://github.com/Kaligule?tab=repositories)

## Ist die Welt dadurch besser geworden?

Ja, denn jetzt sind die configs...

* öffentlich einsehbar für alle, die sich gerade damit herumschlagen. Diesen Menschen hast du geholfen.
* auf Github vor Datenverlust geschützt. Wenn der Computer abschmiert kannst du sie dir einfach wieder hersyncen, selbst wenn du sie sonst nicht gebackupt hast.
* syncron auf allen deinen Rechnern und Geräten, denn du kannst, stat alles mehrmals zu schreiben einfach deine configs dorthin _pull_en, wo du sie hinhaben willst.

