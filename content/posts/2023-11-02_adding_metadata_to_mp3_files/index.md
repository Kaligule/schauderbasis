---
title: "adding metadata to mp3 files"
draft: yes
---

I am currently adding metadata to a lot of music files.
Often they are grouped in a directory, based on their album.

```
arty artist
├── first album
│  ├── 01 some title.mp3
│  ├── 02 other title.mp3
│  └── 03 third title.mp3
└── songs for cooking
   ├── 01 Soßenlöffel.mp3
   └── 02 Küchentüre.mp3
```

(I was young when CDs were they way to acquire music.
I don't even know if todays music is still released in albums.)

But often these files have no metadata attached to them,
so I need to read the data from the file's path and write them into the metadata.
The path `arty artist/songs for cooking/01 Soßenlöffel.mp3` should translate into the following metadata:


| field        | value             |
|--------------|-------------------|
| artist       | arty artist       |
| album        | songs for cooking |
| track number | 01                |
| title        | Soßenlöffel       |

As seen in this example, we will have spaces and umlauts in our paths
(as well as apostrophes and other nasty characters).

If my music collection was more consistently named I would just write a script for all of this.
But it is not:
Sometimes there are track numbers,
some songs do not belong to any album and
sometimes the song is a collaboration of different artists.

So I had to semi-automate:
I wrote a few commands that would do most of the work for me, but I applied them individually where I saw fit.
This required more manual work but resulted in more consistence and correctness I believe.

The core of the following were two programs that allow setting metadata to audiofiles:
- [mp3info](https://www.ibiblio.org/mp3info/) (which is outdated but has a nice command line interface)
- [tageditor](https://github.com/Martchus/tageditor) (which is more modern, but prints more information then necessary in my opinion)

I used mp3info before I found tageditor.
I will provide versions for both programms in the following, but if you have the choice: Just use tageditor.


## Set the title from filename

#### tageditor version

```zsh
ls | while read filename; do title=${filename%".mp3"}; tageditor set --values title="$title" --files "$filename"; done
```

#### mp3info version

```zsh
ls | while read filename; do title=${filename%".mp3"}; mp3info -t "$title" "$filename"; done
```

## Set the album from the directory name

(Use from within the directory.)

#### tageditor version

```zsh
tageditor set --values album="$(basename "$(pwd)")" --files *
```

#### mp3info version

```zsh
mp3info -l "$(basename "$(pwd)")" *
```

## Set the artist for all the files below directory

All the files in this directory or subdirectory will be taged as having the artist `Arty Artist`.
This worked best for me when using [fd](https://github.com/sharkdp/fd), because it behaves nicer then `find`.

#### tageditor version

```zsh
fd --extension=mp3 --type=file --exec tageditor set --values artist="Arty Artist" --files {}
```
#### mp3info version

```zsh
fd --extension=mp3 --type=file --exec mp3info -a "Arty Artist" {}
```

## Set the track number from filename

#### mp3info version

```zsh
ls | while read filename; do track="$(echo $filename | awk '{ print $1 }')"; mp3info -n "$track" "$filename"; done
```
