---
title: "Editoren"
---

Wer Code schreiben will, braucht einen Code-Editor. Es lohnt sich, sich einen Guten zu suchen, denn man wird viel Zeit mit ihm verbringen und er ist schnell ein essentieller Teil des Computers.

Ich selbst bin noch immer auf der Suche nach einem Editor, der mich restlos zufrieden stellt.

## Was ich will

Ein Editor mit dem ich glücklich werde müsste ein paar __Mindestanforderung__ erfüllen. 

* __Open Source__
* __Erweiterbar__ durch Plugins
* __Cross-Platform__ zumindest für alle Unixoiden Systeme. Wer weiß schon was die Zukunft bringt.
* __schnell__ (auch bei wirklich großen Files mit mehr als 100000 Zeilen)
* __durchdacht__ und nicht durchwachsen. Man merkt einem Programm an ob es designed wurde oder mit der Zeit gewachsen ist.
* __Starken Language Support__ für die Sprachen, die ich oft benutze. Das ist im Moment hauptsächlich Haskell und Markdown.
* __Starke Shortcuts__ die das editieren erleichtern und schneller machen
* __Git Integration__ gerne auch durch Plugin
* __Build__ um Code auch mal direkt im Editor auführen zu lassen

Außerdem gibt es noch ein paar __Standardfeatures__, die ohnehin bei den meisten Editoren dabei sind, wie

* __Zeilennummern__
* __Wortergänzung__
* __Syntax Highlighting__
* __Klammer Support__ für runde, eckige, geschweifte, spitze Klammern und was für die jeweilige Sprache noch als Klammer dient (bei Markdown zum Beispiel auch ```_```, ```__```, ```*```, ```**``` etc)
* __Distractionfree Programming__ (eher wenn man normalen Text schreibt und dafür keine Seitenleisten, Menüleisten, Vorschau... braucht)
* ...

### Shortcuts

Shortcuts die ich besonders häufig verwende sind

* Zeile löschen
* Zeile dublizieren
* Zeile verschieben (nach oben oder unten)
* Zeile markieren
* Wort markieren
* nächstes gleiches Wort markieren (Multicursor)
* zu Wort springen (mit fuzzy search oder regulären Ausdrücken)

Ich lerne langsam auch neue dazu (man muss sie ja dann auch verwenden), aber das braucht immer etwas Zeit. Im Idealfall finde ich einen Editor der all das erfüllt und verwachse so sehr mit ihm, dass alle Shortuts intuitiv werden.

Besonders toll (auch wenn ich es nie gelernt habe) finde ich die modularen Shortcuts beim vim:
    
*  man tippt ```3``` um den Befehl dreimal auszuführen, ```d``` für _delete_ und ```aw``` für _a word_. Der Editor entfernt dann die nächsten 3 Worte.
* man tippt ```d``` für _delete_ und ```0``` für _Anfang der Zeile_. Der Editor entfernt alles vom Anfang der Zeile bis zum Courser.
* man tippt ```10``` für _zehnmal_ und ```x``` für _delete Character_ und die nächsten 10 Buchstaben werden entfernt.

Wer das einmal verinnerlicht hat will das sicher nicht mehr missen.

### große Files

Manchmal verprogrammiert man sich und erzeugt aus Versehen rießige Dateien. Manchmal muss man Logfiles anschauen und bearbeiten, die seit Jahren geschreiben werden. Manchmal muss man eine seltsame Datei im Editor aufmachen, weil man dann vielleicht versteht welches Format sie hat (funktioniert öfter als man denkt, probiert das mal mit PDF/Bilddateien/Websiten).

Deshalb _muss_ mein Editor mit Files umgehen können, die seeeeeehr lang sind.

### Language Support

Knifflig, da will man ja immer was anderes.

Für __Markdown__ reicht mir eine Semi-Vorschau im Editor und ab und zu ein Blick auf das gerenderte (html-)Dokument im Browser. Immerhin ist Markdown  (Aktuelle Lösung: Sublimetext im Distraction Free Mode mit den Paketen MarkdownEditing (dark) und Markdown Priview und Firefox mit dem Plugin Auto Reload).

![Da kann man sich konzentrieren.](Distraction_Free_Mode.jpg)

Für __Haskell__ (und alle Sprachen die ich danach lerne) will ich die krasseste Unterstützung die auf diesem Planeten möglich ist, bitte. Ich wünsche mir

* Syntax Highlighting
* Qualifizierte Vorschläge und Ergänzungen
* Snippets
* Automatische Einrückung
* Cabal Support
* Linting
* Warungen bei unnötigen imports
* integrierte Documentation (auch zum offline abrufen)
* Benchmarking
* Automatische Berechnung von Signaturen

Sprich, wenn ich Code schreibe soll mein Editor zur mächtigen IDE werden. Das ist alles möglich (und im ghc schon angelegt), also keine Ausreden!

## Die Kandidaten

Es gibt ein paar Kandidaten die erstmal in die engere Auswahl kommen. Ich werde zu den Einzelnen noch etwas schreiben, vielleicht (hoffentlich) kommen ja noch was dazu. Bis jetzt habe ich auf dem Schirm:

* Sublime Text
* Vim
* Atom
* Emacs

Ich habe schon einige Editoren getestet und mit den meisten bin ich nicht recht warm geworden. Von den vielen seien hier erwähnt:

* [Limetext](https://github.com/limetext/lime)
* [Geany](https://alpha.app.net/lukasepple/post/32818682)
* [Yi](https://duckduckgo.com/l/?kh=-1&uddg=http%3A%2F%2Fwww.haskell.org%2Fhaskellwiki%2FYi)
* [leksah](https://leksah.org/)
* [Eclipse](https://eclipse.org/)

Wer noch Tipps für mich hat, immer her damit. Die Mailadresse steht zum Beispiel im Impressum.

To be continiued...
