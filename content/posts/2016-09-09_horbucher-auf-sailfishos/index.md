---
title: "Hörbücher auf SailfishOS"
image: 20160909151535_1.jpg
---

Unterwegs höre ich sehr gerne Hörbücher - besonders in vollen öffentlichen Verkehrsmitteln, wo man ohnehin nicht viel anderes machen kann als Musik/Podcasts/Hörbüchern zu lauschen oder seinen Feedreeder durchzuarbeiten. Auf SailfishOS gibt es keinen nativen Client, aber die Standardapp "Medien" funktioniert wunderbar. Wie ist also der Workflow?

# Umweg über den Rechner

Bestimmt geht das auch direkt auf dem Telefon, aber ehrlich gesagt habe ichs noch nicht ausprobiert. Außerdem geht es dank ```ssh```/```scp``` sehr flott auch von der Kommandozeile.

## Hörbuch finden

Ich könnte es gar nicht besser zusammenschreiben als es im [Wiki Audiobooks Subreddit](https://www.reddit.com/r/audiobooks/wiki/_/wiki/sources) steht.

Für diesen Blogpost wählen wir [The Wrong Box](https://librivox.org/the-wrong-box-by-robert-louis-stevenson-and-lloyd-osbourne/) auf [Librivox](https://librivox.org).

## Hörbuch herunterladen

Hörbücher sind meistens mehrere Audiodateien, zum Beispiel eine für jedes Kapitel. Meistens bekommt man diese schön kompakt in einer zip-Datei, die lädt man herunter und entpackt sie. Manchmal sieht man aber auch nur eine Website voll mit Links zu dein einzelenen Dateien (wsl für Leute, die alles direkt im Browser konsumieren wollen). In diesem Fall hilft dieser Befehl.

``` bash
lynx -dump https://librivox.org/the-wrong-box-by-robert-louis-stevenson-and-lloyd-osbourne/ | grep 64kb.mp3$ | awk '{print $2}' | xargs -n1 wget
```

In Worten: Schau dir alle Links der genannten Webseite an, nimm die die auf "64kb.mp3" enden und lade sie einzeln herunter. (Es könnte also schlau sein, das in einem leeren Ordner auszuführen.)

Bei librivox ist das aber eigentlich nicht nötig, man kann alles direkt herunterladen (sogar per Torrent, wohooo!).

Es macht übrigends alles (erheblich) einfacher, wenn die Dateien so benannt sind, dass die alphabetische und die logische Ordnung übereinstimmen.

## ssh aufs Telefon

Testweise kann man sich schonmal aufs Telefon verbinden und den Ordner anlegen, wo die Dateien später liegen sollen. Ssh muss in den Einstellungen des Telefons unter "Entwicklermodus" aktiviert werden, dort findet man auch Passwort und IP-Adresse.

```bash
ssh nemo@192.168.178.29
mkdir Music/wrong_box
```

## Hörbuch aufs Telefon

![](20160909151535_2.jpg)

Da wir ja jetzt schon alles vorbereitet haben, reicht ein kurzes Befehl:

```
scp *.mp3 nemo@192.168.178.29:/home/nemo/Music/wrong_box
```

Das wars schon.

## Hören

Wir können alles in der Medien-App anhören. Nach dem aktuellen Kapitel wird automatisch zum nächsten gesprungen.
