---
draft: true
title: "ideas I didn't get to implement"
---
date
## How I use notebooks on the job

On the job I usually have a notebook with me.
I use it for notes of all kinds:

- todos
- meeting preparations and protocols
- ideas and random thoughts
- sketches
- a quote from someone that I found interesting
- quick calculations

Most of them are just a few words, because the context is normally clear to me.

I deal with most of the notes in some way or another.
Often this means doing the todo or transferring the information into a more appropriate place
(like the ticket-system or the company wiki).
After that I cross out the note.
If every note on a page is crossed out I remove the page.

## Some notes keep standing

This process naturally leaves you with a certain kind of notes:
Those that are too much work to implement, but too interesting to throw them away.

When I left my last job and cleaned my desk there for the last time I found my notebook.
I browsed through the pages and found some leftover notes.
I knew I would never realize them (at least in the way I meant to originally).
So in an attempt to not letting them die I will write down some of them down here.
(Of course I cleaned them from any information that should not be public).


- do a lightning talk about parser combinators
  (so we don't use so many regular expressions in production anymore)
- Can you explain your product (or favorite project) in a gif? (I am pretty sure I had this idea after listening to a [GDC talk](https://www.youtube.com/c/Gdconf)) <!-- TODO Image -->
- Build an analogue progress bar: A column of water that can be filled and emptied via a simple api call. It would stand in the middle of the room and be a decorative indicator of your progress. <!-- TODO: Image -->
- If there are too many tickets then you don't need to estimate them - just count them.
- $someone recommended me to look up the comic [One day in Kanban land](https://blog.crisp.se/2009/06/26/henrikkniberg/1246053060000)
- Have a live visualization of open tickets in tech support as [space invaders](https://en.wikipedia.org/wiki/Space_Invaders):
  - Support tickets are space invaders, entering the screen/ticket system from above.
  - As time goes by, tickets sink down.
  - Every member of the support team is a laser canon.
  - Answering to a ticket means shooting at it.
  - Tickets that are dealt with explode
  - Tickets that are not answered with soon enough sink to the bottom of the screen, 'hurting' the earth/company/team.
