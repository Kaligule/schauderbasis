---
title: "I wrote a hugo theme"
image: files.png
---

I just love the concept of static site generators  (SSG) for websites.
They allow to separate content and presentation really cleanly:

- content is defined in some textfiles using some markup language
- the presentation is defined in
  - a configuration file that sets some global metadata
  - a theme, which is mainly a bunch of templates and css and potentially some JavaScript.

My blog has been generated using a SSG (namely hugo) for a long time, during which I switched between quite a few themes.

## the most minimal theme

I always wanted a blog-theme that was as simple as possible.
There are a lot to try, but I never found something that fitted me perfectly.
I don't want Integration of Google Analytics or Disqus or any other third party.
I don't want to need a cookie banner because I don't use any cookies.
This is not a social media platform, it is a blog and I started it for a reason.

But I was also aware of how little I know about JavaScript and CSS.
And about design.
So building a theme myself seemed to be out of reach for me.

But then, some time ago I revisited
[motherfuckingwebsite](https://motherfuckingwebsite.com/)
(and it's many [successors](https://bettermotherfuckingwebsite.com/))
and even though I can see the satire I wanted my blog to be somewhat like that.
This seemed doable.

So I wrote some html-templates.
It worked somewhat well but there was no denying that the black-and-white aesthetics of bare HTML was hard on the eyes.

![bare html is hard on the eyes](bare_html.png)

## nice css that I don't have to write myself

Around that time I rediscovered [classless css themes](https://github.com/dbohdan/classless-css).
Basically a classless css theme is a bit of css that is designed to make generic HTML look good.
Generic HTML means that there don't need to be special classes in it, just the standardized html5 tags.
There are quite a lot of these themes available on the internet, most under free licenses.
This was exactly what I needed.

I added a single configuration setting to my theme:
It determines which classless theme you want to use.
I added a sensible default and documented everything.
I also let the user of the theme decide whether they want to load it from the web on each visit or whether they want to host it themselves.

![nice css that I didn't have to write myself](demo_site.png)

And there we are: [A theme for Hugo.](https://gitlab.com/Kaligule/classless-blog)
Flexible in it's looks, minimal in it's design and beautiful in my opinion.

## Some things I learned on the way

- HTML is really expressive.
  I had assumed that markdown was about as powerful as Markdown.
  I was clearly wrong: there are [soo many HTML tags](https://www.w3schools.com/tags/) for all sorts of things.
  My favourite finds include:
  - [`<abbr>`](https://www.w3schools.com/tags/tag_abbr.asp)
  - [`<time>`](https://www.w3schools.com/tags/tag_time.asp)
  - [`<summary>`](https://www.w3schools.com/tags/tag_summary.asp).

- [Hugo's documentation](https://gohugo.io/documentation/) is hard to use.
  I really wish it was decided into different sections for what I assume are it's three main target groups:
  website-builders, theme-builders and hugo-developers.

- I really wished to find a set of best-practices for building themes.
  Instead you are on your own with all the opinions of the internet as your best guideline.
  This also shows when you try to switch between themes:
  You almost always have to adapt some config that is specific to the implementation of that theme only.

- The theme format and ecosystem of Hugo both have great parts that are simple and powerful
  (like the `theme.toml` or how the `layouts` directory is structured).
  Other parts look like the result of yearlong growth and people doing "whatever works"
  (like the requirements for themes).

- For the demo site I needed some cool images.
  I found a lot on [unsplash](https://unsplash.com/) and it was fun to collect unrelated images that fit together.

- It is hard for me to know if a site is accessible or not.
  There are some web services that will score your site automatically, but I [don't really trust those](https://accessibility.blog.gov.uk/2017/02/24/what-we-found-when-we-tested-tools-on-the-worlds-least-accessible-webpage/).
  So for now I will just browse the site with a command line browser (lynx or w3m) and if that works well I feel like I have done enough.
  I am especially happy about constructive feedback in the point.

- putting together a theme is one thing, finishing it as a project is a lot more work.
  You know, like doing
  a [public repository](https://gitlab.com/Kaligule/classless-blog/),
  the [demo site](https://kaligule.gitlab.io/classless-blog)
  with nice pictures under [free licenses](https://gitlab.com/Kaligule/classless-blog/-/blob/main/exampleSite/README.md#sources),
  the [documentation](https://gitlab.com/Kaligule/classless-blog/-/blob/main/Readme.md),
  a [license](https://gitlab.com/Kaligule/classless-blog/-/blob/main/LICENSE),
  [testing](https://gitlab.com/Kaligule/classless-blog/-/merge_requests/1),
  [CI/CD](https://gitlab.com/Kaligule/classless-blog/-/pipelines)
  etc.
  But having done all that also means that I can be sure that it will continue to be usefull AS it is,
  not just a bunch oft dead code that nobody can get to work anymore.
