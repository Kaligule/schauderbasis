---
title: "Emacs lernen,  Tag 1"
---


Auf der Suche nach einem Editor beschäftige ich mit Emacs. Auf Emacs selbst gehe ich später noch genauer ein, hier geht es um meine ersten Kontakte damit.

## Logbuch, Tag 1
* Fange jetzt an, Emacs zu lernen. Ich google nach einem Handbuch und werde fündig: https://www.gnu.org/software/emacs/manual/emacs.html
* Tippe ```emacs``` in die Konsole. Eine Hässliche GUI geht auf. Die will ich nicht. Tipp meines Nachbarn: Mit ```DISPLAY =emacs``` bekommt man einen Terminalbasierten Emacs. Schon besser. (Später lerne ich, dass es mit ```emacs -nw``` besser geht)
* Außer mir sind noch andere Personen im Raum, die vergnügt Tipps geben Details aus Richard Stallmanns Leben rezitieren. Alle Tipps sind verwirrend.
* Das Handbuch erklärt erstmal, wie toll Emacs ist. Doof.
* Einer der Kollegen zieht aus seinem Schreibtisch zwei Seiten (klein gedruckte) Emacs Schortcuts: Die [GNU Emacs Reference Card](https://www.gnu.org/software/emacs/refcards/pdf/refcard.pdf)
* Es gibt eine Tutorial, ähnlich dem ```vimtutor```. Dieser ist aber natürlich erst später eingeführt worden, Emacs war erster! Das Tutorial bekommt man in Emacs mit ```C-h t```.
* Habe die letzten Minuten damit verbracht, Emacs abzuschießen (weil ich nicht mehr rauskam) und das Tutorial wieder aufzurufen. Wie man Emacs richig beendet habe ich noch nicht rausgefunden.
* Mittagspause
* Das Tutorial ist ganz gut. Manche von den Shortcuts kann ich sogar nachvollziehen. Ich lass das Handbuch beiseite und arbeitet mich durch das Tutorial.
* Die Shortcuts in Emacs werden auf eine bestimmte Weiße aufgeschreiben:
    * ```M``` steht für _Meta_ (also _Alt_)
    * ```C``` steht für _Controll_ (also _Strg_)
    * ```-``` trennt mehrere Tasten, die man gleichzeitig drückt (im Gegensatz zu Leerzeichen, wo Sachen hintereinander gedrückt werden)
    * Kleine Buchstaben bedeuten die jeweilige Taste.
	* Den Emacs verlässt man mit ```C-x C-c```. Das Bedeutet übersetzt: "Drücke erst ```Strg``` und ```X``` (gleichzeitig) und dann ```Strg``` und ```C```(gleichzeitig). Dabei ist es kein Problem, wenn man ```Strg``` nicht loslässt.
	* Das Tutorial findet man mit ```C-h t```, als ```Strg``` und ```h``` gleichzeitig und dann (ohne andere Tasten) ein ```t```.
* Bis jetzt kann ich mit originalen Emacs Shortcuts: Ein(e)(n) Buchstaben/Wort/Zeile/Seite vor/zurück springen. Hat mit den alten Tasten ja auch viel zu lange gedauert. Schön: Die Shortcuts sind mit den Tasten ```b```ack, ```f```orward, ```p```revious, und ```n```ext belegt. Das kann man sich ja sogar merken :)
* Hmmm, ```C-a``` und ```C-e``` für __A__nfang/__E__nde der Zeile. Auf deutsch ganz gut zu merken, im Englischen sind mir die Shortcuts nicht gnaz so klar.
* Man kann Befehle mehrfach hintereinander ausführen lassen. Das kenne ich noch aus meinen wenigen Vim-Erfahrungen. Schön, dass es hier auch geht.
* Die Syntax dafür ist ```C-u Anzahl Eigentlicher-Befehl```. Ich merke mir mal u wie in __u__nit.
* Jonathan neben mir beginnt mit zu fiebern. Er tippt wild  Programme in Emacs, aber benutzt nicht genug Shortcuts. Ich klebe uns beiden die Pfeiltasten ab, um die Emacsshortcuts zu erzwingen.
* In Zeile 316 des Tutorials das erste mal ein Hinweis darauf, wie man text manipuliert: "If you want to insert text, just type the text." Bisher bin nur vor und zurück gesprungen.
* Das Abkleben der verführerischen Tasten funktioniert: Selbst wenn man sie noch ein bisschen benutzen kann, fühlt man sich schlecht dabei.
* Im Gegensatz zu Vim scheint es keinen Commandmode zu geben. Daraus folgen Zwei Dinge:
    - Wenn man was tippen will kann man das einfach tun
    - Wenn man Shortcuts benutzen will, sind diese länger (werden zum Beispiel mit ```C-x``` eingeleitet).
* Es gibt einen Unterschied zwischen _delete_ (einfach löschen) und _kill_ (nur ausschneiden, aber noch zum wieder einfügen in den Buffer legen). Manche Befehle machen das eine, andere das andere. Meine Güte, das wird kompliziert.
* Obwohl alle um mich herum Vi(m) benutzen, sind sie aufgeschlossen und fiebern mit. Der eine ehemalige Emacsuser gibt Ratschläge und entusiastisch vorgetragene Anekdoten zum besten.
* An den Commandmode hatte ich mich scheinbar mehr gewöhnt als gedacht. Ab und zu suche ich ihn noch und wundere mich, wenn komische Dinge passieren (oder nicht passieren)
* Mit ```C-u 100 r``` kann man Hundert rs auf einmal schreiben :)
* rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
* Dieses Log schreibe ich im Moment noch in Sublime Text, aber langsam fühle ich mich tatsächlich fit genug, das auch direkt in Emacs zu machen. 

## Bilanz, 1 Tag

* Hat Spaß gemacht heute.
* Viele ungewohnte Shortcuts gelernt, auch für Dinge die ich schon vorher anders konnte. (Ich soll zum Beispiel kein Page Up/Down und keine Pfeiltasten benutzen)
* Einige Dinge kommen einem zuerst komisch vor, machen aber später Sinn. (Zum Beispiel werden einzelne Buchstaben immer gelöscht (_delete_), ganze Worte, Sätze und Zeilen aber _gekillt_, so dass man sie schnell an anderer Stelle wieder einfügen kann.)
* Das Tutorium ist besser geschrieben als der Vimtutor. Dort werden die Shortcuts hauptsächlich eingeführt, bei Emacs wird erklärt, warum sie sinnvoll sind. Das macht viel aus.
* Von der wahren Macht von Emacs habe ich noch nicht kosten dürfen. Alles bisherige kannte ich anders auch aus Sublimetext, abgesehen von dem ```C-u```, ```C-l```, ```M-a``` und ```M-e```.

Morgen mache ich weiter und schreibe das Log in Emacs.
