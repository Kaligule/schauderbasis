---
title: "Möwenweg reimt sich auf Bullerbü"
image: kinderbuecher.jpg
---

<!-- image link: https://unsplash.com/photos/shallow-focus-photography-of-books-lUaaKCUANVI -->
<!-- image author: Kimberly Farmer -->

## Eine modernere Kinderbuchreihe

Die Bullerbü Bücher (von Astrid Lindgren) [sind toll](https://schauderbasis.de/posts/bullerbue_ist_ein_tolles_erstes_buch).
Aber man vergisst keine Seite lang, dass es in der Vergangenheit spielt.
Eine modernere Version ist die Reihe "Wir Kinder aus den Möwenweg" (von Kirsten Boie).

Beide Buchreihen fühlen sich sehr ähnlich an:
Realistische Alltagsabenteuer einer Gruppen von Nachbarskindern, erzählt von einem 8/9 Jährigen Mädchen.
In beiden Erzählungen passieren deshalb ähnliche Dinge:

- Die Jungs und Mädchen spielen meistens gemeinsam, ärgern sich aber auch oft gegenseitig und legen sich gegenseitig rein.
- Die Kinder fahren Schlitten/Schlittschuh, pflücken Erdbeeren
- Sie arrangieren sich mit dem jüngsten Kind, lassen es manchmal mitspielen und manchmal nicht.
- Die Erzählerin erlebt Geburtstag, Ostern, Weihnachten und Silvester.

Aber Möwenweg ist eine Geschichte für moderne Kinder, deshalb sind ein paar Grundtöne anders:

- Stat 3 Bauernhöfen (isoliert auf dem Land in Schweden) leben die Kinder
  in einer Reihenhausreihe (wsl in einem ländlichen Vorort in Norddeutschland)
- Stat Lämmern und Hühnern haben die Kinder Kanninchen und Meerschweinchen
- Stat bei der Ernte helfen die Kinder im Haushalt.

Übrigens spielt Möwenweg *nicht ganz* in der Gegenwart, aber das merkt man kaum.
Hier sind die einzigen Hinweise auf die Zeit, die ich gefunden habe:

- Als der große Bruder Petja ein Handy zu Weinachten bekommt, beginnt er viele SMS zu versenden.
- Eine Frendin bringt einen *CamCorder* mit und die Kinder drehen einen Film.
  Die Videokamera ist aber so wertvoll, dass sie nicht an andere Kinder weitergegeben werden darf.

Ich würde die Möwenwegbücher deshalb auf die 2000er Jahre datieren - in dieser Zeit erschien auch das erste Buch der Reihe.

Besonders toll an den Möwenwegbüchern sind auch die (ungekürzten) Hörbücher.
Jenny Mierau hat hier absolut fantastische Lesearbeit geleistet -
sie spricht natürlich, man hört die Stimmung und auch den Subtext heraus.
Wirklich gut.

## Das ist doch kein Zufall

Da beide Buchreihen bei uns tagein tagaus gelesen/gehört/nachgespielt werden, fallen auch einige ungewöhnliche Parallelen auf.

Zum Beispiel gibt es in beiden Geschichten einen Großvater/Opa, der diese Rolle für alle Kinder der Bande einnimmt.
Für mich ist sehr klar das Kirsten Boie die Bullerbü Geschichten gut kennt und einige Motive aufgegriffen hat.

Mir gefällt es aber am besten, wenn die Referenz ein bisschen besser versteckt ist:

### Der irreführende Name

Aus "Wir Kinder aus Bullerbü":

> Mitten zwischen Bullerbü und Storbü wohnt ein Schuhmacher, der heißt Nett.
> Er heißt Nett, aber er ist kein bisschen nett, wirklich kein bisschen.

Schuhmacher Nett ist ein Trinker, droht den Kindern mit Prügeln und misshandelt seinen Hund.

Aus "Geburtstag im Möwenweg":
> [...] unsere Lehrerin ist sehr alt und heißt Frau Streng.
> Aber sie ist überhaupt nicht streng!
> Sie ist sogar sehr nett.
> Da müsste sie doch eigentlich Frau Nett heißen.

Frau Nett ist witzig und organisiert immer wieder Spendenaktionen "für die armen Kinder in Afrika".

Die beiden Figuren haben also eigentlich nicht gemeinsam,
werden aber auf komplementäre Weiße eingeführt.

### Der nächtliche Überfall

In Bullerbü wollen die Kinder in der Scheune übernachten, die Jungs in der einen Scheune und die Mädchen in der anderen.
Die Jungs schleichen sich hinüber und erschrecken Mädchen (erfolgreich).

Im Möwenweg wollen die Kinder nach einem Straßenfest im Zelt schlafen, die Jungs in dem einen und die Mädchen im anderen.

> [Wir Mädchen] konnten noch nicht gleich schlafen, weil doch klar war, dass die Jungs uns bestimmt überfallen würden.
> [...] Da mussten wir natürlich wachsam sein.

Als lange Zeit nichts passiert, schauen sie ins Jungszelt und entdecken, dass die Jungs schon eingschlafen sind.


### Die Einkaufsliste

In Bullerbü werden Lisa und Inga zum einkaufen in den Dorfladen geschickt.
Sie wollen sich alles merken, was sie einkaufen sollen, aber sind *so* viele Dinge.

> Wir waren ja ein wenig besorgt, ob wir das alles behalten würden,
> und deshalb zählten wir uns anfangs immer wieder auf, was wir mitbringen sollten.

Trotzdem vergessen sie beim Einkauf immer wieder Sachen (Hefe zum Beispiel) und müssen zurück laufen.
(Es ist mein liebstes Kapitel von allen.)

Auch im Möwenweng kaufen die Mädchen ein:
Sie sollen Hefe holen.

Aus "Geburtstag im Möwenweg":
> [Tieneke hat] vorgeschlagen, dass wir uns die ganze Zeit immer aufsagen müssen was wir einkaufen sollen.
> Sonst vergessen wir es noch.
> In Geschichten vergessen Kinder nämlich immer die Hälfte, wenn sie alleine einkaufen gehen.
> Ich habe aber nicht gewusst, wass wir vergessen konnten, wenn wir nur Hefe einkaufen sollten.
> Trotzdem habe ich eine Weile mit Tieneke zusammen immerzu "Hefe, Hefe, Hefe" gesagt.

## Die beste Art der Anspielung

Ich liebe diese Art von Anspielung.
Wenn man sie findet, dann ist es als ob der Autor einem zuzwinkert:
"Hey, ich kenne Bullerbü auch".
Ein Insiderwitz.

Und trotzdem ist es nicht billig.
Kirsten Boie übernimmt nicht einfach nur Teile der Bullerbü Bücher.
Sie verändert sie und setzt sie kunstvoll in einen anderen Kontext.

Es erinnert mich ein bisschen an das Video ["How to steal like Wes Anderson"](https://www.youtube.com/watch?v=3GK_3KgZios) von Thomas Flight.
Dort zeigt er, wie Anderson in ["The Grand Budapest Hotel"](https://en.wikipedia.org/wiki/The_Grand_Budapest_Hotel) eine Szene des Films  <["Torn Curtain"](https://en.wikipedia.org/wiki/Torn_Curtain) referenziert und so verändert, dass sie gleichzeitig vertraut und witzig anders wirkt.
