---
title: "Musik hören"
image: record.jpg
---

Wenn man irgendwann seine Musik gefunden hat, weiß man recht genau was einem gefällt (bei mir ist es Soundtrack Musik). Woher bekommt man die jetzt? Da fallen einem spontan mehrere Wege ein:

- kaufen
- piratisieren
- im Radio hören
- Streamingdienste
- Konzerte
- freie Musik

Gehen wir da mal kurz durch.

## kaufen

Ich habe keine Lust, mir eine große Musiksammlung anzulegen, indem ich einzelne CDs oder Alben kaufe. Das ist teuer und umständlich - und man hört immer wieder das selbe. Falls mich mal einzelne Lieder wirklich wegflashen mache ich das gerne.

## piratisieren

Mag ich nicht. Das muss jeder erstmal mit sich selbst ausmachen.

## Radio

![Quelle: https://pixabay.com/en/radio-retro-transistor-radio-old-543122/](radio-1.jpg)

Radio nervt mich, wegen Werbung und Moderatoren. 
Für Musik habe ich noch keinen Radiosender gefunden, der mich nicht nach kürzester Zeit genervt hat. Aber es gibt ja auch noch Internet Radio... Hier wird es interessant:

## Internetradio

Internet Radio besticht auf mehreren Ebenen:

- __Keine Werbung:__ Die meisten Sender beschränken sich darauf, den Sendernamen alle halbe Stunde zwischen zwei Liedern in den Namen des Senders zu sagen.
- __Auswahl:__ Es gibt ja soooooo viele Internet Radio Sender. Wer einen weiteren aufmachen will muss ja auch Frequenzen aufkaufen - es reicht eine IP-Adresse. Ich persönlich mag das [Soundtrackradio](https://www.streamingsoundtracks.com/).
- __skriptbar:__ Oh ja, da geht viel. Unter anderm werden auch stets die Metadaten (zum Beispiel Name des Stücks, Komponist, Interpret...) mit übertragen. Hier sei auf jeden Fall die Software [streamripper](https://de.wikipedia.org/wiki/Streamripper) erwähnt. Sie gehört zu den großen Schätzen in meiner Software-Repertoire.
- __Clients:__ Es gibt quasi keinen Musikplayer, der mit Internetradio nichts angangen kann. Man hat also freie Auswahl.

Wenn ich am Computer Musik hören will ist es also mit dieser Zeile getan:

```
mpd; mpc add https://209.9.229.206:80; mpc play
```

(Naja, eigentlich steht die IP-Adresse in ```/etc/hosts``` und für die Zeile gibt es einen Alias.)

![](Screenshot_2015-12-03_22-34-52.png)

## Streamingdienste

Zu Streamingdiensten (abseits von Internetradio) habe ich nie einen echten Zugang gefunden. Bestimmt ist das toll für viele, ich habs nie für mich entdeckt.

## Konzerte

Zählt nicht. Und selbst musizieren auch nicht. Livemusik hat andere Regeln, hier geht es um Musik, die sich in den Alltag integrieren lässt.

## Freie Musik

Und dann gibt es da noch die freie Musik - Musik unter freien Lizenzen. Das macht es natürlich schön unkompliziert: Stücke aussuchen und runterladen und immer dann hören, wenn man mag.

Es ist halt nicht so leicht, die zu finden. Ein guter Startpunkt kann die Website [Jamendo](https://www.jamendo.com/start) sein. Dort habe ich auch die Grundlage für das Intro der [Echokammer](https://echokammer.eu/) gefunden.

![Photo by <a href="https://unsplash.com/@dmav">Daniel Mav</a> on <a href="https://unsplash.com/photos/purQtRyZWUQ">Unsplash</a>](speaker.jpg)

# Fazit

Ich höre Musik [beim kochen](https://www.flavourjournal.com/content/4/1/25), beim essen, beim Programmieren und auch dazwischen. (Unterwegs höre ich Podcasts). Und eigentlich ist höre ich immer Internetradio, das funktioniert für mich einfach am besten.
