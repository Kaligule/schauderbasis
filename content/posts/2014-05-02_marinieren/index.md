---
title: "Marinieren"
---

__TLDR:__ Seit ich das Fleisch vor dem anbraten noch mariniere hat sich meine Lebensqualität erhöht.

Ich wollte nur darauf hinweißen, dass mariniertes Fleisch nochmal so gut schmeckt. (Marinieren heißt quasi "in würzige Sauce einlegen". Ich kannte es nicht bevors meine Freundin mir gezeigt hat, drum erwähne ichs lieber noch mal.)

## Rezept

Ich hab keines genaues, darum gings hier eigentlich auch nicht.
Ich nehme...

* ein paar Asiatische Soßen
* Senf
* Essig
* Peffer+Salz
* eine geschnittene Chillishote (wenn mans scharf mag)
* einen kleinen Schluck Wasser

...in _irgendeinem_ Verhältnis. Einmal umrühren bis sich alles gelößt hat, das Fleisch dazu und ab in den Kühlschrank.

Im Internet stehen auch noch tollere Rezepte.

## Einfache Grungregeln

* Zwischen Einlegen und Zubereiten sollte Zeit vergehen, damit der Geschmack auch einziehen kann. 30 min - 6 Stunden
* Man braucht weniger Marinade als man denkt
* Wenn man zuviel gemacht hat kann man den Rest immernoch super zum anbraten benutzen und bekommt noch viel guten Geschmack heraus.


## Aufwandseinschätzung

Extrem niedrig. Die Marinade macht man in 2 Minuten. Man muss nur dran denken es ein paar Stunden vor dem Kochen zu machen.

## Tipp zum Schluss

Nicht nur Fleisch eingelegt werden, da geht noch mehr:

* Zwiebeln
* Tofu
* Nudeln
* ...