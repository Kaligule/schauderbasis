---
title: "Am Arch"
image: SchienenSwitch.jpg
---

(Titelbild von [olibac](https://www.flickr.com/photos/olibac/) unter CC BY 2.0 von [hier](https://www.flickr.com/photos/olibac/3244014009/in/photolist-5WEqm2-4CC585-4XqwWg-68m3oj-eVuUPq-4R8tKB-2aXbZN-9VwtLU-7Y1bKA-7ZVE4T-gEnMta-akjJ21-aLmkDe-d8AkrQ-ffmpFS-6RJA3T-aLmkn4-cZNDhu-7Ri7PA-6PjUKB-aeQ77h-4Qb97G-c7N3WA-bE4MuC-KskHA-dCkVCL-4t5WKe-hG6zrM-6JYAoe-qWUL62-6K3GEE-5Xw8BU-34zb6V-4MUeA3-qWTFVx-bmEVEv-aLmmai-pukvrR-ncmScD-ekbZ9d-72pFtq-7jfwxB-4yMDxK-8agbNi-asSLFM-nxmKdG-d7tUXu-3j7eG4-6vHhTY-6K3Fhq/))

Bei meiner Wahl der Distribution habe ich einen Fehler gemacht.

Ich hatte [damals](https://www.schauderbasis.de/eine-distribution-auswahlen/) meine Auswahlkriterien folgendermaßen definiert:


> * modern: wenn es neues Zeug und coole Erfindungen gibt, will ich davon profitieren
> * stabil: so modern nun auch wieder nicht, dass ständig alles abstürzt
> * einsteigerfreundlich: ein System, das man auch als angagierter Anfänger meistern kann, eine Anlaufstelle für meine großen Haufen von Fragen
> * lebendig: ein System das beständig weiterentwickelt wird, mit Sicherheitsupdates, neuste Versionen und Kompatibilität für alles

Die Wahl fiel damit auf Fedora, und diese Konsequenz würde ich heute auch noch so ziehen. Aber meine Präferenzen haben sich geändert:

## Hilfe aus dem RL

(Wer nicht weiß was das RL ist, der verbringt entweder zuwenig Zeit am Computer oder sehr viel zuviel.)
Manchmal bleibt man stecken und dann hilft einem die ganze schöne Dokumentation nicht. Dann helfen auch keine Wikis, Hilfeforen, dann nützt Stack Overflow nichts und Reddit versagt. Manchmal muss man _jemanden_ fragen. Ich rate also jedem, sich eind Distribution zu suchen, die in der unmittelbaren Umgebung auch schon benutzt wird.

Bei uns in der Uni läuft ausschließlich Ubuntu in den Computerräumen. In meiner unmittelbaren Umgebung werden außerdem Debian, Arch und Mint eingesetzt. Künftig werde ich das in meine Entscheidungen mit einfließen lassen.

Neues Kriterium:

* RL-Support: zum über Dinge untehalten und sich gegenseitig Sachen zeigen kann

(Hätten wir unseren ursprünglichen Plan - gemeinsam auf Linux umzusteigen - direkt verwirklicht, dann wäre das kein Problem gewesen. Aber [Till wechselt seine Betriebsysteme sobald der Wind sich dreht](https://blog.tillmail.de/ich-will-wieder-auf-windows-wechseln/), da muss ich also in andere Richtungen suchen.)

## Stabilität wird teuer erkauft

Kurz und gut: Die hoffnung ein __modernes__ aber __stabiles__ System zu bekommen war etwas zu optimistisch. Oft ist es mir schon passiert, dass ich von einer neuen Version eines Programmes gelesen habe, die dann aber nicht in den Repositorys aufgetaucht ist. Die Phrasen "gut abgehangen" und "bewährt" tauchen in diesem Zusammenhang immer wieder auf. 

Weil ich dazu neige, die wenigen Programme die ich benutze auch etwas weiter auszureizen (das Wort "Poweruser" ist ja inzwischen so ausgelutscht das man es wahrhafig nicht nocheinmal in den Mund zu nehmen braucht) habe ich es trotzdem immer wieder geschaft, die Programme gegen die Wand zu fahren. Das passiert einfach.

Inzwischen weiß ich besser was ich will:

> ~~ __modern__: wenn es neues Zeug und coole Erfindungen gibt, will ich davon profitieren~~

Ich brauche nicht wirklich ein stabiles System. Ich brauche nur Backups und einen Packetmanager, der mir (auch gerne stündlich) die neusten Updates um die Ohren haut.

## Konsequenz?

Ich bin weg von Fedora und komplett auf Arch Linux gewechselt. Zum Umzug später mehr.
