---
title: "Sane defaults for navigation keys?"
image: navigation.jpg
---

<!--
credit: Martin Fisch
creditlink: https://www.flickr.com/photos/marfis75/5374308475
license: CC-BY-SA
-->

When I switched to Emacs, I thought there would be a consistent plan for navigation in a file (or buffer). I thought there would be a key for

* __c__haracter,
* __w__ord,
* __l__ine,
* __s__entence,
* __p__aragraph,
* the whole __f__ile

...and so on. In addition (so I imagined) there would be a key for every standard function you could apply to such a text component, like

* __c__opy,
* __p__aste,
* __k__ill,
* __m__ark,
* jump __f__orward or __b__ackward,
* __r__eplicate,
* e__x__chage with neigbour,
* move __u__p and __d__own

etc.

This way I would be able to build commands like "kill a word" (__C-k w__), "duplicate line" (__C-r l__) or "mark the paragraph" (__C-m p__) etc. It wouldn't be necessary to remember those shortcuts, you could easily derive them. Damn, you could even combine them (use (__C-l C-w C-w k__) to delete one line and two words). And they would perfectly fit together with the universal argument (C-u).

I don't even know why I expected it to be so. I just thought that the best editors (and Emacs claims to be on the top, no doubt) would have a concept like this (or better) with their shortcuts.

Instead, there are these short, but hard to remember combinations. Wouldn't it make sense to have similar shortcuts for similar tasks? In Emacs, this doesn't seem to be the case. For example, _"Move to the beginning of the file"_ (__M-<__) and _"Move to the beginning of this line"_ (__C-a__) just don't seem to be connected. Neither are _"Kill word"_ (__M-d__) and _"Kill line"_ (__C-k__).

Of course, I am far away of being some good at Emacs. I don't know any saints in the Church of Emacs to ask. There are two possibilities:

1. There is a deeper sense in the defaults, I just don't see it. It would be so kind if someone could give me some hint (you can find my mail address in the impressum). At some point, I will achieve wisdom.
2. The default keys were never designed, the just grew that way. And while this would be very disappointing, this is still Emacs, isn't it? Nothing would stop me to define my own shortcuts, much better then the old ones. Perhaps someone did this before me and there is some module I should know?

I am still not sure what I want it to be.
