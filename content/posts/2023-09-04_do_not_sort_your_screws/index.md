---
title: "Do not sort your screws"
image: screws_all_over.jpg
---

<!--
screws_all_over image source: https://unsplash.com/photos/uOIvZYZ3PwA
unsorted_screws image source: https://unsplash.com/photos/qLByjahQ-SU
pile_of_screws image source: https://unsplash.com/photos/dsJtesSBCeY
Photographer: Dan Cristian Pădureț
Photographer url: https://unsplash.com/@dancristianpaduret
License: Unsplash
-->

I used to have a medium-sized toolbox in my basement.
Then recently I got my hands onto a relatively big amount of tools.
They were not sorted at all.

From what I can tell the previous owners used to keep the tools in the whatever room they used them in last.
"Finished the sink?
Just put screwdivers and the leftover sealing rings in the kitchen-box."
I guess it worked for them.

It didn't work for me.
I threw all the piles together and spent multiple evenings sorting.
Tools, material, boxes…

Sorting through your stuff is fun.
I have been a fan of Marie Kondo since I read her book the first time, so I wasn't afraid to let go of a lot of stuff.
It is a joyfully way to spend a few hours.

One thing I didn't get sorted out were the screws.
There were just too many different ones.

## What others do

![many drawers and organizers, nicely labeld](drawers.jpg)

<!--
Image source: https://unsplash.com/photos/qF0lQZZ7NVg
Photographer: Yuriy Vertikov
Photographer url: https://unsplash.com/@noa69
-->

I looked up how other people sort their screws.
High shelves with small boxes, sorted by length, thickness, head type...
There are selfmade tools that assist the sorting and big machines that do it all on their own.

All those solutions don't fit my situation.
My screws comes from many years of leftover screws, partly rescued from older furniture repuposed.
I don't have 5 or 10 or 20 kinds of screws, more like 100.
At the same time, it is very seldom that I have 10 of the same kind.

I also seldom need a very specific screw.
And if I did I probably wouldn't have it in stock.
Most of the time I need "2 Wood screws around _this_ length, not _too_ thick" or "anything with a round head like _this_".

## What works for me

So everything got into a big pile, in a big open box.
Only those screws and dowels that are still in their original package were allowed to stay there.
All of this went into a drawer.

This was not exactly a stroke of genius.
It was simply the thing that required the least amount of work.
For now I was fine with not making the problem worse.

![A pile of different screws"](pile_of_screws.jpg)

I turned out to work extremely well though.

- doesn't take a lot of space
- very easy to maintain
- finding a fitting screw is fairly easy (although finding a single certain screw is hard)
- there are practically no sunken costs (no stacking boxes, no rack, no time invested into sorting thousands of small parts), so I am very open to improvements

## I sorted them a little bit

After using that system for some time I noticed that I could do a little better.
This was mainly because I found that my screws could be divided into two piles very naturally: Wood screws vs Metal screws.
So I separated them into two different containers.

I think this works because of two reasons:
1. The division line is very clear (so both sorting and searching are easy)
2. The resulting piles are about equal in size (roughly a relation 1:2), so the split really cuts down the search space.

For those reasons this divide increased searching speed without sacrificing the spirit of the system.
The advantages get hurt just a little bit, but the trade of is worth it in my opinion.

I later split the bigger pile into long screws (>3cm) vs short screws (≤3cm), which works for the same reasons.
I don't think more splits would improve the situation for me, though.

## Conclusion

In some situations the best sorting system is not to sort at all. Keep in mind that this is a valid option, too.
