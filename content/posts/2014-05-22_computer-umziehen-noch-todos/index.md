---
title: "Computer umziehen"
---

Jetzt sitze ich also für kurze Zeit (meine Freundin steht in den Startlöchern, um den alten abzunehmen) auf 2 Laptops und soll mein Produktivsystem umstellen.

## Dateien

* alles was in Home liegt (außer den Ordnern "Owncloud", "Dropbox" und "Downloads")
* Meine PGP-Keys, vor allem die Privaten Schlüssel (und das soll ordentlich passieren, nicht über Dropbox)
* Bilder, Musik, Filme

Noch mehr? Ich weiß nicht genau. Wie viel information liegt in den Apps?

## Programme

Wenn man das Betriebssystem wechelt wechselt man fast alle seine Programme. Schwierig schwierig...

### Konversation Table

Um nicht völlig unvorbereitet da zu stehen haben wir (Till und ich) eine gemeinsame Tabelle angelegt, in der wir im vorhinein mal Gedanken dazu machen. Im Moment sieht sie etwa so aus:

* Dropbox -> Owncloud
* 1Password	-> KeepassX2
* Mail -> Geary, Thunderbird
* Kalendersync -> Owncloud
* Kontaktesync -> Owncloud
* Kontakte Client -> Gnome Contacts
* Browser -> Firefox
* Podcast aufnehmen -> ardour (Empfehlung von [hier](https://www.radiotux.de/index.php?/archives/7983-RadioTux-Sendung-Maerz-2014.html))
* Maps -> Irgendwas mit OpenStreetMap
* Appstore -> Paketmanager, konkret yum
* Dasy-Disk -> baobab
* keyword search im Safarie -> Instant Quick Search
* Marked -> Firefox

### Im Detail

Für beinahe jedes Programm muss ein angemessener Ersatz gefunden werden. Besondere Schwierigkeiten ergeben sich bei:

* __1Password:__ Die Alternative (unserer Wahl) heißt [KeePassX](https://www.keepassx.org/). Aber die Passworte umzuziehen stellt sich als nicht trivial heraus. 1Passwort kann als CVS Datei exportieren, aber das schluckt KeePassX nicht direkt. Vielleicht muss ich noch einen Converter schreiben. (Ich hab aber keine Lust)

* __Marked:__ Ich schreibe diese Artikel alle in Markdown. [Marked](https://marked2app.com/) zeigt eine .md Datei gerendert an, wärend ich sie in einem anderen (beliebigen) Programm bearbeite. Das funktioniert Mac-typisch sehr schick. Für Linux habe ich soetwas noch nicht gesehen. Es gibt ein Firefox Plugin, das ähnlich arbeitet, aber die Ergebnisse sind nicht halb so schön und Umlaute funktionieren nicht.
![OSX: Marked (rechts) rendert das .md-File wunderbar. Leider gibt es Version 2 nicht im App-Store, deshalb hier Version 1.](Bildschirmfoto-2014-05-12-um-10-32-09-1.png)
![Linux: Firefox versucht das .mdown-File anzuzeigen. Das Encoding funktioniert offensichtlich noch nicht perfekt.](Markdown_Firefox.png)

* __Editor:__ Ich schreibe demnächst nochmal was zu Editoren, hier nur so viel: Wer einen mächtigen graphischen Editor unter Linux sucht und nicht die vi-Shortcuts lernen will hat echt ein Problem.

## Backup

Ich habe mir noch keine Backupstrategie für Linux überlegt. Ich weiß das das nicht gut ist, aber alles braucht seine Zeit. Backup mache ich, wenn ich nur noch einen Laptop habe. Gibt es Tipps?
