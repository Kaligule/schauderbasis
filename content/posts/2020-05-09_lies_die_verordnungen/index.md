---
title: "Lies keine Oden, lies die Ausgangsbeschränkungen"
image: jen-theodore-alte-zeitungen.jpg
---

<!--
credit: Jen Theodore
creditlink: https://unsplash.com/photos/qybUrqKmFQw
-->

Die Ausgangsbeschränkungen (wegen
[COVID-19](https://de.wikipedia.org/wiki/COVID-19)) ändern sich ab und
zu und haben dann sofort große Auswirkungen auf unser Leben. Deshalb
sind dann Zeitungen und Radiosendungen voll mit Diskussionen und FAQs:
"Was ist nun erlaubt, was nicht?"

Das ist ja auch in Ordnung. Es wäre aber schon wichtig zu wissen,
dass man auch die Verordnungen im Original lesen kann:

https://www.stmgp.bayern.de/coronavirus/rechtsgrundlagen/

Am Ende gelten auch nicht die Regeln, die der Söder in der
Pressekonferenz erzählt, sondern das was im Gesetz (oder in diesem
Fall in der Verordnung) steht.

Darum mein Plädoyer: Bevor du Abhandlungen über Gesetze liest, lies
vorher die Gesetze selbst.

Sie sind genauer.
