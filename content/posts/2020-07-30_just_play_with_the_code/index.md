---
title: "Just play with the code"
image: plan.jpg
---

<!--
credit: Glenn Carstens-Peters
creditlink: https://unsplash.com/photos/RLw-UC03Gwc
license: Unsplash License
-->

When I want to do something for fun, this is what I do:

1. Plan it (find a date and time, ask people I want to participate)
2. Prepare it (pack the necessary stuff, drive there, set up the
   table/workspace)
3. Do it
4. Clean up (drive home, put everything back, remove tmp-files)

Now that I think about it, the workflow is pretty similar to what I do
when I want to work on something not for fun.

## It is easier for babys

While playing with my daughter I noticed that she does it
differently. She doesn't plan or prepare anything, she just jumps from
"the idea of doing something" to "Do it".

This results in a number of games I couldn't even have planed:

- Making noises with the gummi guiraffe every time her mother walks by
- Me patting her back to the rythm of "the lion sleeps tonight"
- Climbing the ever growing mountain of cushions I built up to stop
  her from falling from the bed

In a way this is how I used to work on my side-coding-projects: I
would browse through the code and do to it whatever I thought of
doing.

I would add features spontanously, refactor what I stumpled upon or
do research on how something works, just because my eye fell on
it.

This workflow was not very productive in the sense of reaching goals
or finishing projects. But it was great for learning, relaxation and
gradually improving the codebase. If development is a discussion, this
is the smalltalk.

And while my workflow changed a lot in the last years, I think this a
great way to get to know a codebase. Just play with it.
