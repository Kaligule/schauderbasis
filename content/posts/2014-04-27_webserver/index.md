---
title: "Webserver"
---

__Langfristiges Ziel:__
 So viel wie möglich Infrastruktur auf dem eigenen Server hosten. Dazu sollen gehören

* eigene Website mit Blog
* RSS und ein Späterlesen-Dienst
* Mail
* Kalender und Adressbuch
* Cloudstorage
* Chatten (Jabber?)

__Grundidee:__ Webspace anmieten. Das ist vorerst sinnvoller als in Hardware zu inverstieren und sehr viel billiger.

## Gehostete Websiten

Ich habe mir mal bei [Netcup](https://www.netcup.de/bestellen/produkt.php?produkt=354) eine gehostete Website gemietet. Die Kosten sind minimal<sup id="reference1">[<a href="#footnote1">1</a>]</sup>, der Funktionsumfang aber auch. Es ist [eine Domain](https://www.schauderbasis.de) dabei, ich darf  meine Mailadresse da haben und MySQL-Datenbanken anlegen. Es reicht also für ein Wordpress Blog.

Für mehr aber auch nicht. Recht schnell kommt man an die Grenzen seines Vertrages:

* Kein Comandline Accses zum Server hinter dem ganzen
* Mehrere MySQL Datenbanken, aber nur ein Passwort für alle
* Confixx ist vom ersten Moment an unsympathisch und stinkt
* Es kann fast nur Software installiert werden, die von Netcup auch vorgesehen ist.

Im Verlauf des Jahres werde ich also einen richtigen Webserver brauchen, der mir diese Probleme hoffentlich vom Hals schafft. Eventuell ist es sinnvoll, das mit Freunden zusammen zu tun, denn die Preis-Leistungskurve wächst in diesen Preisklassen scheinbar exponentiell<sup id="reference2">[<a href="#footnote2">2</a>]</sup>.

Einiges habe ich aber schon gelernt, das meiste von Till. Seine Ausführungen liegen glücklicherweise in [diesem Podcast](https://tillmail.de/echokammer/wordpress/?p=437) (ab 1:26:50) vor und haben mich mehrmals sehr weitergebracht (Hörempfehlung). Außerdem ist ein Passwortmanager nützlich, sonst dreht man durch.

Bilanz: An dieser Stelle muss ich später weiterforschen. Für die hohen Ziele habe ich zu kurz gegriffen. Die Erfahrung war es aber wert, man lernt sehr viel über Websiten.

## Dropbox

Dropbox hat sich in den letzten Jahren als sehr zuverlässig erwiesen. Daten gehen eigentlich nie verloren und das Syncing ist schnell, bequem und problemlos. Aber langsam verlieren sie (bei mir) ihr makelloses Image:

* es gibt keine Kontrolle über die Daten
	* ich weiß nicht ob sie verschlüsselt werden
    * Dropbox könnte mir Dateien unterschieben
    * [die Dateien werden geöffnet](https://blog.threatagent.com/2013/09/three-reasons-why-dropbox-previews-are.html)
* Dropbox schluckt gute Dienste
	* [Mailbox](https://www.mailboxapp.com/blog/#/posts/45426605131)
	* [Readmill](https://readmill.com/epilogue)
    * [Loom](https://blog.loom.com/loom-is-joining-dropbox-2/)
    * ...
* Condoleezza Rice wird plötzlich [in den Vorstand einberufen](https://www.theguardian.com/technology/2014/apr/11/dropbox-condoleezza-rice-privacy-surveillance) 
	* ... das ist die mit dem [Waterbording](https://de.wikipedia.org/wiki/Waterboarding)

Die Dropbox Idee ist genial, aber die Firma wird mir langsam unheimlich. Je schneller ich da weg komme desto besser.


## OwnCloud

OwnCloud ist wie eine Kombination aus iCloud und Dropbox in Open Source. Man kann sie selbst hosten oder irgendwo anmieten und sie synct so ziemlich alles zwischen allen Geräten: 

* Dateien
* Kalender
* Kontakte
* To-Do-Listen
* Musik
* Photos
* ...

Alles kann verschlüsselt abgelegt, gesynct und geshared werden, bei Bedarf auch passwortgeschützt geshared. Es gibt ein Webinterface und wenn man einzelne Funktionen nicht mag, kann man das Modul einfach rauswerfen. Außerdem wird OwnCloud von vielen Linuxen nativ unterstützt. Klingt ja alles sehr vielversprechend<sup id="reference3">[<a href="#footnote3">3</a>]</sup>.

![Webinterface unserer Owncloud](Bildschirmfoto-2014-04-27-um-11-59-53.png)

Till hat auf dem Server, auf dem auch [unsere Podcasts](https://tillmail.de/echokammer/wordpress/) liegen eine OwnCloud installiert, die wir Drei von der Echokammer (und inzwischen auch ein paar andere) benutzen. Es ist schon der zweite Versuch, beim ersten Mal ging irgendetwas kaputt und wir haben abgebrochen. Aber OwnCloud wird permanent weiterentwickelt und im Moment sieht es gut aus.

# Ausblick

Wir trauen OwnCloud noch nicht wirklich und haben von allem irgendwo ein Backup - aber sie läuft jetzt schon längere Zeit (3 Monate) sehr gut und ist ein großer Schritt in die richtige Richtung.
Später noch mehr dazu.

<sup id="footnote1">1. 17,91€ für 12 Monate. Till hatte noch einen 5€ Gutschein für mich. Insgesamt viel billiger als ich dachte. <a href="#reference1">↩</a></sup>
<sup id="footnote2">2. Das ist gut wenn man viel mietet (und sehr gut wenn man noch mehr mietet). <a href="#reference2">↩</a></sup>
<sup id="footnote1">3. Wer möchte kann sich auch mal [Seafile](https://seafile.com/en/home/) anschauen.<a href="#reference1">↩</a></sup>
