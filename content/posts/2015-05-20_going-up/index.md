---
title: "Going up"
image: Words-1.jpg
---

<!--
credit: John Fife
creditlink: https://www.flickr.com/photos/good-karma/405077721/in/photolist-BN8uD-NzdWc-7ieXLs-4GmSkp-txN62-eF861J-7UQWSy-gkjvfg-E252b-7NUn5t-b8q5ZP-aP3gAB-nscQ61-m13254-qnCEvo-CDkm7-7ym4bS-cKKrDq-6YhPqD-whBV4-gEP1e-fyhSAJ-db2aKJ-4zDNo1-6L1QmF-5KhzwQ-e2QVc5-kCdoWy-9dvwYJ-5HGwbk-9wUEGJ-ahanEy-bMQpSt-5r8yh6-j9mHqN-cPx5tL-fKVq6q-4wFkA1-9jPCqA-8HawBQ-7L1yny-4rqEZX-6XGcBZ-cmi89C-nuZsNw-4jFLbA-a5GaWi-o1SQsA-8WidP3-kY1FqR
license: CC-BY-ND
-->


## 5

I I I I   I   I a I I   I a a I   a a   I   a a a   I I a   a   a a a   I I a I   a   I I I I   I I   I I I   I a I a I a

## 150

I can not talk about anything.

## 200

I try to not to say those things that I am told not say.

## 300

I may not say some things, but still try not to talk like a baby. It is a hell of a job.

## 500

This is my try to deal with the problem of telling what the problem is and not to say any word that is not one of the 500 that people use most. It is really hard. For every word that gets lost from your word set gets,  you need more words to say anything at all and it gets hard to understand what all this means, but I had help from a think-thing that does the some of the easy work. The idea came from a man, who's work many people look at.

## 1000

This is a test, where I try to explain the test in the 1000 words people use most. The less words you have the more you have to talk. I had help from one of those think-thing that does some of the easy work. The idea came from a man, who's work many people look at.

## 5000

This is an experiment, where I try to explain the experiment itself in the 5000 most common english words. It is actually pretty difficult not to use difficult terms, often you have to describe them. As a setup, I used a web based editor, that does the counting for me. The original idea is from a famous series of pictures from the Internet.

## infinity

This is an experiment, where try I to explain the experiment itself in the [n most common English words](https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists) (where n is now finaly infinity, so I can say what I want). As it turns out, shrinking down your own vocabulary is pretty hard and you have to go back to the definitions. As a setup, I used [a web based editor](https://splasho.com/upgoer6/) that does the counting for me. It was inspired by xkcd, a [famous webcomic](https://xkcd.com/1133/).
