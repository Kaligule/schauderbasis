---
title: "Plotter"
---

Im Mathestudium spielen Plots eine erstaunlich geringe Rolle (man möchte sich davor schützen, aus Bildern falsche Schlüsse zu ziehen). Trotzdem ist es manchmal nützlich/notwendig, es gibt hauptsächlich zwei Anwendungsfälle:

* Mal schnell rausfinden, wie eine Funktion aussieht oder wie sich ein Parameter auswirkt
* Eine schicke Grafik für ein Skript oder eine Präsentation erstellen.

Mit guten Plots kann man prima angeben. Und deshalb möchte ich hier mal die besten Plotter vorstellen, die ich im Laufe meines Studiums kennen und schätzen gelernt habe.

Um einen halbwegs sinnvollen Vergleich zu bekommen habe ich bei jedem Programm die Funktion `f(x)= sin(1/x)` geplottet.


## Schnell und Flexibel

Für Zwischendurch und wenn man mal schnell eine Vorstellung oder Abschätzung des Graphen braucht.
Die Programme sollen schnell zu bedienen sein und fix arbeiten. Wenn man mit Parametern arbeitet will man diese verschieben können und der Plot verändert sich live mit.
Das Wort _kompilieren_ will man hier nicht hören.


### [Quick Graph](https://kzlabs.me/)

![Quick Graph](QucikGraph.png)

#### Plattformen:

* iOS

#### Besondere Stärken:

* mal schnell was plotten
* großer Funktionsumfang auf mobilem Gerät

Ich habe seit Jahren keine andere App zum plotten auf dem iPhone als Quick Graph.
Auf seinem Telefon will man ja meistens keine Wissenschaft machen sondern "mal schnell was rausfinden".

Gibts in einer [kostenlosen](https://itunes.apple.com/us/app/id292412367) und einer [Bezahlversion](https://itunes.apple.com/us/app/quick-graph+-your-scientific/id541477533?mt=8).


### Grapher

![Grapher](Grapher.jpg)

#### Platform:

* Mac OS

#### Besondere Stärken:

* vorinstalliert
* (2D-)Vektorgrafik Export
* Navigieren im Plot (verschieben, zoomen)
* starke Mathematische Funktionen
* Vektorfelder (2D und 3D)

#### Demos:

* im Programmmenü unter Examples

Grapher ist das am meisten unterschätzte Programm auf dem Mac (finde ich). 

Graphers Schwächen liegen in der Bedienung bei komplizierteren Funktionen.
Bei schwierigeren Sachen muss man muss schon ein bisschen arbeiten, bis man das sieht was man wollte. Dafür funktionieren dann aber auch Dinge (Vektorfelder, Fallunterscheidungen, Spuren von Differetialgleichungen ect) die man sonst kaum findet. 

Man sollte sich deswegen auf jeden Fall die mitgelieferten Beispiele anschauen, außerdem gibt es [im Internet](https://guides.macrumors.com/Grapher) ein bisschen Dokumentation.


### [Graphing Calculator](www.pacifict.com)

![Graphing Calculator](Graphing_Calculator.jpg)


Plattformen:

* Mac OS
* Windows

Vorteile:

* einfache Bedienung

Das Quick-Graph für den Desktop. Zum "schnell mal eben gucken" total gut. Starten, Funktion eintippen, Bild erscheint. Graphen exportieren geht leider nicht ordentlich.

Quick-Graph ist nicht herausragend, aber er macht was er soll und braucht keine Eingewöhnungszeit.


### [Google](https://www.google.de/search?hl=de&q=plot%20generator&gws_rd=ssl#hl=de&q=plot%20sin%281%2Fx%29)/[Wolfram Alpha](https://www.wolframalpha.com/input/?i=sin%281%2Fx%29)

![Google](Google.png)
![Wolfram Alpha](WolframAlpha.png)


Platformen:

* Browser

Vorteile:

* Verfügbarkeit

Geht natürlich auch. Wer nichts installieren will oder kann bekommt hier schnell was er braucht. Trotzdem würde ich nativen Programmen immer den Vorzug geben. Komplizierte Funktionen werden hier schnell knifflig und Parameter bekommt man nicht so einfach hin.


## Seriös und elegant

Für Skripte, Präsentationen ect...
Die Graphen sollen genau sein, gut aussehen, ordentlich beschriftet sein und wichtige Informationen vermitteln. Außerdem sollen verschiedene Ausgabeformate (pdf, svg, png..) unterstützt werden (Vektorgraphiken sind besser als Pixelgrafiken).
Dafür ist es auch gerechtfertigt, etwas mehr Arbeit und Zeit zu investieren.

Weil es hier mehr um Programmieren als um Programme geht schreibe ich die Plattform nicht jedes mal dazu - auf Linux, OS X, BSD und Windows wird jedes der Programme laufen.


### [Gnuplot](https://www.gnuplot.info/)

![Gnuplot](Gnuplot.png)

#### Demos

* [offizielle Seite](https://gnuplot.sourceforge.net/demo)
* [der Gnuplotting Blog](https://www.gnuplotting.org/)

#### Besondere Stärken:

* Datenpunkte plotten
* programmierbar/automatisierbar
* Open Source

Ich muss aber zugeben dass ich (damals) auf dem Mac Schwierigkeiten hatte Gnuplot zu installieren - gelohnt hat es sich trotzdem. Auf Linux hatte ich keine Probleme.
 
Wer es nicht installieren will kann Gnuplot auch im Browser ausführen (auch wenn man dann die meisten Vorteile verliert). Zum Beispiel [hier](https://plotshare.com/index.ws/plot/763911417) oder [hier](https://gnuplot.respawned.com/).

Mit Gnuplot kann man hervorragend Messdaten visualisieren, plotten und diese Prozesse automatisieren. In meiner Bachelorarbeit musste ich den Verlauf eines chaotischen Algorithmuses nachvollziehen und hier kann man die Stärken des Programms wirklich ausspielen: Man lässt sich die Logdateien des Algorithmus ausgeben und lässt diese von Gnuplot auslesen und parsen. Auch bei großen Datenmengen ging das wunderbar, wo andere Programme schon in die Knie gegangen sind.


### [R](https://www.r-project.org/)

![R](R.png)

#### Demos

* [offizielle Website](https://www.r-project.org/screenshots/screenshots.html)
* [Menugget](https://menugget.blogspot.de/)

#### Besondere Stärken:

* Daten visualisieren
* Mit Zufallsdaten arbeiten
* Nicht-Funktionen-Plots
* programmierbar/automatisierbar

R ist eigentlch ein Statistik Programm. Aber es ist auch super im Daten visualisieren, besonders wenn es nicht um normale Plots geht sondern um Histogramme, Kuchendiagramme, Punktdiagramme, gefittete Funktionen ect.

Ich selbst kenne mich nicht so gut aus mit R (denn ich habe selten mit Wahrscheinlichkeitsrechnung zu tun), aber die Ergebnisse waren immer extrem gut.

Ach ja, eines noch: Der Name ist extrem dumm. Der Nächste der vorschlägt, etwas nach einem Buchstaben zu benennen sollte sich vorher genau überlegen, wie man im Internet nach Dokumentation dafür suchen soll.


### [LaTeX](https://pgfplots.sourceforge.net/)

![PGFPlots](pgfplot.png)

Besondere Stärken:

* direktes Erzeugen im Latexdokument
* extrem detailfreudig wenn nötig

(Wer nicht weiß was LaTeX ist sollte jetzt wirklich aufhören diesen Artikel zu lesen und sich schleunigst damit beschäftigen (etwa [hier](https://www.latex-tutorial.com/)).)

Wenn man ein Dokument mit Latex erstellt liegt es nahe, auch die Graphiken damit zu machen. Geht auch. Das Paket heißt [PGFPlots](https://pgfplots.sourceforge.net/) und basiert auf [TikZ](https://en.wikipedia.org/wiki/PGF/TikZ).

Die Vorteile liegen auf der Hand:

* der Stil des Plots ist genau der des restlichen Dokumentes
* wenn sich die Daten nochmal ändern sollten muss man nicht alles neu plotten sonder nur einmal sein File neu kompilieren
* es gibt keine third-party, auf die man sich verlassen muss

Die [Beispielseite](https://pgfplots.sourceforge.net/gallery.html) zeigt auch recht deutlich, dass man es hier mit den Details wirklich ernst nimmt. Sehr (sehr) viele Varianten mit sehr feinen Unterschieden. Wer sich fragt ob man sich über Plots überhaupt so viele Gedanken machen kann, den bitte ich einfach einmal durch [das Handbuch](https://pgfplots.sourceforge.net/pgfplots.pdf) zu scrollen. (Man braucht es nicht lesen, einfach einmal von oben bis unten scrollen und ein paar Bilder anschauen.)

Ich habe mit PGFPlots selbst noch nicht gearbeitet, aber TikZ selbst schon sehr viel. Es ist immer viel Arbeit, aber man hat die absolute Kontrolle über alles und die Ergebnisse sind immer großartig gewesen.


### [Python](https://matplotlib.org/index.html)

![Python](xkcd.png)

Mit den meisten Programmiersprachen kann man auf die eine oder andere Weise plotten. Matplotlib ist eine Python Bibliothek, die das halt auch kann.

Besonders toll finde ich aber die [xkcd-Style Plots](https://jakevdp.github.io/blog/2013/07/10/XKCD-plots-in-matplotlib/).

Vor kurzem hat [dieser Artikel](https://www.chrisstucchio.com/blog/2014/why_xkcd_style_graphs_are_important.html) ein paar Wellen geschlagen. Es geht darum, warum es (manchmal) gut ist wenn Bilder/Diagramme wie gemalt wirken. Dann achtet man nämlich nicht mehr auf die genauen Werte, sondern nur noch auf den Verlauf der Kurve. Manchmal will man ja genau das.

Und in Matplotlib ist das auch sehr einfach zu machen. Man ruft einfach vor dem plotten einmal ```matplotlib.pyplot.xkcd()``` auf und alles sieht aus wie von [Randall Munroe](https://en.wikipedia.org/wiki/Randall_Munroe) persönlich  [gezeichnet](https://xkcd.com/657/large/). Mein Script sieht so aus:

```
import numpy as np
import matplotlib.pyplot as plt
plt.xkcd()
x = np.linspace(-1.0, 1.0, 1000)
y = np.sin(1/x)
plt.plot(x, y, 'r-')
plt.title('plotted in xkcd-style')
plt.ylabel('sin(1/x)')
plt.show()
```


## Bilanz

Und welchen Plotter soll ich jetzt nehmen?

* automatisiert Daten auswerten
  * Gnuplot
* einzelne PLots für Folien und Skripte
  * Gnuplot (für weniger aufwendige Sachen)
  * Python (für Folien, der xkcd-Style kommt im Vortrag gut an)
  * Grapher (wenns ganz schnell gehen muss)
  * LaTeX (wenns um ungewöhnliche Details geht)
* schnell eine Funktion anschauen
  * irgendwas
