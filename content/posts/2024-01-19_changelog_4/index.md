---
title: "Changelog 4"
---

I like the concept of those changelog posts.
It makes me feel less pressure to write a full story and add pictures or so.
It also gives me a better sense of progress.

I noticed that this post is longer then the older ones though.
That is something I need to keep an eye on.

I am also not sure about the fact that almost every sentence contains the word "I".
It is a consequence of the forat of course.
Every sentence is about something I wanted, decided, tried or did.

Enough of this, here is what I did since the last changelog.

## merged my home-manager config into my NixOs config

I used to have separate repos for my home-manager config and my NixOs config.
This was useful once when I was using macOs regularly.
But I haven't touched a mac in years, so the split wasn't helping me anymore.
On the other hand it meant that I needed to set up multiple repositories for every new computer.
So I decided to merge them.

This was a lot more involved then I thought.
Merging 2 git repositories with unrelated histories is surprisingly hard.
Especially since I wanted one repo to be in a subdirectory of the other one.

I recently wrote an article about how [git filter-branch wants you not to use it](https://schauderbasis.de/posts/do_not_use_this_software/).
But filter-branch is exactly what you would normally use for such a task,
where the history of a repo is heavily edited.
Fortunately there is an external software-project,
[git-filter-repo](https://github.com/newren/git-filter-repo) that is much cleaner and better documented.

I finally managed to do it with `git filter-repo --to-subdirectory-filter` and then `git merge --allow-unrelated-histories`.

A lot of backups were created that day.
Changing history is about as easy as scify movies make it sound.

## set up a Jellyfin instance on the server

I finally have a need for a media center:
The kids want to consume the same media again and again (as kids do).
I need an easy way for all family members to consume that media _with_ the them.

After having had an eye on [kodi](https://kodi.tv/) and [plex](https://www.plex.tv/) for quite some time
I decided that I'd rather try out [Jellyfin](https://jellyfin.org).

Jellyfin is really cool.
(Once it is running) it looks quite nice.
It fetches tons of metadata from [imdb](https://www.imdb.com/) or [tmdb](https://www.themoviedb.org).
And it forces you to keep your media files in an orderly structure.

Once again, figuring out how to do anything on NixOs was hard.
But I am getting better at it, especially since I am slowly getting the hang of nginx.
Also reading nix code is getting easier and easier.
So it didn't take too long to get it running.

The hardest thing was finding out why I couldn't connect to the server.
It was set up, I could read it's logs and all, but I just couldn't connect to the Jellyfin server.
I rubberducked it to Sandra and to my astonishment she gave me the answer right away:
"Perhaps you need a restart, or it is the firewall or so?"
She is not a very technical person, so this was awesome advice.
I had forgotten to open the right ports...

This is what workes for me:

```nix
{ lib, config, ... }:

let
  jellyfinPort = 8096;
  jellyfinUrl = "MY_URL_FOR_JELLYFIN";
in

{
  services.jellyfin = {
    enable = true;
  };

  services.nginx = {
    enable = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
    virtualHosts."${jellyfinUrl}" = {
      enableACME = false;
      forceSSL = false;
      locations."/".proxyPass = "http://localhost:${toString jellyfinPort}";
      locations."/".proxyWebsockets = true;
    };
  };

  security.acme = {
    acceptTerms = true;
    certs."${jellyfinUrl}".email = "MY_MAILADDRESS";
  };

  networking.firewall.allowedTCPPorts = [
    80 443
  ];

  networking.firewall.interfaces.wiregrill = {
    allowedTCPPorts = [ jellyfinPort ];
  };
}
```

## set up Jellyfin again, but on a local server

I later decided that I wanted to have my media server at home rather then on the internet.
So I set up my old raspberry pie 4 and installed NixOs on it.
My NixOs config is nice and modular, so I could just enable the module from above and everything worked just as on my server.
No problem at all.

I love that so much about NixOs.

## used `mkOption` for the first time

I finally found that it was time to add my own options into my NixOS config.
The documentation is fine here and there are plenty examples around.

I need this because the config for backups is almost but not quite identical on my machines.
For example the paths of the directories that I want to back up vary.
So now I can write one general module that defines some options and every machine sets this option to it's own value.

Ideally every machine would only specify the following:

```nix
backup = {
  enable = true;
  paths = [
    "/home/johannes/SecretPlans/"
    "/home/johannes/PublicPlans/"
  ];
};
```

The rest ("Where is the backup server?", "How often should backups be done?", "How should they be encrypted?") is defined in the backup-module.
That is also a good place to write down some documentation.

This feels very clean to me.

I still didn't do monitoring of my backups.
This is dearly needed though.
I don't want to find out I haven't done backups for months.

