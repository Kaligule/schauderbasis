---
title: "Eine Distribution auswählen"
---

Also Linux, na toll. Und da gibt es ja auch nur unendlich viele davon (Statistiken gibts auf [Distro Watch](https://distrowatch.com)). Natürlich kann man auch im Nachhinein wechseln, aber es wäre ja schon gut, wenn man was hätte, wo man sich wohl fühlt. Wie also auswählen?

## Meine Kriterien

* __modern:__ wenn es neues Zeug und coole Erfindungen gibt, will ich davon profitieren
* __stabil:__ so modern nun auch wieder nicht, dass ständig alles abstürzt
* __einsteigerfreundlich:__ ein System, das man auch als angagierter Anfänger meistern kann, eine Anlaufstelle für meine großen Haufen von Fragen
* __lebendig:__ ein System das beständig weiterentwickelt wird, mit Sicherheitsupdates, neuste Versionen und Kompatibilität für alles

Mehr oder weniger nimmt so ziemlich jede Distribution für sich in Anspruch, das alles zu erfüllen. Ein guter Startpunkt für die Recherche ist vielleicht [hier](https://distrowatch.com/dwres.php?resource=major).

Auf [Tills](https://blog.tillmail.de/) Vorschlag hin haben wir uns erstmal für Fedora entschieden.

## Warum ausgerechnet Fedora?

* Fedora ist das Testvehikel von [Red Hat Enterprise Linux](https://de.redhat.com/fedora/). Deswegen gibts da immer den heißen Scheiß der vielleicht auch mal in Red Hat reinkommt. (+)
* Das bedeutet für mich auch, dass die Entwicklung nicht morgen eingestellt wird, weil es an Geld mangelt. (+)
* Außerdem ist die Führung der Distribution an eine unabhängige Institution abgegeben worden, die sind also nicht morgen _evil_ und bauen jede Menge Backdoors und Werbung ein. (+)
* Fedora ist eine von den größeren Distributionen, die von vielen Leuten (sogut man das halt feststellen kann) als Desktopumgebung benutzt wird. Da gibt es also eine Community. (+)
* Bei einem ersten Reinschnuppern macht alles eine ganz gute Figur. Besonders fällt auf, das OwnCloud out of the Box unterstützt wird. Cool! (+)
* Der Updatezyklus ist 6 Monate. Der Support für eine Version dauert 12 Monate, man kann also (nur) eine Version gefahrlos überspringen. Klingt fair. (+-)
* Red Hat ist eine Distribution für Unternehmen. Entsprechend ist Fedora nicht in erster Linie als Desktopumgebung entworfen worden. Wir werden sehen, wie sich das auswirkt. (-)

Insgesammt klingt das recht vielversprechend. Mal sehen wie sich die Punkte bewahrheiten werden
