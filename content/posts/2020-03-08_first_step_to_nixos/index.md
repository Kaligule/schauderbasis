---
title: "First step to NixOS"
image: snowflake.jpg
---

<!--
credit: Aaron Burden
creditlink: https://unsplash.com/photos/5AiWn2U10cw
 -->

Given my OS history (Windows -> macOS -> Fedora -> Arch Linux) these
were the options I was interested in as a next one:

- __Gentoo__ (but I have no interests in compiling everything myself)
- __GNU Hurd__ (but I don't think it is ready yet)
- __Arch Linux__ (There was nothing bad about it except the installation process)
- __Manjaro__ (because it is Arch with an installer)
- __NixOS__ (because I like writing configurations)

I decided on NixOS because I could see me using it for the next years
(other then Gentoo and GNU Hurd) and it would give me the opportunity
to learn something completely new (other then Arch and Manjaro that I
used for a few years now).

Since my old laptop T440S was gone and beyond repair I got my hands on
an old T430S (which makes it amazingly easy to swap parts) and created
a bootable USB Stick. And then ...

some wonderful things happened in my private live and I had no time to
go further.

It has been a few months, but I finally could pick up my experiment
with NixOS. Booting from the stick was easy and before you know it you
get a Desktop Environment. There are some multiple tools to help you
get the disk in the shape you want before the real installation
begins.

While I know how my way around the commandline the same can not be
said about partitioning, formating and file systems. It seems like
there are multiple definitions of "device" for different programms and
I wouldn't even know how a good setup would look like. I clearly lack
knowledge in this area.

The [NixOS manual](https://nixos.org/nixos/manual/index.html#sec-installation-partitioning)
uses a lot of commands that sounded obscure to me (`parted`, `mkfs`
and `mkswap`) so I choose to follow a guide from
[alexherbo2](https://alexherbo2.github.io/blog/nixos/install-guide/)
that seemed more detailed and recommended "GParted for
discoverability". Unfortunatelly I got stuck when creating new
partitions. I couldn't assign any other filesystems then 'linux-swap
and 'minix'.

Asking for help in the IRC (#nixos) got me some advice (from
colemickens, thanks a lot) - he encouraged me to give the commands
from the manual a try (because I didn't have much to loose on that
machine). It turned out that they did work as advertised. When I typed
them in by hand I got a better understanding of what they did than
from just reading/copying/pasting them.

The rest of the installation process went as expected. I had a little
trouble because I hadn't installed the network-manager before
rebooting (so I couldn't install it later because I had no
network-manager to get the wifi going) but fortunately the Ethernet
worked out of the box.

So now I have a working NixOS on my Thinkpad. Having managed the
installation so far makes me a bit proud and I am looking forward to
learn the nix way. I really like it so far, but didn't have much time
to play with it. Currently I only have `emacs`, `network-manager`,
`sway` and `firefox` installed, `qutebrowser` will follow. That should
be enough to get along for a while, the next step is to migrate my old
data.
