---
title: "Changelog 3"
image: "monitoring_schauderbasis.png"
---

## Addressbook visualization came to a halt

Previously I had tried to visualize my address book as a graph.
I stopped when I noticed that the vcf export I had didn't include the photos.
This was a problem of the address book app I use on my phone,
so I [filed a bug report](https://github.com/SimpleMobileTools/Simple-Contacts/issues/1054#issuecomment-1817994610).
This stopped my momentum to work on the project for the moment.

Now that the Simple Mobile Tools
[have been sold and are likely to go in a different direction](https://github.com/SimpleMobileTools/General-Discussion/issues/241#issuecomment-1837102917)
I will probably have to replace most of my mobile apps anyway, though.

## Work on video streaming solution

![Jellyfin is running, but there is no media yet](empty_jellyfin.png)

I installed Jellyfin (a video streaming) solution on my server.
It was reasonably easy (thanks to NixOS),
but I soon noticed that I don't have enough storage space on my server to store all the movies and shows I want.
I monitored the [Netcup Adventskalender](https://www.netcup.de/adventskalender/) for cheap storage upgrades, but the right thing for me was not in there.

So I will have to buy for the regular price soon.
It is not really expensive, so that is fine.
After that I will be able to continue the work on Jellyfin.

## Backup Sandra's laptop

The one PC in our home that doesn't run NixOS is Sandra's laptop.
On NixOS automatic backups are just a question of a few lines of config of course.
But on Fedora I had to do everything by hand, including a systemd unit for triggering the backup.
I used a [guide](https://fedoramagazine.org/automate-backups-with-restic-and-systemd/) of course.

The biggest problem I had after that was the initial backup,
which took so long that it failed for some reason.
The Laptop is a very weak one and I guess it was just too much for it.

So I started out with excluding the big directories (Music and Images) from the backup.
Then, when the backup was successful I reincluded them gradually,
so only a few GB of data had to be added to backups.

If this had been a work project I would have had to think of something better,
but for a private project doing this by hand was fine.

And now we know to have working backups of all machines again, yea!

## Monitoring

Wouldn't it be nice if you got an email if there has not been a backup created for a few days?
Now that the backups run I would like to monitor them.
It is time to have monitoring.

But boy this stuff is complicated.
Especially if you want to have it set up in a reproducible and documented way.

Zabbix didn't work for me, so I looked into Graphana.

Graphana is cool in combination with Prometheus and Telegraf.
At some point I also had Loki, Promtail and InfluxDB running.
It is nice to see how well everything works together,
but I found it very hard to understand what I need.
Especially because I don't even know what I want.

It would probably be easier if I had a working setup to learn from.

In the end I ended up with a selfmade dashboard that monitors my website (schauderbasis.de).
That is fine for now, I will try to expand on this at some point.

![Monitoring schauderbasis.de status code and response time](monitoring_schauderbasis.png)

## IOPCC

I found out I had won the [IOPCC](https://pyobfusc.com/).
I was very proud and tried to explain the achievement to a few non-technical people.
All of them were very supporting,
but it was clear that I had not been able to get across the point of the contest.

So I wrote a [blogpost](https://schauderbasis.de/posts/winner_of_the_first_iopcc/) about it.

## Nixos channel update

I noticed that my Laptops Nixos is running on an old channel,
so I updated it to the latest stable channel (from 22.11 to 23.11).

![current status of different nix channels](nix_channel_status.png)

I used [this guide](https://superuser.com/a/1604695) to upgrade because I don't do this very often.
It went uneventful.
Some config parameters had to be renamed and some software had to be replaced because it was not supported anymore (exa -> eza).

I took the opportunity to run `nix-store --gc`, for the first time in about a year.
Many GB of storage were freed.
But perhaps I shouldn't have done it:
When I recompiled my system it took nearly an hour (instead of less than a minute, like normally).
I think a big chunk of that is building emacs from master.

Worth it, though.
