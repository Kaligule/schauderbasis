---
title: "Sind alle Kreise gleich?"
---

Das Verhältnis von Umfang und Fläche eines Kreises ändert sich mit der
Größe des Kreises. Aber sind nicht alle Kreise gleich/kongruent?
Sollte dann nicht auch das Verhältnis immer gleich sein? Bei Umfang zu
Durchmesser ist es ja auch für alle Kreise gleich.

Ein Freund kam neulich mit dieser Frage zu mir. Er hatte sie mehreren
Leuten gestellt und alle konnten ihm die Formel für das Verhältnis
herleiten. Aber Formeln anschauen ist nicht das gleiche wie
verstehen. Tatsächlich ist es eine sehr gute Frage, die an die
Grundlagen des Messens heran reicht.

### Gleiche Messgrößen

Größen, die Dinge aus der gleichen "Kategorie" (zB Länge, Gewicht,
Geschwindigkeit) beschreiben kann man direkt zueinander ins
Verhältnis setzten (-> vergleichen). Zum Beispiel:

- Dein Auto ist schneller als meines (Kategorie _Geschwindigkeit_)
- Umfang eines Kreises und sein Durchmesser (Definition von $\pi$ (pi), die Kategorie ist hier _Länge_)

### Unterschiedliche Messgrößen

Bei unterschiedlichen Kategorien kann man nicht direkt vergleichen
- Bist du schwerer als du hoch bist?
- Sie ist so dumm wie die Nacht finster ist.

Hier können wir verstehen, warum das Verhältnis Umfang zu Flächeninhalt eines Kreises nicht einfach so zu messen ist: Die Kategorien (Fläche und Länge) sind verschieden.

In diesen Fällen muss man zuerst die einzelnen Größen "messen",
also ins Verhältnis zu einer Einheit setzten (siehe den Exkurs unten).

Hat man nun die beiden Größen ins Verhältnis zu ihren Einheit
gesetzt kann man diese Verhältnisse vergleichen. Man muss aber die
Einheit dazu nennen:

- 5 kg Mehl für 4 EUR -> 0,8 EUR/kg
- 1 Liter Wasser hat eine Masse von 0,998 kg -> Die Dichte von Wasser ist 0,998 kg/l

Nun können wir also den Umfang und den Flächeninhalt vergleichen, wenn
wir uns vorher auf Maßeinheiten (zB cm und cm^2) festlegen. Sobald wir
das aber tun sind eben nicht mehr alle Kreise gleich: Sie
unterscheiden sich in ihren Radien. Das Verhältnis ist nicht
platonisch, es hat eine Einheit (1/cm).

![Des Verhältnis Umfang zu Fläche hat eine Einheit](kurve.png)

Kongruenz kann man übrigens nicht als Argument heranziehen, dass
Umfang zu Fläche konstant sein sollten. Denn Kongruenz bedeutet nur,
dass die Verhältnisse von _Strecken_ konstant sind - und die sind
alle in der selben Kategorie.

### Exkurs: Einheiten an sich selbst messen.

Eine Einheit sollte außerhalb des zu messenden Systems definiert sein:

- Er ist 1,80 Meter groß (Vergleich zum
  [Urmeter](https://de.wikipedia.org/wiki/Urmeter)) und 90 kg schwer
  (90 mal schwerer als das
  [Urkilogramm](https://de.wikipedia.org/wiki/Kilogramm#Urkilogramm)).
- Das Mehl kostet 4 Euro (Vergleich zu dem Wert, den wir einen Euro
  nennen)
- Andromeda ist 2500000 Lichtjahre entfernt (Vergleich zur Strecke,
  die Licht in einer gewissen Zeit zurücklegt).

Wenn man stattdessen eine Einheit an dem definiert, das man messen
will, dann ist das Verhältnis trivial und die ist Messung nutzlos:
- Das Monster ist so groß, wie ein Monster groß ist.
- In einen Hefeteig soll so viel Mehl, wie in einen guten Hefeteig
  rein kommt.

So leicht lässt sich die Mathematik nicht austricksen.
