---
title: Remark
image: remark-hilfe.png
---
Die letzten Tage habe ich auf der [PyconGermany](https://de.pycon.org)
in Karlsruhe verbracht. Die dominanten Themen waren Machine learning,
Docker und vielleicht noch ein bisschen BigData. Über Python2 redet
eigentlich keiner mehr, das ist schön.

Ich habe viel gelernt und einige spannende Leute getroffen (schöne
Grüße an Corrie). Irgendwie schafft es die Pythoncommunity, ein
breiteres Feld als die üblichen Nerds anzusprechen (zu denen ich mich
selbst dazu zählen würde). Insbesondere das Verhaeltnis
Maenner:Frauen war erfreulich ausgeglichen, wenn auch noch nicht 1:1.

Dort habe ich meinen ersten Lightningtalk gehalten. Ich war sehr
aufgeregt, zumal der Vortrag auf Englisch sein sollte. Aber es lief
sehr gut, ich habe das Zeitlimit eingehalten und die Aha-Momente sind
übergesprungen. Ich bin sehr zufrieden.

Für die Slides habe ich [Remark.js](https://remarkjs.com/) verwendet:
Man hat eine html-Datei, den eigentlichen Inhalt schreibt man im
Markdownformat. Das wird dann vom Javascript in Folien umgewandelt,
wenn man die Datei im Browser öffnet. Das ist ein cleveres Format,
weil es mehrere Vorteile vereint:

* Schreiben in Markdown
* Styling mit der Macht von HTML und CSS
* Code Highlighting mit highlight.js
* Nur ein einzelnes, schlankes Dokument (zumindest solange man keine
  Bilder einbettet)
* Läuft auf jedem OS, das einen Browser hat.
* Es gibt ein minimales Interface für den Praesentierenden, in dem
  Notitzen, ein Timer und die nächste Folie angezeigt wird. Hilfe
  gibt es mit `h` oder `?`.

![Interface fuer den Praesentierenden.](interface.png)

Ein paar Schwierigkeiten habe ich aber noch:

Es ist knifflig, offline zu präsentieren, weil Remark.js aus dem Netz
nachgeladen wird. Gerade auf Konferenzen ist stabiles Netz ja keine
Selbstverständlichkeit. Man kann das natürlich lösen, indem man das
Repo klont und die lokale Version lädt, aber dann ist es halt nicht
mehr so portabel.

Außerdem habe ich ein bisschen die Allmacht von Orgmode vermisst,
Code einzubinden und den Output direkt generieren zu lassen. Aber das
ist ein Luxusproblem, wer Orgmode will muss eben Orgmode benutzen.

Insgesamt war ich mit Remark.js sehr zufrieden und würde es für
Praesentationen wieder verwenden. Andererseits kenne ich mich gut
genug um zu wissen, dass ich selten zweimal das gleiche Setup für
meine Grafiken und Folien verwende. Es gibt einfach zu viele spannende
Tools.
