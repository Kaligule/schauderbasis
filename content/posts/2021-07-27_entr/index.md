---
title: "entr should be part of every linux installation"
---

If you use the commandline as your IDE, then [entr](https://eradman.com/entrproject/) is super usefull.
As most unix programms, it does exatly one thing: "Run arbitrary commands when files change"

The syntax goes like this:

```
NAMES_OF_FILES_TO_WATCH | entr ENTR_FLAGS COMMAND COMMAND_FLAGS
```

## Example usage

The `entr` [website](https://eradman.com/entrproject/) lists a bunch of usecases and examples, some are quite elaborate.
Here are some of the things I do with `entr`:

### Export a markdown file to pdf and reexport it so that it is always up to date

Many converter tools
(like [latexmk](https://mg.readthedocs.io/latexmk.html), [hugo](https://gohugo.io/) and [diagramms](https://archives.haskell.org/projects.haskell.org/diagrams/))
have a flag that makes them watch for changes in files (and do their thing then).
But this doesn't feel like it should be in there.
Why would they all reimplement the same functionality?
Using `entr` feels more natural and is more composable.


```sh
ls thesis.md | entr pandoc --to pdf thesis.md
find . -name "*.md" | entr -c hugo # I am using this line while writing this blogpost
```

### Have a self-updating list of all TODOs in my org-file

```sh
echo "thesis.org" | entr -c rg TODO /_
```
This is nice when you are killing TODOs in your project and want to have a small "dashboard" of the remaining ones.
(the `/_` will be with the name of the file that changed)

### Run the testsuit when I changed the code or tests

Whenever I have some tests failing I use this command so I don't have to restart the test after every attempted.

```sh
find . -name "*.py" | entr python -m pytest
```

### continously try out my script

```sh
ls nondestructive_script.bash | entr -c bash /_
```

This will run the script whenever I change it (which is nice as long as I am sure that the script doesn't do something terrible).
I can then examin the output and adapt the script.

## `entr` is very unixoid

I really wish it was preinstalled on all unixoid systems. It fits so well into the ecosystem:
- It works on files
- It works on text
- It doesn't produce any output (unless you ask it to)
- it composes well with other programs as stdin (especially `ls`, `find` and `grep --files-with-matches`)

The only strange thing is that the files to watch are specified through stdin.
The interface  doesn't feel like a filter that way, a bit like `xargs cat`
But one gets used to that.

`entr` plays especially well with [make](https://www.gnu.org/software/make/) (because make only acts on files that have changed).

## `entr` a bit more Meta then other commands

`entr` is a meta command, it is intended to call other commands.
This distinguishes it from familiar programms like `tail`, `wc`, `sort`, `uniq`, `curl` etc.
and puts it more in line with `watch`, `time`, `xargs` and `sudo`.

## `entr` is the best

I really like to have `entr` around. Luckily it is available for allmost all linux distros and even macOS.
