---
title: "Digitaler Haushalt"
---

Wer nach dem Urlaub nach Hause kommt muss erstmal einiges an Haushalt erledigen: Sachen einräumen, den Kühlschrank auffüllen, Wäsche waschen, durchfegen, Blumen gießen, sich bei Nachbarn und Freunden melden und sich um die Post kümmern.

Im Digitalen fallen erstaunlich ähnliche Dinge an. Nach einer Woche Offline-Abstinenz in Italien (nur gelegentlich unterbrochen von kurzen Netz-Momenten) geht es ans Liegengebliebene:

* RSS-Nachrichten der letzten Tage durchgehen
    - zumindest grob, vielleicht war ja was interessantes dabei
* RSS-Feeds auffrischen
    - Von ein paar schönen Blogs habe auf der Heimfahrt gelesen, die kommen dazu.
    - Ein paar Nachrichtenseiten gehen mir langsam zu sehr auf den Keks, weg damit.
* Späterlesendienst ausschütteln
    - auf der Fahrt habe ich zwar einiges weggelesen, aber mir ist aufgefallen dass ich schon seit Monaten kein Inbox Zero mehr hatte.
    - Was ich in den letzten 8 Wochen nicht gelesen habe lese ich wohl nicht mehr. Hier darf großzügig gelöscht werden.
* Mails beantworten
    - Zum Glück nicht zu viele, dafür aber ein paar Wichtige.
    - Ham zu Ham, Spam zu Spam
* Sich in den Sozialen Netzwerken blicken lassen
* Urlaubsfotos archivieren
    - Alle Fotos von Allen Urlaubsteilnehmern sollen auch allen zur Verfügung stehen.
    - Wird Zeit dass ich mal eine [Mediagobblin](https://mediagoblin.org/pages/campaign.html) Instanz aufsetzte. Hmmmm, später.
* Frisch geplante Projekte müssen auf Tauglichkeit überprüft und in den Alltag eingebettet werden, ohne dass die Arbeit drunter leidet

Tatsächlich ist es genau wie Hausarbeit: Es tut gut, mal alles liegen zu lassen. Dannach fängt man an und je mehr man tut desto mehr müsste man noch machen. Ganz sauber wird es nie, nur sauberer.
