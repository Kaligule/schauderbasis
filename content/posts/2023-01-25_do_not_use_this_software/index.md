---
title: "Do not use this software"
image: do_not_use.png
---

There is a piece of software on your computer that you shall not use.

It is not broken or alpha-software, it works perfectly fine.
It is part of git after all, the basis of almost all modern and not so modern software.
But have a look at it's manpage and you will find a big warning to please not use it.

I am talking about the command `git filter-branch`.
Just have a look at [it's manpage](https://git-scm.com/docs/git-filter-branch).
I quote:

> WARNING
>
> git filter-branch has a plethora of pitfalls that can produce
> non-obvious manglings of the intended history rewrite (and can leave
> you with little time to investigate such problems since it has such
> abysmal performance). These safety and performance issues cannot be
> backward compatibly fixed and as such, its use is not recommended.
> Please use an alternative history filtering tool such as git
> filter-repo[1]. If you still need to use git filter-branch, please
> carefully read the section called “SAFETY” (and the section called
> “PERFORMANCE”) to learn about the land mines of filter-branch, and
> then vigilantly avoid as many of the hazards listed there as
> reasonably possible.

I read a lot of documentation: manpages, infopages, html-manuals, api-references, docstrings and (occasionaly) even as books.
But I don't remember reading about a piece of software that tries so hard to convince you to not use it, while simultaneously being available on nearly every computer in the world.

I found that quite amusing.
