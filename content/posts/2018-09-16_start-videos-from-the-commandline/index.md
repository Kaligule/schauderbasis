---
title: "Start videos from the commandline"
image: glasses-and-laptop.jpg
---

<!--
credit: Kevin Ku
creditlink: https://unsplash.com/photos/w7ZyuGYNpRQ
-->

> __tldr:__ Just use `mpv VIDEOURL` to start watching.

Lets say you want to watch a video, how would you do it from the
commandline?

## local video file

If the file you want to see is already on your computer, then this
can't be too hard. Just go with your favorite video player (vlc in
this example):

```bash
vlc ~/Videos/Nyan_Cat.mkv
```

## videos from the internet

So what if the video isn't already downloaded? Lets say we want to
look at this [YouTube Video: Nyan Cat](https://www.youtube.com/watch?v=QH2-TGUlwu4).

No problem, we can download it right away with
[youtube-dl](https://github.com/rg3/youtube-dl).

```bash
youtube-dl -o "~/Videos/Nyan_Cat.mkv" "https://www.youtube.com/watch?v=QH2-TGUlwu4" && vlc ~/Videos/Nyan_Cat.mkv
```

The reason why we don't use `curl`/`wget` here is that we don't have
the url of the video file itself (and if we did, we could just
continue to feed it into vlc directly). The video is embedded into a
website and there is no trivial way to get its url. If we ask
youtube-dl nicely (with `--get-url`) he will tell us, but at this
point we have used youtube-dl anyway, so what's the point?

![The video that google uses has a really long url. Something tells me that is isn't meant for direct use.](get-url.jpg)

Youtube-dl is quite amazing. It supports a [great number of video
sites](https://rg3.github.io/youtube-dl/supportedsites.html) and file
formats. It also supports playlists, logins for private videos and
downloading the audio only. Being controlled from the commandline
makes it scriptable, but the best thing about it is that it has been
under [continuous
development](https://github.com/rg3/youtube-dl/graphs/commit-activity)
for many years now, catching up with all the api-changes from all the
supported websites.

## streaming, not downloading

Downloading works, but what if we want to start to watch straight
away, waiting for the download to finish is unnecessary (especially if
the video is a big one, like the 10 hour version of the video above).

Youtube-dl can write the file to stdout (`--output -`) and tell the
video-player to play from stdin (`vlc -` in this case).

```bash
youtube-dl --output - 'https://www.youtube.com/watch?v=wZZ7oFKsKzY' | vlc -
```

Unfortunatelly there is no easy way to jump positions in the videos itself.

## mpv to the rescue

`vlc` has a big fanbase, but for quite some time I preferred to use
`mplayer`, because it has the most minimal gui imaginable (a window
whith the video in it, nothing more) and is easier to use from the
keyboard. _Then_ I found [`mpv`](https://mpv.io/), a pimped version of
mplayer - and everything is perfect. Much like `feh` for images, it does all I want and goes out of the way otherwise. I just love it.

Mpv makes all the problems from above trivial.

```bash
mpv 'https://www.youtube.com/watch?v=wZZ7oFKsKzY'
```

It uses youtube-dl, can show subtitles in different languages, jumps
between positions and has an easy syntax. It is free software (like
all the other programs I talked about in this post) and its
development continues steadily.


## Why not use a browser, like a normal person?

Using a native video-player comes with some cool bonuses. You can
configure and finetune it, use media-keys on your keyboard, it plays
nicely with the rest of your setup and your window-manager etc.

The only downside I see is that the resolution is not adjusted on the
fly if the connection is not good enough to stream in full
resolution. That's why I still use a browser for twitch-streams.




















