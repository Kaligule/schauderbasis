---
title: "Hide the tmux statusbar if only one window is used"
image: wasted_space.png
---

## What?

I want the tmux statusbar to show only if I have multiple tmux-windows open.
When only one tmux-window is open, then the status bar should be hidden.

## Why?

I like tmux, it is awesome.
But I don't use it in most shell sesssions.

My window manager ([swaywm](https://swaywm.org/))
is very good at creating and closing terminals.
I often spawn them for a quick command and close them after a few seconds.
So most of the time I do _not need_ tmux in my terminals.

On the other hand, tmux features a big shiny green statusbar.
That is usefull if I want to manage multiple tmux-windows.
But if I don't do that, then the status bar is distracting and takes up valueable space.
Therefore I do _not want_ that in every terminal I spawn.

![The statusbar is using up valuable space in my terminal.](wasted_space.png)

Then again sometimes I am kneedeep into some project
and then would like to use some tmux features.
But at that time it is somewhat too late.

The best solution would be to have tmux running in every terminal,
but have the status bar only show up if I start using multiple tmux-windows.

## How?

Showing or hiding the statusbar permanently is as easy as
adding one of the following lines to the tmux config file:

```
set -g status on
set -g status off
```

![The status bar is gone, the terminal is empty and full of potential](full_of_potential.png)

I was supprised to find out that automatically hiding the status bar doesn't seem to be a very common thing to want.
I didn't find much on this, but it was quickly clear that I would have to
use [hooks](https://man.openbsd.org/OpenBSD-current/man1/tmux.1#HOOKS)
and [conditionals](https://man.openbsd.org/OpenBSD-current/man1/tmux.1#FORMATS).

Then I found a [github issue](https://github.com/tmux/tmux/issues/659) about my very usecase.
The solution they suggested did only work half of the time unfortunatelly.
(It failed to hide the statusbar when you would
have multiple windows and start closing them from the left.)
But I did manage to get it working by adding more hooks.

Here is the relevant part of my tmux.conf

```
# only show status bar if there is more then one window
set -g status off
set-hook -g after-new-window      'if "[ #{session_windows} -gt 1 ]" "set status on"'
set-hook -g after-kill-pane       'if "[ #{session_windows} -lt 2 ]" "set status off"'
set-hook -g pane-exited           'if "[ #{session_windows} -lt 2 ]" "set status off"'
set-hook -g window-layout-changed 'if "[ #{session_windows} -lt 2 ]" "set status off"'
```

![Multiple windows make the status bar show up.](multiple_windows.png)

Hooks are a bit of a pain in tmux,
I wasn't able to get a list of all active hooks.
Closing the tmux session wouldn't reset the hooks already set.
So I had to reset the hooks by killing tmux (not just the session, tmux itself).

## Done

So now I can have tmux active in every terminal I open.
It won't show the status bar if it is not needed, only if I have more then one window.
I am quite happy with this new setup.
