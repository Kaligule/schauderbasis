---
title: "winner of the first IOPCC"
image: "main.py.png"
---

I am a big fan of the "[The International Obfuscated C Code Contest](https://www.ioccc.org/)" (IOCCC).
Their winners page is littered with creative and (in it's own way) elegant code.
Unfortunately I know just enough C to appreciate the code there, I could never write anything like this myself.

So I was on fire when I heard that there was a **python-version** of the contest:
The [IOPCC](https://pyobfusc.com).
I immediately started to work on a submission.

It was a lot of fun on multiple levels.
I had wanted to do something with abstract syntax trees for a long time,
so I started fooling around with them until I had something sufficiently obscure.
Then I started to condense it, make it as obscure as I could and formed it into an ascii art image.

When I was satisfied I submitted the code and waited.

## What I submitted

Normally code I write would go into a repo,
but this is a piece of art, not a piece of work.
It won't be iterated on anymore, there are no pipelines, no issues.
So I will just link the files here in this blogpost:

- [The code](main.py) is save to run, but you should not trust my word of course.
- [An explanation](remarks.txt) of what it does and how it does it.

The output of the code looks like this:

![The output prints out it's own code, then evaluates the code quality in terms of the zen of python. It gives itself a pretty bad score.](output.png)

## The announcement

I waited and waited.

A few months later [the winners are announced](https://pyobfusc.com/#winners).
And I am one of them! Horray!

Here is what the Judges said about my code:

> Most Introspective.
>
> Very well put together, top-notch obfuscation.
> Tongue-in-cheek, comments on itself (very meta, which we like).

This definitely goes into my resume.
If people see this award winning code they probably won't hire me,
but I don't care.
I am very proud.










