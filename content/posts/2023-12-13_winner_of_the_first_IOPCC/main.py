"""ᐌ([ᐊ(ᐄ(ᐆ('print',ᐇ()),[ᐄ(ᐈ(ᐄ(ᐆ('open',ᐇ()),                   [ᐆ('__file__                           ',ᐇ()),ᐃ('r'
)],ᐉ),'read',ᐇ()),ᐉ,ᐉ)],ᐉ)),ᐊ(ᐄ(ᐆ('print',ᐇ()),ᐉ,ᐉ)),             ᐂ([ᐁ('conte'                         'xtlib'),ᐁ(
'io'),ᐁ('operator'),ᐁ('time')]),Assign([ᐆ('f',ᐋ())],ᐄ(ᐈ(           ᐆ('io',ᐇ()),                       'StringIO',ᐇ
()),ᐉ,ᐉ)),With([withitem(ᐄ(ᐈ(ᐆ('contextlib',ᐇ()),'redirect'         '_stdout',ᐇ(                     )),[ᐆ('f',ᐇ(
))],ᐉ))],[ᐂ(                                   [ᐁ('this')])])        ,For(Tuple([                   ᐆ('i',ᐋ()),ᐆ
('l',ᐋ())],ᐋ                                     ()),ᐄ(ᐆ('enum'       'erate',ᐇ())                 ,[ᐄ(ᐈ(ᐄ(ᐈ(ᐆ(
'f',ᐇ()),'g'                                      'etvalue',ᐇ())       ,ᐉ,ᐉ),'spli'               'tlines',ᐇ()
),ᐉ,ᐉ)],ᐉ),[                                       ᐎ(ᐒ(ᐏ(),ᐆ('i'        ,ᐇ())),[ᐊ(ᐄ(             ᐆ('print',ᐇ(
)),[ᐓ([ᐃ('H'                                       'ow does this'        ' code do i'           'n terms of'
' >'),ᐑ(ᐄ(ᐈ(                                       ᐆ('l',ᐇ()),'r'         'eplace',ᐇ()         ),[ᐃ(','),ᐃ(
'<')],ᐉ),-1)                                      ,ᐃ('?')])],ᐉ))           ,ᐐ()],[ᐎ(ᐒ(ᐏ       (),ᐆ('l',ᐇ()
)),[ᐊ(ᐄ(ᐆ(''                                     'print',ᐇ()),ᐉ,            ᐉ)),ᐐ()],ᐉ)]     ),ᐊ(ᐄ(ᐆ('pr'
'int',ᐇ()),[                                    ᐍ(ᐅ(ᐆ('comment'              ,ᐋ( )),ᐄ( ᐈ(   Dict([ᐃ(11),
ᐃ(12),ᐃ(13),                                 ᐃ(17),ᐃ(19)],[ᐃ(                 'no errors were silenced'
),ᐃ('no err'                           'ors were thrown'),ᐃ(                   'everything is crystal'
' clear'),ᐃ('for this code, never would have been better')                      ,ᐃ('does not apply')]
),'get',ᐇ()),[ᐆ('i',ᐇ())],ᐉ)),ᐄ(ᐈ(ᐓ([ᐃ('\x1b[32m✔ '),ᐑ                           (ᐆ('l',ᐇ()),-1),ᐃ(
'\x1b[0m__#_'),ᐑ(ᐆ('comment',ᐇ()),-1),ᐃ('.')]),'re'                               'place',ᐇ()),[ᐃ(
'_'),ᐃ(' ')],ᐉ),ᐓ([ᐃ('\x1b[91m✘ '),ᐑ(ᐆ('l',ᐇ())                                     ,-1)]))],ᐉ)),
ᐊ(ᐄ(ᐈ(ᐆ('time',ᐇ()),'sleep',ᐇ()),[ᐃ(0.3)],ᐉ                                          ))],ᐉ),ᐊ(ᐄ(ᐆ
('print',ᐇ()                                                                         ),[ᐃ('\x1b['
'0m')],ᐉ)),ᐊ                                                                         (ᐄ(ᐆ('print'
,ᐇ()),[ᐃ('W'                                                                         'ell...')],ᐉ
) ),ᐊ(ᐄ(ᐈ(ᐆ(                                                                         'time',ᐇ()),
'sleep',ᐇ())                                                                         ,[ᐃ(3)],ᐉ)),
ᐊ(ᐄ(ᐆ('prin'                                                                         't',ᐇ()),[ᐃ(
'I tried.')]                                                                         ,ᐉ))],ᐉ)""";
...;from ast                                                                         import *;ᐁ,\
ᐂ,ᐃ,ᐅ,ᐄ,ᐆ,ᐈ,                                                                         ᐇ,ᐌ,ᐉ,ᐋ,ᐊ,ᐍ\
,ᐐ, ᐎ,ᐏ,ᐒ,ᐓ,                                                                         ᐑ = alias ,\
  Import   ,                                                                         Constant  ,\
 NamedExpr ,                                                                         Call, Name,\
 Attribute ,                                                                         Load,Module\
,[], Store ,                                                                         Expr,IfExp,\
Continue,If,                                                                         Not,UnaryOp\
,JoinedStr ,                                                                        FormattedValue
exec(unparse                                                                    (fix_missing_locations
(eval(__doc__                                                                   ))))#Johannes Lippmann
