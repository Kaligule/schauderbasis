---
title: "Woher und wohin"
---

# Von Apple...

Ich bin Mathematikstudent. Seit 2011 benutze ich ein MacbookPro und seit 2012 ein iPhone 4S, ich bin gut eingelebt in der Applewelt. Es gibt vieles, was sehr einfach funktioniert, wenn man nur Appleprodukte verwendet. In den letzten Jahren habe ich mir keine Gedanken machen müssen über:

* Treiber
* Festplattendefragmentierung
* wo man Sachen einstellt
* das Innere meines Computers
* Viren
* wie man etwas mit der Konsole macht (außer ich wollte es)
* Kompatibilität
* woher ich Software bekomme

Einen Teil davon, vielleicht sogar alles, werde ich in der nächsten Zeit aufgeben müssen und ich habe schon ein wenig Angst.

# ... zu Linux

Es wird schwierig, unbequem und ein Haufen Arbeit. Manches/vieles/alles wird komplizierter werden und nicht so gut funktionieren wie davor - zumindest in der ersten Zeit.

Ich bin bereit, mich da reinzuhängen, aber nicht uneingeschränkt. Wenn sich alles als zu schwierig herausstellen sollte werde ich zurückrudern. Wenn Sachen nicht funktionieren, muss ich auf meine alten Systeme zurückgreifen.

Im Idealfall bekomme ich ein mächtiges und schnelles System mit genau dem, was ich für nötig halte und mit großer Kontrolle über die einzelnen Komponenten. Im schlimmsten Fall muss ich das gesamte Projekt als Fehlschlag verzeichnen und kehre zurück zu alten Gewohnheiten.