---
title: "An der Weboberfläche kratzen"
image: scratch.jpg
---

<!-- https://pixabay.com/de/photos/katze-kratzen-sone-haustier-pfote-750259/ -->

Ich mag Programmieren, aber mit Netzwerk, Browser, Javascript und Webseiten kenne ich mich nicht gut aus. Wenn ich dann einen Fuß ins kalte Wasser halte, ist es schon spannend (für mich). Hier mein jüngstes Erlebnis:

## Bilder in Ghost

Meine Freundin hat, wie ich, einen Ghostblog. Anders als ich schreibt sie dort regelmäßig und oft, mit vielen Bildern über Abenteuer in der Küche. Neulich konnte sie plötzlich keine Bilder mehr hochladen.

![Something went wrong](something_went_wrong.png)

### Fehlerjagd

Fehler eingrenzen:

- Die Fehlermeldung ist wenig hilfreich.
- So wenig hilfreich, das man nicht mal sinvoll danach googlen kann.
- Till und ich haben Blogs auf dem gleichen Server, können aber Bilder hochladen.
- Auf Sandras Blog kann man keine Bilder hochladen, egal ob von ihrem oder meinem Account.
- Firefox, Safari oder Chrome - kein Unterschied
- Verschiedene Dateitypen oder kleinere Bilder ändern nichts an der Fehlermeldung.
- Die offizielle Ghost Dokumentation ist lausig. Immerhin gibt es einen sehr kurzen Abschnitt über [Image upload issues](https://support.ghost.org/troubleshooting/#image-upload-issues). Nichts Hilfreiches. Das angesprochene Cloudflair benutzen wir nicht.


Ideen, woran es liegen könnte:

- Sandras Blog ist von allen mir bekannten Ghostblogs der mit dem meisten Content, insbesondere der mit den meisten Bildern. Vielleicht gibt es da irgendwo ein Quota, dass erreicht ist?
- Sandras Blog ist der mit dem Umlaut in der url ("Gewürzrevolver"). So bitter es ist, das hat bis jetzt einige Probleme gemacht. Liegt es vielleicht daran?

Mehr kann ich jetzt alleine nicht rausfinden, ab hier brauche ich Ansprechpartner.

### Mal fragen

Ich befrage Till (der Herr über den Server) und den Ghost Slack Channel [help](https://ghost.slack.com/messages/help/), wo ein [Kevin](https://ghost.slack.com/team/kevin) mir freundlich zur Seite steht.

_"Gibt es auf unserem Server irgendeine Begrenzung des Speicherplatzes?"_

Nein, Till weiß von keinem Quota und ein mysteriöses künstliches Quota bei Ghost scheint auch nicht zu existieren.

_"Wo soll ich nach Fehlern suchen?"_

Schau mal in den Web Inspektor.

Wie immer, wenn ich mit dem Support spreche google ich nebenbei, was die Antworten bedeuten. Den Webinspektor ist das Tor in die Javascript-Hölle. _Here be dragons_. Aber _here be also a log of what happens in your browser when you interact with a website_.

![Webinspektor öffnen](Inspect_element.png)


Ich öffne also den Webinspector und versuche, ein Bild auf den Blog hochzuladen. Tatsächlich finde ich nach ein bisschen herumsuchen eine Fehlermeldung.

![So sieht ein Fehler auf einer Webseite aus](webkonsole.png)

![Die Fehlermeldung](error.png)

Die Fehler kommt in Form eines JSON Objektes, die entscheidende Zeile ist: 

```
EACCES, open '/var/www/ghost/revolver/content/images/2016/04/Organigram-svg.png'
```

Sehr hilfreich sieht das erstmal nicht aus. Aber wenn man mal schaut, was EACCES überhaupt heißt, bekommt man heraus dass es sich wohl um einen permission error in dem angegebenen Ordner handelt. (Diese Schlussfolgerung zieht eigentlich der hilfreiche Kevin aus dem Ghost Chat, ich plappere das nur fleißig nach).

Also nochmal dem Till geschrieben, mit der Bitte die Rechte in dem Ordner zu checken und - tatsächlich, das war es. Der Upload funktioniert jetzt wieder und Sandra kann ihren neuen Blogpost mit Bildern ausstatten ([Spargel](https://gewürzrevolver.de/sie-sind-wieder-da-spargel-und-rhabarber/), Huäääääääh!).



## Abschliessende Gedanken:
- Fehlermeldungen sollten immer aus zwei (!) Teilen bestehen: Eine technische (für den Entwickler) und eine menschenlesbare für den User.
- "Something went wrong." ist keine gute Fehlermeldung. Da ist _keine_ nützliche Information drin. Dass es nicht geklappt hat sieht der User auch so.
- Der Fehlersuche war _nicht_ qualvoll und nervig, denn alle waren geduldig und haben sich professionell verhalten. So macht das Spaß, danke an Till und Kevin.
- Wie so oft bei Computerproblemen gilt: Wenn du noch nicht wirklich selbst versucht hast, das Problem zu lößen, dann ist es nicht angebracht anderer Leute wertvolle Zeit dafür in Anspruch zu nehmen.
- Keiner weiß, wie das mit den Rechten passiert ist (mit den Rechten weiß man das nie so recht). Till war es nicht und auch sonst keiner. Auf dem Server spukt es.

(Titelbild von [hier](https://pixabay.com/en/cat-scratching-sun-pet-scratch-750259/), Pixabay ist eine tolle Quelle für freie Bilder)
