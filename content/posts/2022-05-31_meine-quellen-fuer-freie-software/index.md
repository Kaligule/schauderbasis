---
title: Meine Quellen für die Welt der freien Software
image: sources.jpg
---

<!--
Image source: https://unsplash.com/photos/vgQFlPq8tVQ
Photographer: Artem Maltsev (https://www.instagram.com/art.maltsev_photo/)
License: Unsplash
-->

Ich komme zur Zeit nicht viel zum schreiben.
Dafür gibts gute Gründe, aber manchmal macht es mir etwas aus.
Es ist auch nicht so, dass es nichts zu erzählen gäbe, aber die Zeit...

Naja, hier ist eine kurze Liste von Feeds,
mit denen ich mit der Welt der freien Software in Kontakt bleibe.
Selbst dann, wenn ich wenig Zeit habe.

Blogs, die häufig geupdated werden.
- [GNU/Linux.ch](https://gnulinux.ch):
  Meine Lieblingsnewseite zur Zeit.
  Sehr offene Community, richtig stark.
  Für mich ist es ein gefühlter Nachfolger von [Pro-Linux](https://www.pro-linux.de/).
- [Linux News](https://linuxnews.de/)
- [Emacs news](https://sachachua.com/blog/category/emacs-news/) (english):
  Die Emacs Community ist stark, aber auf viele Plattformen verteilt.
  Die unsterbliche Sacha Chua sammelt einmal die Woche alle interessante Sachen zusammen.
Podcasts:
- [Binaergewitter](https://blog.binaergewitter.de/):
  Laberrunde mit fittem Team, die viel Informationen zusammentragen und einordnen.
- [Python bytes](https://pythonbytes.fm/) (english):
  Die finden alles amazing, solange nur das Wort "Python" darin vorkommt.
  In eigentlich jedem Podcast sind ein oder zwei feine Fundstücke drin.
- [Radio Tux](https://www.radiotux.de/):
  Fühlt sich ein bisschen wie ein Magazin an.
Auf dem Radar:
- [LinuxWeeklyNews](https://lwn.net/) (english):
  Einer der sehr wenigen bezahlten Feeds, die für mich attraktiv sind.
  Sehr technisch und sehr in die Tiefe.

Außerdem floge ich noch etwa 150 privaten Blogs
(zb
[Xe Iaso](https://xeiaso.net/blog),
[Drew DeVault](https://drewdevault.com/),
[Brian Moses](https://blog.briancmoses.com)
)
und den "offiziellen" Blogs von etwa 80 Projekten, die mich speziell interessieren
(zb
[Godot](https://godotengine.org/news),
[Thunderbird](https://blog.thunderbird.net/),
[Neovim](https://neovim.io/news)
)
Jeder schreibt selten, aber zusammengenommen gibt es jeden Tag was zu lesen.

(So vielen Quellen gleichzeitig zu folgen ist natürlich nur möglich,
wenn man RSS/Atom-Feeds benutzt.)

Wenn ich das hier so aufschreibe fällt mir auf,
dass fast alle deutschsprachig sind.
Eigentlich cool, dass es da so viel deutsches Material gibt.
Ich wäre aber auch an mehr guten englischsprachigen Blogs interessiert.
