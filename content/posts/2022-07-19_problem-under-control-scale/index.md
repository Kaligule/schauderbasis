---
title: How much control over the problem do you have?
image: pocket-slide.jpg
---

<!--
Image source: https://unsplash.com/photos/2XLqS8D0FKc
Photographer: Roberto Sorin (https://stock.adobe.com/contributor/204016507/Roberto%20Sorin)  "
License: Unsplash
-->

There is a problem and a team who should do something about it.
That team wants to communicate to others **how much control over the problem** they have.
So they draw a scale, we will call it the **problem-under-control-scale**.

Here are some points they might mark on it (sorted roughly from bad to good):

- We don't know there is a problem.
- We suspect there is a problem.
- We know there is a problem.
- We know there is a problem and how bad it is.
- We know there is a problem and how to find out more about it.
- We know where the cause of the problem lies.
- We were able to reproduce the problem.
- We know the cause of the problem.
- We understand the problem.
- We are looking for solutions.
- We know there is a solution.
- We know where to find a solution.
- We have an idea how a solution might look like.
- We are choosing between multiple apparent solutions.
- We know how we want to solve the problem.
- We are implementing a solution.
- We are testing a solution.
- We have solved the problem for now.
- We have solved the problem.
- We have solved the problem and learned from it.
- We have solved the problem and documented our learnings.
- We made sure problems like this can be solved easily in the future.
- We made sure problems like this can not happen anymore.

(I am sure there are more possible steps.
For example, what if additional help is needed at any point?)

## What to use the problem-under-control-scale for

I like this problem-under-control-scale, because it allows to think about problems in an abstract way.

- One of my favorite topics for talks/blogpost is "How we once improved our position on that scale".
  For example:
  - "How we went from `We know there is a problem.` to `We have solved the problem.`."
- In your teams' update meeting you probably like to talk about `We are choosing between multiple apparent solutions.` or better.
  But really important would be to talk about everything worse then `We understand the problem.`.
- Quite often people stop when they got to `We have solved the problem for now.`,
  when they really should push further down the scale.
- Open your favorite news-site.
  Most of them articles will be about problems.
  At which point are they on the problem-under-control-scale?
- You could also try to categorize posts on social media this way.
- Note how the first third of the scale doesn't mention solutions at all.
  Don't jump to solutions straight away.
  Sometimes knowing the problem might even be enough.
- Most ads try to sell you a solution to a problem you didn't really have
  (at least you made it so far without the advertised product).
  So they have to get you from `We don’t know there is a problem.` all the way to at least `We know where to find a solution.`
  (but not accidentally to `We have solved the problem for now.` or better, because that wouldn't sell anything).
  Try to see how they manage that next time you see an ad.
- Most jobs pay so the employee solves some kind of problem.
  When you get a problem to deal with, where is it on the `problem-under-control-scale`?
  Close to `We suspect there is a problem.`?
  (You are a detective then, cool!)
  Or more at `We know how we want to solve the problem.`?
  (... and we want you to implement that solution)

## What else could we do with the scale?

I think you could get some nice graphs if you pair the problem-under-control-scale with some other scale.
Time perhaps?
Or costs?
Expertise_needed?

![](graph.jpg)
<!--
Source: https://unsplash.com/photos/AT77Q0Njnt0
Photographer: Isaac Smith (https://frontierjournal.net/)
License: Unsplash
   -->

You would then plot the the course of your problems on the resulting coordinate system.
This could help to answer questions like:

- Which step in the problem solving journey takes the most time/effort?
- Where do you get stuck most often?
- Should you perhaps approach your problems in a different way to get stuck less often?


