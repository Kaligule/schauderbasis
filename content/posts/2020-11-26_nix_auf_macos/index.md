---
title: "Nix on macOS"
image: into_the_ice.jpg
---

<!--
credit: Annie Spratt
creditlink: https://unsplash.com/photos/J8U87CjWAlo
-->


Nix is packagemanager like nothing I have seen anywhere else (I have suspicion that [Guix](https://guix.gnu.org/) goes in a similar direction, but I haven't tried it out yet). Using it is a bit similar to using a package manager for the first time: Once you have seen the light, there is no coming back.

That's why I have Nix running on 3 machines currently: My main Laptop, my "home-server"-raspberry-pi and (since this weeks) my MacBook for work (I am in the happy situation that my current employer allows devs much freedom on their work-devices).

Nix can't replace [brew](https://brew.sh/) completely yet, just because brew has so many packages that are not available via Nix. I am not yet at the point where I can create my own Nix-packages, so for more special things I still rely on brew. But for everything else...

After installing (two great blogposts about that: [1](https://wickedchicken.github.io/post/macos-nix-setup/), [2](https://dev.to/louy2/installing-nix-on-macos-catalina-2acb)) there are two ways I make use of Nix:

1. listing `~/.nixpkgs/darwin-configuration.nix` to list the packages I want (similar to NixOS)
2. using `nix-shell` to create development-environments (similar to `virtualenv` in python.)

I have not yet suggested using nix to my coworkers, but if it continues to be amazing as it is, I think I will.


















