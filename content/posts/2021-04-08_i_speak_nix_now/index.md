---
title: "I speak nix now"
image: control.jpg
---

<!--
credit: Photo by Chris Leipelt on Unsplash
creditlink: https://unsplash.com/photos/4UgUpo3YdKk
-->

After living in a love-hate relationship with my nixos-configuration it finally came to the point that it became too big of a file to handle.
I already had it versioned (nearly everything I write is versioned).
But now the file approached a few hundred lines at I was going to loose my grip on it.

A few hundred lines of code is nothing.
But a few hundred lines of config is a whole lot.
Especially if you don't know the configuration language well.
Or if the config is about very different aspects of a complicated system.

I know I had to find a solution. There were two obvious ones:

1. Manage the config in org-mode (literate-config-programming) as I do with my emacs config.
2. Learn more about the nix-language and split the file up into modules.

I knew I would have to make a decision and I dragged it around with me for about a week.
Then a night came where I couldn't sleep and I used it to read a lot about nix, especially [this article](https://cbailey.co.uk/posts/drinking_the_nixos_kool_aid).
And finally it clicked.

### I had a breaktrough.

When I finally understood how `import` function works in nix I was blown away in a storm of motivation.
Over the next days I rewrote my config into a series of modules, nice and separated.
(I can only work in the evening on stuff like that, often no more then 20 minutes a day.)
I learned a lot about nix in the process, a lot of pieces fell into their place.
Now my laptop and my server share many modules and it is easy to add new ones without adding much complexity.

It feels great.

This has happened before.
I declared emacs-config-bankruptcy multiple times before I finally _got it_ and brought it all into a nice, organized format.
Since then the config grew and grew, is counts 2000 dense lines of config and holds over 40 chapters on minor-modes alone.
Yet I don't fear touching anything in it.
I even read other peoples configs from time to time, taking good ideas from them.

My emacs config is under control.
And now my nixos-config is, too.
