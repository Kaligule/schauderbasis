---
title: "I quit Social Media"
image: chains.jpg
---

<!-- credit: "Zulmaury Saavedra" -->
<!-- creditlink: https://unsplash.com/photos/zh0J32MrJfA -->
<!-- cropped by me -->

I get addicted to social media quite easily.
It doesn't do me good, I tend to sink a lot of time into it.

I usually use only one network at a time.
I have had accounts on:

- _Facebook_: because all my classmates were there
- _Twitter_: because people there were so funny
- _[App.net](https://en.wikipedia.org/wiki/App.net)_: because Tim Pritlove promoted it
- _Reddit_: (because many interesting communities live there)

Each of those was hard for me to quit.

I also tipped my toes into the Fediverse,
a collection of decentralized social networks:
[GNU social](https://gnu.io/social/) (almost the same as [Mastodon](https://joinmastodon.org/) today) and [Friendica](https://friendi.ca/) (which I even hosted myself).
While there were some very interesting communities there
(mostly nerds and trans-people) I never got hooked enough to stay.
The reason might be that those networks don't work so hard to make you addicted to them,
which is a good thing.

## What I didn't quit yet

I still watch a lot of **Youtube**, but I don't "like", "subscribe" or "comment" there,
so it is not really a *social* medium for me, just a medium.
I use it as a portal for video-podcasts.
At some point I will probably have to reduce this, too.

I read *a lot* of **Blogs** (and similars) via my feedreader.
The open web is just the best.
And I don't think I will ever want to stop that.
My share of it is [this blog](https://schauderbasis.de) you are reading right now.

I do use **mail**, **messenging** and **phones**.
But those are not social media to me, I would call them social networks.

## How to quit

From time to time I realize that I have a problem with an addictive website. And some time later I get a "It's time to quit"-moment.
What works for me then is quiting cold turkey, by doing some of those:

- Log out on every device
- Delete the account
- Delete the site from my browser history (so I don't get suggestions when typing an url)
- Block the site via DNS
- Tell my wife/friends that I quit

This has worked well for me, and after a week or so I rarely feel the need to go back.
