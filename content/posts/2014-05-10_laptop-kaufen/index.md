---
title: "Laptop kaufen"
---

## Warum?
Für mein Linux Projekt wollte ich einen neuen Computer. Mein treuer Begleiter bisher war ein 13 Zoll MacBook Pro (Early 2011). Der ist auch noch gut, für den Wechsel gab es aber mehrere Gründe:

* Ich habe schon einmal (2010) Linux (Ubuntu 10.10) auf einem MacBook laufen gehabt. Es war die Katastrophe: Laut, schlechte Hardwareunterstützung und die Batterie hielt nur noch 2 Stunden. So etwas wäre also schade um die Hardware, die mit OSX noch hervoragend läuft.
* Meine Freundin braucht einen neuen Computer und möchte von Windows auf OSX umsteigen (dazu vielleicht ein andermal mehr). Das MacBook wird also weitergenutzt und ich spare mir das umständliche weiterverkaufen.
* Ein MacBook kann man quasi nicht upgraden. Ich möchte gerne einen Computer, bei dem das möglich ist.



## Wie man einen Laptop auswählt

Ich habe [Till](www.tillmail.de) gefragt und er hat mit mir einen ausgesucht. Till kann so etwas sehr gut und ich beschreibe kurz den Vorgang:

1. Man überlegt sich *vorher* worauf man Wert legt. In meinem Fall war das:
	* möglichst viele Teile sollen austauschbar sein (Akku, Speicher, Festplatten, SSDs ect)
	* Gewicht (ich bin viel unterwegs und nehme meinen Computer quasi immer mit)
	* Bildschirm (ordentliche Auflößung, matt)
	* 13 Zoll hat sich in der Uni bewärt. Damit passt man auf jeden Tisch und die Bildschirm ist groß genug zum ordentlich arbeiten
	* Weniger als 8 GB Ram sind 2014 lächerlich
	* Wie viel will ich ausgeben?
2. Man sucht die Website [Geizhalz](https://geizhals.at/?cat=nb) auf. Dort hat man eine große Auswahl an Hardware und ein erstaunlich gut funktionierendes Filtersystem, mit dem man seine Wahl von 1. sehr genau einstellen kann.
3. Man betrachtet die übrig gebliebenen Geräte und trägt mögliche Kandidaten in eine Liste ein. Bei zu wenig Auswahl lockert man den Filter ein wenig, aber nicht zu sehr. Wenn man im Vorhinein schon Kandidaren hat kommen die auch auf die Liste.
4. Die Liste wird zur Tabelle erweitert. Technische Details werden von den offizillen Seiten zusammengsucht, Besonderheiten aufgeschrieben. Unterpunkte sind zum Beispiel:

	* Vorteile
	* Nachteile
	* Preis
	* Auflösung
	* Prozessor
	* ...

5. Jetzt wird es schwierig. Bei mir blieben 3 Modelle zur Auswahl, bei anderen vielleicht mehr. Wir haben Testberichte gelesen und diskutiert. Welches Gerät passt am besten, welche Nachteile wiegen am schwersten? Wo bin ich am ehesten bereit für Abstriche? Hier ist es wirklich gut eine zweite Person dabei zu haben - besonders wenn sie einen kennt und technische Expertise hat.

Und zum Schluss schläft man eine Nacht drüber und bestellt ihn. So einfach ist das.

## Noch ein paar Anmerkungen

* Die vorgestellte Methode hat mir sehr geholfen aus der unüberschaubaren Flut von angeboten eine Wahl zu treffen, mit der ich zufrieden bin. Ich empfehle sie hiermit weiter.
* Es hilft, im Elektronikmarkt auch mal ein paar Geräte angeschaut zu haben.
* Als Student bekommmt man Rabatte, als Berufstätiger kann man das Gerät eventuell absetzen.
* Die Rechnungen hebt man in jedem Fall gut auf.