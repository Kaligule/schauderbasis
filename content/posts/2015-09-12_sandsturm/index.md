---
title: "Sandsturm"
image: Sandstorm-1.jpg
---

<!-- https://pixabay.com/de/photos/d%c3%bcnen-w%c3%bcste-h%c3%bcgel-windig-sand-691431/ -->

Server haben ist anstrengend und man muss viel Geheimwissen haben. Außerdem funktioniert dauernd was nicht und wenn man mal was austesten will muss man ewige Dokumentation lesen, bis etwas geht. Ich bin schon oft gescheitert.

[Sandstorm](https://sandstorm.io/) ist eine freie Software mit dem Ziel, Server so einfach zu machen wie Handys.

Alles wird über eine Website geregelt, die Knöpfe sind groß und gut beschriftet. Konsolen, Deamons, Prozesse sucht man vergebens, statt dessen gibt es "Apps" und einen "App Market". Zum installieren sucht man sich etwas aus und drückt auf _INSTALL_ ... und wenige Sekunden später ist alles fertig, wie von Magie.

Ich setzte große Hoffnungen in Sandstorm. Die Entwickler schreiben kluge Sachen in [ihren Blog](https://blog.sandstorm.io/) und haben scheinbar wirklich verstanden, warum die meisten Leute keine Sachen selbst hosten. Außerdem ist Sandstorm freie Software. Hach...
Es macht viel Freude das Projekt zu beobachten.

## Welche Apps?

Im Moment ist hier nur freie Software vertreten. Das Angebot ist noch übersichtlich, aber es gibt schon jetzt einige Highlights:

* [Gitlab](https://gitlab.com) - eine freie (und mächtigere) Alternative zu Github
* [Ghost](https://ghost.org/) - dieses Blog läuft mit Ghost
* [ttrss](https://tt-rss.org/) - habe ich [schonmal verbloggt](https://www.schauderbasis.de/rss-software/).
* [Mediagoblin](https://mediagoblin.com/) - ein bisschen wie flickr, aber dezentral und offen
* [ShareLaTeX](https://www.sharelatex.com/) - online LaTeX, mit anderen gleichzeitig, ein bisschen wie Google Docs
* [Roundcube](https://roundcube.net/) - freier Mailclient

...

Da findet sich schon viel Tolles. Außerdem kann man natürlich eigenes Zeug verwalten, es ist ja immerhin auch der eigene Server.

## Zu Gast in der Oase

> "Und wenn ich keinen Server habe?"

![Bild von Matthew Paulson: https://www.flickr.com/photos/matthewpaulson/6728990349/in/photolist-bfBQoZ-bVeY97-ien8YV-hxnF1V-oR1pBo-97uBDx-i3mYgz-ozmMmN-caXPXS-oEuDH8-51i1C-frNDjN-ecxLgm-83Hxvk-7yWSbn-boqYmE-8D5fF3-pD3UnD-npCduL-hJucEG-d9z1bN-69a11F-eefMTq-c3Kno-ecPFAy-e91KsR-k8aRZx-fgT2sX-fXzsRx-6qjJyE-6tbPUX-fk61uu-d8346S-hM9J51-sL9wfm-hjS6iJ-pE6UGe-dUsKKX-etxp1K-piZpV8-prgwZ2-pegSdF-oqmzar-dMpFUd-a6vLv1-7fN8q2-djF9Vn-aWRAeR-d26jgJ-gjy6sB](Oasis.jpg)

Man kann sich seinen Sandstorm auch hosten lassen. Das heißt dann [Oasis](https://blog.sandstorm.io/) und ist wärend der Beta-phase kostenlos.

## Ein Beispiel mit ganz vielen Bildern

Hier ist eine Anleitung, wie man Gitlab auf Sandstorm aufsetzt und ein bestehendes Repository hinzufügt. Es sind extra extra viele Bilder dabei

1. Man begibt sich auf seine Sandstorm Instanz oder auf die [Oase](https://oasis.sandstorm.io/) und meldet sich an. Beim ersten Anmelden darf man eventuell noch ein Profil anlegen.

2. Wir sind drin und sehen ein paar Flächen, die recht selbst erklärend sind.

  ![](Screenshot_2015-09-11_21-20-16_963x465.png)

3. Wir wollen Gitlab installieren, also klicken wir auf das große dicke __+__
 und sehen gelangen in den _App Market_. Welche App darf es sein?

  ![Welche App darf es sein?](App_Market.png)

4. Wir entscheiden uns für "Gitlab" und bekommen nochmal eine Beschreibung der App.

  ![Sieht schon extrem nach Appstore aus.](Gitlab_Beschreibung.jpg)

5. Wir klicken auf _INSTALL_.
  Fertig. Weil wir ohnehin nur freie Software benutzten müssen wir uns nicht mit einem Zahlungsprozess aufhalten (Wir werden später eine Spende erwägen). Das wars. Mehr müssen wir nicht machen. Die App ist installiert und bereit zum benutzt werden.

  ![](Gitlab_installiert.jpg)

Keiner dieser fünf Schritte unterscheidet sich vom installieren von Apps auf dem iPhone. Wir klicken noch auf das App Icon und sind direkt dabei:

![](Gitlab_inside.jpg)


#### Noch ein Wort zu Gitlab
Um ein bestehendes Repository hinzuzufügen klickt man sucht man aus der Box die URL des Repos heraus, geht in den Ordner mit dem git Projekt und gibt diese beiden Befehle ein:


```
git remote add gitlab https://url-zu-meinem-repo.git
git push gitlab master
```

Das hat aber mit Sandstorm nun wirklich nichts zu tun.

<!--
https://unsplash.com/photos/29SqSdfvN_A
https://unsplash.com/@forrestcavale
-->
![Ab in die Wüste!](into-the-desert.jpg)

#### tldr:

Sandstorm ist könnte alles ändern. Mit diesem Userinterface könnten auch nicht-Nerds Server bedienen. Das Argument "Das ist mir alles viiiiell zu kompliziert" könnte ausgehebelt werden, denn es macht sogar Spaß, in den Apps herum zu stöbern.
