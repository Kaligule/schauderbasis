---
title: "Emacs lernen, Tag 3, 4 und 5"
---

# Emacs lernen, Tag 3

* bin wieder dabei, los gehts
* versuche, einen eingebauten Markdown-Mode zu finden. Es scheint keinen zu geben - wo kann ich Modes kennen lernen? Schau ich später nach, jetzt mach ich noch den Rest vom Tutorial zu Ende.
* Dokumentation zu Befehlen gibt es mit ```C-h c``` für den Funktionennamen (was oft ausreicht) und mit ```C-h k BEFEHL``` für ein ausführlichere Beschreibung.
* Heute komme ich nicht recht vorran, andere Sachen wollen erledigt werden.
* Tutorial ist fertig. Kam nichts Interesantes mehr.
* Was mache ich jetzt? Ich könnte mich wieder mit dem Handbuch beschäftigen. Oder ich schaue mal, welche Modes ich so brauchen könnte.
* Ich schau mal nach Modes. Für Haskell gibt es eine [tolle Anleitung ](https://github.com/serras/emacs-haskell-tutorial/blob/master/tutorial.md), aber die ist so lang, dass ich das nach hinten verschiebe. Erstmal Markdown.
* Ein Markdownpackage ist scheinbar nicht installiert, zumindest finde ich mit ```M-x``` nichts. Auch der Packetmanager findet nichts, das auf _emacs_ und _markdown_ matcht.
* Es gibt ein Emacs-Wiki mit einem [Eintrag zu einem Markdown Mode](https://www.emacswiki.org/emacs/MarkdownMode). Darin auch ein Link zur [Website des Entwickler](https://jblevins.org/projects/markdown-mode/) dieses Moduses.
* Aha, ich soll emacs-goodies installieren. Kein Problem.
* Der Emacs muss neu gestartet werden, damit die Completion den markdown modus kennt. Dann endlich ist es geschaft - ich hab Markdown-Syntax-Highlighting.
* Der Markdown-Modus ist nicht so schmeichelhaft wie ich erhofft hatte. Wenn ich eine Liste mache und enter drücke, muss ich das ```* ``` selbst tippen. Das ärgert mich ein bisschen. Immerhin gibt es nur 2 sinnvolle Dinge, die ich dann manchen wollen kann: Die Liste mit einem neuen Element fortsetzten (```* ```) oder die Liste beenden (zwei Newlines).

Für heute muss ich Schluss machen, gibt viel zu tun.

# Emacs lernen, Tag 4

* So, jetzt würde ich gerne etwas programmieren. In Haskell
* Mache ein neues File auf und fange an zu tippen.
* Nach zwei Zeilen merke ich, dass das ohne Syntaxhighlighting keinen Spaß macht.
* rufe [https://github.com/serras/emacs-haskell-tutorial/blob/master/tutorial.md](die Anleitung zum Haskellcodeschreiben im Emacs) auf. Hoffentlich funktioniert das jetzt schön flott.
* Ich soll ```(find-file user-init-file)``` evaluieren. Nach dem dritten Versuch kommt mir, dass ich die Klammern mit tippen muss. Wahrscheinlich ist das so bei Lisp.
* Ich benutze heute mal den Graphischen Emacs um zumindest einer Menubar zu haben, mit der ich etwas anfangen kann.
* Installiere den Haskell mode. Tada!
* Beim Rumklicken im Menü stelle ich fest, dass es das Tutorium auch auf Deutsch gegeben hätte. Super.
* Programmiere hin und her. Mir fehlen noch viele der wichtigen Shortcuts, aber ich hab schon wirklich Schwierigkeiten mit dem Courser. Immer wieder springe ich an stellen, wo ich nicht hin wollte.
* Immerhin der Shortcut für Zwischenspeichern sitzt schon bombenfest. Das macht ja Hoffnung für den Rest.
* Es hilft auch nicht dass das, was ich gerne coden würde nicht funktioniert.

---
Pause

---

# Emacs lernen, Tag 5

* Ein paar Tage sind vergangen seit ich das letzte mal einen Editor angefasst habe. Die Uni hat ein bisschen Zeit gefordert.
* Es ist seltsam: Ich kann mich seit dem letzten mal an kaum einen Shortcut erinnern, aber wenn ich sie brauche sind sie da. Eigentlich cool, hoffentlich klappt das ab jetzt ja immer so.
* Kein Syntaxhighlighting. Wie wechselt man nochmal den Modus?
* ```C-x m```, und der Editor erwartet von mir, dass ich eine Mail schreibe. Seltsam, das war es wohl nicht.
* Immer noch nicht gefunden, dafür (in der graphischen Variante) unter 'Options' irgendwo Themes gefunden. Von den dort verfügbaren hat mir keines gut gefallen. Darum kümmere ich mich später.
* Schön: Der Schortcut ```C-l``` scrollt so, dass der Courser in der Mitte ist - auch wenn das File noch gar nicht so lang ist. Wenn man einen Text schreibt ist das ganz angenehm: Oben das Geschriebene, unten noch leerer Platz. Das fühlt sich richtig an, ist für Code aber nicht nützlich.

![](C-l.png)

* Etwas Recherche: In ```~/.emacs``` kann man auch konfigurieren, das Files mit bestimmten Endungen (.md oder .hs) in bestimmten modi geöffnet werden.
* Gleichmal alles mit git versionieren. Ich hab ja [schonmal geschrieben](https://blog.schauderbasis.de/meine-configs/), dass ich meine Configs auf Github hochlade. Die .emacs [liegt hier](https://github.com/Kaligule/Configs-in-home/blob/master/.emacs).
* Arbeite jetzt zum ersten mal sinnvoll mit zweigeteiltem Editor. Einmal um diesen Text zu schreiben, der andere Teil ist für ein Haskellprogramm. Damit kann ich mir jetzt schön Stück für Stück meinen Haskellmode konfiguriern.
* Was sich als etwas schwieriger herausstellt als gedacht.
* Jetzt habe ich eine Promt im Emacs aufgemacht (cool, dass das geht) und bekomme den Rahmen nicht mehr zu.
* Habs jetzt doch geschafft. Man geht in den Befehlsmods (```M-x```) und tippt "delete-window" ein. Bin ich sogar selbst drauf gekommen :)

Für mich reichts jetzt, ich geh ins Bett.

# Bilanz der letzten Tage

Trotz längerer Pause habe ich das Gefühl, dass die zumindest die grundlegensten Befehle funktionieren. Emacs fühlt sich immernoch wie rießiges Monstrum an, dass ich nicht im Ansatz verstehe. Aber immerhin durfte ich schon ein bisschen von den Vorzügen kosten.
Ich werde jetzt mal meinen Primäreditor auf Emacs umstellen und sehen, ob sich das bewährt.
