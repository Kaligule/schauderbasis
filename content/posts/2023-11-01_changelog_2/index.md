---
title: "Changelog 2"
---

## git showpast

I wanted to write a command-line program that would show you the history of some code within a git repository.

- When/How/By whom was it written?
- When/How/By whom was it slightly changed?
- When/How/By whom was it moved?

I know `git blame`, but this is tedious to use and I still haven't got the hang of it.

I spent some time deciding on which tools I would like to use for this project.
- Python, because it is the best to get something done?
- Haskell, because it is the best to write logic in.
  This is one of the rare "much logic, little IO"-problems, which is where haskell normally shines.
- Rust, because I want to get better in rust.

I decided to go for rust (and the excellent [git2](https://docs.rs/git2/latest/git2/) crate).
Rust is so cool to work with, because of the strict compiler.
I really enjoy that.

I even started reading the [git mailing list](https://lore.kernel.org/git/) a bit.
It was very interesting, but not ultimately useful to my course.

The project died very early, though, with very little code to show for it.
Other projects got more interesting to me.

## music streaming

I finally got [navidrome](https://www.navidrome.org/) on my server going.
It is a self-hosted music streaming service.

I dislike the loss of control that streaming-services like Spotify etc bring with them.
But a selfhosted version?
I can get behind that.

Navidrome is cool for multiple reasons.
- It only reads my music directory and doesn't change it
  (This is the best.
  I can work with files and navidrome will discover changes and update it's internal database.)
- There is a web UI, but every subsonic-compatible client will work (there are a lot of those).
- Installation on NixOs is just a few lines in the config.
  Getting this right is hard (for me) but once it works it is so consistent, dense and elegant.

Next steps:
- move all my music into one central place for navidrome to read
- add metadata to all the files because navidrome doesn't really care for directories.
  ([mp3info](https://ibiblio.org/mp3info/) is use useful for that)
- Install clients on all my devices
- Make it available to others in the house

## address book analysis

I wanted to do an analysis and visualization of my address book.
I wrote a [blog post](https://schauderbasis.de/posts/vcf_confusion/) about a mistake I made in that process.
There was a cool parser package for python once ([vobject](https://github.com/eventable/vobject))
but it's development has stalled many years ago.
So I wrote a basic parser myself and it was actually quite fun.

I am looking forward to continuing with this project.
