---
title: "Bullerbü ist ein tolles erstes Buch"
date: 2024-10-09T06:52:44+02:00
draft: false
image: schwedischer_hof.jpg
---

<!-- image link: https://unsplash.com/photos/brown-and-white-wooden-house-near-green-trees-during-daytime-5_gr244ZlY0 -->
<!-- image author: Anatoliy Gromov -->

Ich liebe es, meinen Kindern vorzulesen.
Die ersten Jahre funktionieren nur kurze Bilderbücher, die man in einem Rutsch durch bekommt.
Aber bei 3 Jahren habe ich vorsichtig angefangen, längere Geschichten mit wenig Bildern für sie zu lesen, mit mehreren Kapiteln.
Und das erste Buch war "[Wir Kinder von Bullerbü](https://de.wikipedia.org/wiki/Wir_Kinder_aus_Bullerb%C3%BC#/media/Datei:Bullerbyn-dsc_2725.jpg)".

Ich hatte das lange vorher geplant und bin im Nachhinein sehr glücklich mit dieser Wahl.

Bullerbü ist fantastisch:
- Die Sprache ist kindgerecht und zieht auch mich als Erwachsenen an.
- Die Kapitel sind kurz (etwa 3 Doppelseiten).
- Die Kapitel bauen lose aufeinander auf, sind aber in sich geschlossen.
- Die Themen sind Altagsabenteuer von Kindern.
- Die Geschichten spielen in einer ziemlich heilen Welt.

Trotzdem ist das natürlich anstrengend für kleine Kinder und ich muss viel unterstützen.
- Nach (und manchmal auch wärend) einer Geschichte besprechen wir, was passiert ist.
- Nach jedem Kapitel darf das Kind entscheiden, ob es noch eines will oder nicht.
- Oft müssen Begriffe erklärt werden ("Rüben verziehen", "Hausmädchen", "Landstreicher" ...).
  Meistens kann man aber auch einfach darüber hinweg lesen - man braucht nicht alle Details, um Geschichten zu verstehen.
- Bullerbü spielt so vor 120 Jahren (Anfang des 20. Jahrhundersts) und wurde vor über 70 Jahren geschrieben.
  Dafür ist die Sprache nicht schlecht gealtert, aber manches klingt schon seltsam für moderne Ohren.
  Manchmal übersetze ich beim vorlesen direkt (zum Beispiel "es ist mir über" zu "es ist mir zu viel").
- Einige Kapitel sind ein auserzählter Witz, den das Kind nicht überblicken kann.
  <!-- Ich achte auf die Reaktion des Kindes und wenn sie es nicht versteht, dann fasse ich es nochmal zusammen: -->
  ("Die Kinder haben sich so Mühe gegeben, sich die Einkaufsliste zu merken. Aber immer vergessen sie was.")
- Ich darf nicht enttäuscht sein, wenn das Kind sich direkt nach der Geschichte an nichts mehr erinnert (oder nichts wiedergeben kann).
  Das wird schon, beim 3. oder 4. mal lesen.
  Manchmal passiert auch das Gegenteil und 2 Tage später benutzt das Kind einen Satz aus dem Buch. 
- Wenn das Kind Angst hat (oft an Stellen an denen das nun wirklich überhaupt keinen Sinn macht) dann erzähle ich was passieren wird und manchmal überspringen wir dann die Stelle oder das ganze Kapitel.

Manchmal (aber zu selten) spielen wir auch Szenen nach.
Das geht dann meist in freies Spielen über und ist ein guter Abschluss für die Leserunde.

