---
title: changelog
---

I do a lot of  personal computerstuff over the weeks.
Most of it ends up in a repository of some sort (if it is not in a repo, have you even done something?)
A few things end up in a database, which is fine, too.

But I don't write about most of it.
Apart from the git log there is no changelog, no releases (everything is released immediately) and no Retro.

Thats mostly OK.
Most stuff I do is small and not aimed at the public
(even when I do it in the open,
[almost all of my code is on gitlab](https://gitlab.com/Kaligule)).
So if I do a commit or two every month there is not much to tell.

But wouldn't it be nice to have some document of accomplishment every once in a while? An opportunity for retrospection? Lets try that.

So here is what I did in these days:

## talks

In the last weeks I have written a
[website for the talks I have held](https://talks.schauderbasis.de/).
It was only the second site that I wrote completely from scratch.
I am quite proud of it.

I used [hugo](https://gohugo.io/) as a site generator,
but wrote the theme completely myself.
I had an easier time figuring out the hugo templates, the HTML and the css.
[MDN](https://developer.mozilla.org) is a really good resource.

The most difficult part was getting the css work for both mobile and desktop.

This was a big project for me.

There is still a lot to do content wise
(writing transcripts,
asking the rightholders for permission to distribute recordings of talks)
but I am comfortable having the site online already.

## elfeed

[Elfeed](https://github.com/skeeto/elfeed) is a Feedreader for Emacs.
It has a great reputation within the community.
It fetches the feeds itself by default,
but with elfeed-protocol it can sync with different servers like [Newsblur](https://newsblur.com/) or [ttrss](https://tt-rss.org/n).
[Elfeed-protocol](https://github.com/fasheng/elfeed-protocol)
has seen some critical improvements over the last few weeks,
so I tried it out once more.

Elfeed sits in the intersection of 2 things I love:
emacs and RSS.
It's gorgeous and I would love to use it,
but when I first tried it in 2020 it missed some (for me critical) features.
Those were resolved, some of them just a few weeks ago.

But I am not sure the sync of the read-status works reliably.
I will have to try it some more.
I am also in the in the process of reading through the code.
This is bit hard for me because (e)lisp is still a bit hard for me to read.
