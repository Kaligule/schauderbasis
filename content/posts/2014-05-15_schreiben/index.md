---
title: "Schreiben"
---

	Wirtschaftsprofessor : "Also wenn Sie einen Text erstellen wollen, dann kommen Sie an den Microsoft Office Paketen nicht vorbei."
    
    Ach so ist das.


Als wir diesen Post in einem sozialen Netzwerk sahen mussten wir alle sehr lachen: Diese Wirtschaftler in ihrer kleinen Microsoft-Welt. So süß.

Aber gerade weil diese Aussage aus einem akademischen Umfeld kommt, möchte ich diese Aussage auf keinen Fall so stehen lassen. Darum hier eine (sehr kleine) Auswahl aus dem Großen Feld der Programme, mit denen man hervoragend "einen Text schreiben" kann.

## Latex

Die klare Nummer 1. Mit Latex wird im Naturwissenschaftlichen Umfeld alles geschrieben, was auch nur Halbwegs professionell wirken soll: Wissenschaftliche Arbeiten, Skripte, Handouts, Beamerfolien...

### Formeln

Die größte Stärke von Latex sind eindeutig Formeln - in keinem mir bekannten Programm werden diese auch nur annährend so gut unterstützt. Nicht nur wird jedes Zeichen ([und sei es auch noch so absurd](https://www.tex.ac.uk/tex-archive/info/symbols/comprehensive/symbols-a4.pdf)) unterstützt, die Formeln werden auch ordentlich und sauber dargestellt. Beispiele findet man unter anderen [hier](https://www.mathjax.org/demos/tex-samples/). Man ist nicht auf einen abstrusen Formeleditor angewiesen, sondern kann sie direkt auf der Tastatur tippen. Und sie sehen immer gut aus.

### Dokumentenklassen

Da Latex wirklich weit verbreitet ist, gibt es eine große Anzahl von Vorlagen für alle möglichen Zwecke. Beispiele findet man [hier](https://www.latextemplates.com/).
Weil für Beamerfolien und Skripte die gleiche Quelle verwendet werden kann sind diese  auch wunderbar kompatibel. Man muss in erster Linie die Dokumentenklasse ändern (zum Beispiel von ```article``` auf ```beamer```).

### Trennung von Inhalt und Format

Texte schreiben ist Arbeit. Es ist schwierig, braucht Konzentration und man muss viel nachdenken.

Dinge auf die ich beim schreiben achten will:

* mein Text
* meine Quellen

Dinge auf die ich beim schreiben _nicht_ achten will:

* Zeilenabstände
* Schriftgröße
* Schriftart
* Rechtbündige, Linkbündig, Mittig, Blocksatz
* Worttrennung, Zeilen und Seitenumbrüche
* Header und Footer
* Textbreite und Korrekturrand

Deswegen ist in Latex der Inhalt von der Formatierung getrennt. So kann man sich auf das konzentrieren, was wichtig ist.

### Verfügbarkeit

Latex ist nicht nur schon recht alt sondern auch [Opensource](https://en.wikipedia.org/wiki/Open-source_software) und kann auf allen denkbaren Systemen installiert werden. Das schließt Windows, MacOS X, Linux, Bsd und Solaris ein, aber aber auch Mobile Systeme (Android, iOS ...). Wer eine Wissenschaftliche Arbeit auf seinem Tablet oder gar Telefon schreiben will sollte zwar vielleicht nochmal über das Wort "Produktivität" nachdenken, aber möglich ist es.

### Einfach drucken

Machen sie mal einen eifachen Test: Gehen sie in einen Copy-Shop (die Läden, wo man seine Arbeiten drucken und binden lassen kann) und fragen sie nach, welche pdfs mehr Probleme machen: Die mit MS-Word erstellten oder die mit Latex.
Microsoft hält sich nicht (nur so halb) an den PDF-Standard. Latex schon. Das merkt man zum Beispiel beim drucken, wo (auch explizit Windows-kompatible) Drucker regelmäßig scheitern.
Die Open Source Gemeinde dagegen ist gut im Einhalten von Standards, denn eine wichtige [Unix-Grundregel](https://en.wikipedia.org/wiki/Unix_philosophy#McIlroy:_A_Quarter_Century_of_Unix) lautet: "Programme sollen gut zusammen arbeiten können."

### Lernkurve

Die Lernkurve bei Latex ohne Frage steiler als bei anderen Programmen (das Stichwort heißt hier: [What You See Is What You Mean](https://de.wikipedia.org/wiki/WYSIWYM)).
![WYSIWYM ist nicht selbstverständlich. Quelle: https://xkcd.com/1341/]( https://imgs.xkcd.com/comics/types_of_editors.png )
(Bildquelle: https://xkcd.com/1341/)
Wem das zu schwierig ist, der sei auf [Lyx](https://www.lyx.org/) verwiesen, einem Editor der vielleicht mehr den Maßstäben eines Word-Anhängers genügt, aber dennoch einwandfreien Latex-Code erzeugt. Lyx ist übrigends auch der Editor meiner Wahl (zumindest wenn es um irgendetwas geht was jemand anderes nochmal lesen soll).

### Austesten

Wer keine Lust hat sich was zu installieren kann Latex auch einfach mal [im Browser testen](https://www.sharelatex.com/), inzwischen sogar collaborativ.

---

All das führt dazu, dass (natur-) wissenschaftliche Arbeiten die nicht in Latex geschrieben sind oft sehr schräg von der Seite angeschaut werden.

## Markdown

Markdown besticht durch Einfachheit. Man sucht sich irgendeinen Editor, tippt einfach drauf los und es kommt das heraus, was man meint. (TODO:Format Überprüfen) Einfache Beispiele:

* __Aufzählungen:__ für einfache Aufzählunen wie diese hier
	
    * bli
    * bla
    * blub

	tippt man einfach
	
```
* bli 
* bla 
* blub
```
    
* __Überschriften:__ Man tippt ```# Kartoffelsalat``` und bekommt eine Überschrift, schön groß und so.
* __Hyperlinks:__
	Keine Rechtsklick und seltsame Menüs - stat dessen tippt man mitten im Text ```[lalala](www.google.de)``` und bekommt [lalala](www.google.de). Gerade diese Syntax für Links ist gerad drauf und dran der de fakto (TODO: Prüfen) Standard in allen möglichen Bereichen zu werden.

Wenn man dann seinen Text fertig hat kann man ihn mit einem der diversen Programme dafür in Pdf, Html oder sonst ein Format umwandeln lassen. Alle Inhalte dieses Blogs sind zum Beispiel in Markdown geschrieben. Und wer Formeln mag kann sie mittels [MathJax](https://www.mathjax.org/) auch im Latex-Format tippen.

Die Vorteile sind bestechend:

* Lernkurve: Flach, sehr flach. Eigentlich hat jeder schon mal eine Liste mit [Asterisken](https://en.wikipedia.org/wiki/Asterisk) (so heißten diese kleinen Sterne: * ) markiert.
* Schreibgeschwindigkeit: kein herumgeklicke und gesuche in komplizierten Menüs - man muss die Hände nicht von der Tastatur nehmen
* Vielfalt: Markdown kann in ziemlich jedes Format konvertieren
* Verbreitung: Es gibt eine Vielzahl von Editoren, die sich besonders gut für Markdown eignen (mit live-Vorschau, Formatierungshilfe ect). Ich empfehle hier mal [Haroopad](https://pad.haroopress.com/user.html), der ist open Source und für alle Desktop-Betriebsysteme
* Mobil: Wegen der großen Einfachheit macht Markdown übrigends auch für Tablets und Telefone Sinn, passende Apps gibt es wie Sand am Meer. 

## Libre Office / Open Office

Ok, ich gebs zu: Latex und Markdown sind eigentlich keine Programme sondern Standards (sogenannte [Markaup languages](https://de.wikipedia.org/wiki/Auszeichnungssprache)), die in allen Möglichen Editoren geschrieben werden können.
Wer sich aber gar nicht vom Konzept der Officepakete losreißen kann sei auf Libre Office und Open Office verwiesen. Mit jedem dieser Pakete (die miteinander nur wenig zu tun haben) bekommt man einen Ersatz für Word, Excel und Powerpoint (Für den Mac gibts dann noch iWork). Der benutzte OpenDocument Standard ist teilweise auch kompatibel mit den Formaten die Microsoft verwendet.
Ich benutze sie selbst nicht aber ausprobieren kostet ja nichts. Hier sind die Links: (Libre Office)[https://de.libreoffice.org/] & (Open Office)[https://www.openoffice.org/de/]

## irgendein Editor

Dir gefallen die alle nicht? Dann nimm doch einfach _irgendeinen Editor_. Es gibt ja wahrhaftig genug. George R. R. Martin, der Autor der Vorlage der Serie "Game of Thrones" schreibt [nach eigener Aussage](https://www.youtube.com/watch?v=X5REM-3nWHg) zum Beispiel in [WordStar 4.0](https://de.wikipedia.org/wiki/Wordstar), einem Editor von 1987.
Wer auf viele Features steht kann sich ja mal mit [vi](https://de.wikipedia.org/wiki/Vi), [vim](https://de.wikipedia.org/wiki/Vim) und [emacs](https://de.wikipedia.org/wiki/Emacs) beschäftigen.

## Und warum nicht einfach Microsoft Office?

Die vorgestellten Programme haben alle (bis auf WordStar) eine Gemeinsamkeit: Sie sind Open Source und basieren auf freien und offenen Standards. Das ist bei MSOffice nicht der Fall. Das hat mehrere wichtige Folgen:

* __Cross Platform:__ (TODO:Rechtschreibung) Man kann sie auf jeder Platform (Windows, MacOS X, Linux...) verwenden.
* __Kostenlos:__ Man kann sie einfach runterladen und ausprobieren. Keine Lizenzkeys, keine Trialversionen. Man bekommt das volle Paket, einfach so. Eine Private Lizenz von Office (für genau 1 Computer) [kostet zur Zeit 70€](https://office.microsoft.com/de-de/microsoft-office-2013-suiten-und-office-365-abonnements-kaufen-FX102886268.aspx). 
* __Frei:__ Wissen muss frei sein, deswegen haben wir unabhängige Schulen und Universitäten. Für mich macht es Sinn dieses Wissen auch so abzulegen, dass es frei verfügbar ist - auch wenn man seine Soft- und Hardware nicht von Firmen aus den USA lizensieren lassen möchte. 

Ich finde dass sich die Aussage, man käme um die Microsoft Office Pakete nicht herum, nicht halten lässt.