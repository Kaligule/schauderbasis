---
title: "Don't be a cynic"
image: "dark_room.jpg"
---

<!--
image source: https://unsplash.com/photos/a-bust-of-a-man-in-a-dark-room-jkM80OMAwII
Photographer: Vinay Gujar
License: Unsplash
-->

I was a cynic for most of my teenage years.
One day my drama teacher labeled this behavior:
"Ahhh, you are a cynic."
That one sentence made me reflect and realise that I didn't really like that about myself.
So I stopped.for a long time in school

This turned out to be relevant to a lot of discussions.
Sometimes I discuss a difficult problem with someone and they say something like this:

> I put my personal information on all the platforms.

> Yes, smoking kills, but we all have to die one day.

> Climate change is here anyway.

To me, these sentences feel like an attempt to escape the problem.
Make a joke about it and keep doing what you know is bad.

In a group discussion, people always laugh at these sentences.
But I think it is not a happy laugh.
They laugh not because it is funny, but because they share the helplessness.

I found that a good response is:

> Right, but I don't want to be cynical about it.
> This is a real problem that deserves a real solution.
> So what can we do about it?

As a result, many people tend to get much more serious.
Often they think a bit and then say:

> I know it is problematic, but right now the good outweighs the bad for me.
> So I'm going to keep doing it.

Which is a much more helpful way to think about things in my opinion.

And even if there is nothing we can do about a problem, then I will talk about that.
Don't make a joke of it.
That's what the Joker would do.
And I don't want to be a cynic like the Joker.
