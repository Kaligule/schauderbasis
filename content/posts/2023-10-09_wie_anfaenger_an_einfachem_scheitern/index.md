---
title: Wie Anfänger an Einfachem scheitern
image: snakes_and_ladders.jpg
---
<!--
Source: href="https://unsplash.com/photos/O5Fim95WhFI
License: Unsplash
Photographer: VD Photography
Photographer link: https://unsplash.com/@vdphotography
-->

"Snakes and ladders" ist ein so einfaches Spiel, dass es fast keine Regeln zu erklären gibt:

> "Würfeln, laufen und wer als erstes ins Ziel kommt gewinnt. Wer auf einem markierten Feld landet kommt woanders raus."

Ich wüsste wirklich nicht was man mehr erklären sollte.

Im eigentlichen Sinn ist es nicht einmal ein Spiel:
Es gibt keine Entscheidungen, welche die Spieler treffen können,
keine Fähigkeiten werden unter Beweis gestellt - man würfelt und zieht.
Es ist mehr wie ein Protokoll, das abgearbeitet wird, um einen zufälligen Sieger zu bestimmen.
Kaum mehr als ein verkomplizierter Münzwurf.

Aber wer versucht, es mit einem kleinen Kind zu spielen,
der kann sehen wie viel implizites Wissen hier vorausgesetzt wird.

Hier ist eine Reihe von Schwierigkeiten, die ich beim Spielen mit kleinen Kindern hatte.
(Um die Situation zu vereinfachen hatten wir auf alle Schlangen verzichtet und das Spielbrett sorgsam aufgebaut.)

- Sie wissen nicht, wer als nächstes dran ist.
- Sie wissen nicht, sie man würfelt.
- Der Würfel ist runter gefallen und unterm Sofa verschwunden.
- Sie wissen nicht, was die Zeichen auf dem Würfel bedeuten.
- Sie wollen lieber mit dem Würfel selbst spielen.
- Sie wissen nicht das jeder Spieler nur eine Spielfigur hat
- ... und diese nicht mitten im Spiel mit der eines anderen tauschen darf.
- Sie wissen nicht, wie weit sie laufen dürfen.
- Sie laufen in die falsche Richtung.
- Sie können sich nicht auf das Spiel konzentrieren, nicht mal während sie selbst dran sind.
- Die Spielfiguren sind verrutscht und keiner weiß mehr wo sie waren.
- Sie wissen nicht mehr welches ihre Spielfigur ist.
- Sie wollen nicht warten bis der Vorgänger mit seinem Zug fertig ist.
- Sie wissen nicht, wann das Spiel zu Ende ist.
- Sie wissen *sehr* genau, wer gewonnen hat.

Ich schreibe das alles voller Respekt.
Ich weiß sehr genau was es bedeutet, ein Anfänger zu sein.

Das ist einer der Gründe, warum gute Dokumentation zu schreiben unglaublich ist:
Es ist sooo schwierig vorherzusagen, wie viel der Leser schon weiß.
