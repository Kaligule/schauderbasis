---
draft: true
title: "Warteschleifen"
image: old_phone.jpg
---

<!--
image source: https://unsplash.com/photos/-0xCCPIbl3M
Photographer: Pawel Czerwinski
license: Unsplash
-->

In den letzten Wochen hatte ich das zweifelhafte Vergnügen,
bei verschiedenen deutschen Firmen den Kundensupport anzurufen.
In vielen Fällen mehrfach über meherere/viele Tage hinweg.
Und immer immer hängt man in der Warteschleife, bevor man jemanden erreicht.

<!-- TODO Kundensupport anschluesse sind inzwischen immer umsonst, oder? -->

Da gibt es 3 Phasen:

1. Automatisches Aufnehmen von Informationen
2. Warten
3. Beratung

Ich möchte davon erzählen, was für mich gut funktioniert hat und was mehr genervt hat.
Alles ist aus sicht eines Anrufers, ich habe absolut keine Insider-Informationen.

## Automatisches Aufnehmen von Informationen

TODO
Das scheint inzwischen bei e

## Warten

Der Anrufer wartet nun also auf einen freien Supportmitarbeiter.

### Musik

Erstmal darf man sich Musik anhören.
Jede Firma hat ein anderes Musikstück, dass sie dann endlos wiederholen.
Warum eigentlich?
Wäre es so ein Problem, verschiedene Lieder zu haben?

Die Abspielqualität ist immer schlecht.
Das liegt daran, dass viele Teile des Systems
[für Stimmen optimiert wurden und nicht für Musik](https://www.youtube.com/watch?v=w2A8q3XIhu0).

Zwischendurch bekommt man immer wieder automatische Durchsagen.
Davon gibt es 3 Sorten.

### nicht hilfreich: ermutigende Durchsagen

Ein Computer versichert, es ginge _gleich_ weiter.
Halten sie durch und legen sie nicht auf.

Im schlechten Fall sind es generische Hinhaltesätze, bei denen man sich verarscht vorkommt
(spätestens wenn sie sich wiederholen):

- Wir sind gleich für sie da.
- Der nächste freie Mitarbeiter ist gleich für sie da.
- Es dauert noch einen Moment. Wir danken für ihr Verständniss.

### nicht hilfreich: entmutigende Durchsagen

Paradoxerweise gibt es andere Durchsagen, die den Anrufer zum Aufgeben/Auflegen zu bewegen sollen.
Man möge doch lieber ein Supportangebot nutzen,
das leichter automatisiert/verwalten/liegengelassen werden kann?
Oder sich die Antworten am besten gleich selbst aus dem Internet raussuchen.

- Wussten sie schon? Auf unserer Website finden sie viele nützlicher Informationen.
- Haben sie schon unser Kundenforum besucht?

### hilfreich: informative Durchsagen

Damit hatte ich nicht gerechnet:
Beim Support von [avm](https://avm.de) wird ein Zwischenstand durchgegeben,
wie lange die Warteschlange noch ist.
Die wird genau dann eingespielt, wenn sich die Länge der Warteschlange ändert.

- Es sind noch **4** Kunden vor ihnen an der Reihe.

Das war sooo hilfreich.
Es wurde eine *echte* Information übermittelt,
mit der ich abschätzen konnte, wie lange es noch dauern würde.
Da fühlte ich mich ernst genommen.

## Fazit

Es hilft, wenn der Anrufer das Gefühl bekommt, echte Informationen zu bekommen.

