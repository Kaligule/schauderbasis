---
title: "Emacs lernen, Tag 2"
---

## Logbuch, Tag 2

* Starte Emacs mit ```emacs -nw``` wie gestern gelernt.
* Starte einen zweiten emacs, um das Logbuch mit zu tippen. Wie bekomme ich jetzt eine neue Datei? Hab keine Ahnung. Behelfe mich schließlich mit ```emacs -nw Blogeintrag.mdown```. Das geht noch besser.
* Weil die Pfeiltasten noch abgeklebt sind muss ich die Shortcuts benutzen um hin und her zu springen. Juhu!
* Bevor ich loslege sollte ich wahrscheinlich lernen, wie man eine Textdatei speichert. Sonst gibts nachher Tränen.
* Speichern geht mit ```C-x C-s```
* Der erste Hammer des Tages: Paste ist nicht, wie erwartet ```C-v``` sondern ```C-y``` für __y__ank (großzügig übersetzt mit zurückreißen).
* Yank ist komplizierter als gedacht.
* Der Shortcut für _undo_ (```C-\```macht auf amerikanischen Tastaturen mehr Sinn, weil dort das ```\``` direkt unter dem Delete Key ist. Mist - ich habe eine Deutsche Tastatur an meinem Laptop und daran wird sich so bald auch nichts ändern.
* Lerne ich lieber ```C-_``` dafür, da muss man sich nicht so verrenken.
* Undo wird erklärt, aber wie geht _redo_? Wird hier nicht erklärt, hätte ich aber gerne.
* Warum ist _undo_ eigentlich nicht ```C-z```? __AAAAAAAAAAAAAARrrrrrrgh!__ Ich wollte es doch nur ausprobieren, nicht den Emacs schließen! Nein! und das letzte mal, dass ich das Logbuch gespeichert habe ist ewig her.
* Als ich die Datei wieder öffne und beginnen will, das verlorene wieder einzutippen, erhalte ich eine seltsame Nachricht:
	![](Hoffnung.png)
* Gibt es noch Hoffnung für meine Änderungen? Ich tippe ```s``` und der Dialog verschwindet - ohne die erwünschte Wirkung. Mist, das wäre mal ein Feature gewesen das ich brauchen kann. (später lerne ich: Hätte man ins Terminal einfach fg getippt wäre alles ok gewesen. Tja)
* Viel zwischenspeichern ab jetzt. ```C-x C-s``` ```C-x C-s``` ```C-x C-s``` ```C-x C-s```
* Ein Bisschen Theorie über Buffer. Alles wo Text drin steht ist ein Buffer.
* Man kann Buffer scheinbar wie Tabs benutzen, nur ohne die Tableiste. Dafür mit ```C-x b``` und dann den Namen von dem Tab eingeben.
* Ein File öffnen geht mit ```C-x C-f```. Das steht angeblich für "_f_ind file", aber ich merke mir einfach _f_ile.
* Mittagspause
* Mir wird erklärt, dass die Datei vorhin nicht wirklcih weg war, sondern "nur in den Hintergrund gerückt". ```C-z``` ist auch eigentlich kein Emacs-Befehl sondern geht direkt an das Terminal, in dem der Emacs läuft. Ich habe noch keinen Überblick über die vielen Schichten der Komplexen Programme, die hier laufen.
* Es gibt eine Art Notificationcenter (eigentlich ein Buffer), wo alle eingegangenen Nachrichten (in der unteren Zeile aufgeführt werden. Langsam wird mir klar, warum Leute Emacs als Betriebssystem verstehen.
* Nach dem ich 57% des Tutorials durchlaufen habe wird erklärt, wie man Emacs beendet. Ich habe es ja schon schmerzhaft selbst herausgefunden.
* Die Echo Area (letzte Zeile, wo die Tastencombos angezeigt werden die man bereits getippt hat) hat einen eingebauten Delay. Das finde ich schlecht, sobald ich weiß wie hier alles läuft mache ich den Weg.
* Zwischendurch was trinken, ist ja auch wichtig.
* Jetzt wird es spannend. Es gibt Modes. Für verschiedene Programmiersprachen verwendet man verschiedene Modi, die Dinge verändern (zum Beispiel, wie ein Kommentar aussieht oder was ein Paragraph ist)
* Es gibt Major-Modi und Minor-Modi. Man kann nur einen Major- aber mehrere Minor-Modi gleichzeitig benutzen.
* Ich wechsle sofort vom Fundamental-(Major-)Mode in den Text-(Major-)Mode.
* Fühle mich gleich besser. Es ist gut, wenn der Editor weiß, was ich tue (wenn ich es schon nicht immer weiß)
* Der Minor Mode Autofill nervt eher. Was ich nicht mag, wird nicht benutzt. Ha!
* Ich habe das Gefühl, ein großer Teil der Flexibilität von Emacs wird aus diesen Modes kommen.
* Mein Kopf brummt. Gut dass heute nicht viel los ist.
* Weiter machen: Searching
* Die Incremental search ist genau das, was bei Sublimetext das ```Strg-I``` macht. Nur nicht fuzzy.
* fuzzy search wird überhaupt immernoch viel zu sehr unterschätzt, außer Sublimetext macht das kaum einer richtig.
* Mehrere Frames in einem Fenster. Den Teil überfliege ich nur, von sowas bin ich kein großer Freund. Schön, dass es geht.
* Shortcut des Tages: `<Esc> <Esc> <Esc>` als ein _all-purpose "get out" command_. Das ist gut, denn das drückt man sowieso meistens ganz panisch.

## Bilanz, Tag 2

* Diesen Text habe ich im Emacs getippt, Yeah! (und es ist mir nur einmal schief gegangen.
* Das Tutorium habe ich zu 90% durch, sehr gut. Morgen suche ich mir was anderes zum Üben.
* Leider hab ich, als es etwas zu Programmieren gab noch Schwierigkeiten gehabt und dann doch schnell Sublimetext genommen. Ein Moralischer Fehltritt.
* Vielleicht suche ich mir morgen ein paar Modes heraus, die mich beim Schreiben wirklich unterstützen - heute habe ich von ihnen kaum was gemerkt.

Ich freu mich auf morgen.
