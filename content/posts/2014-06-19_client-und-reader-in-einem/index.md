---
title: "Client und Reader in einem"
---

Nachtrag zur [RSS Software](https://www.schauderbasis.de/rss-software/):

![Pritlove, dessen Frueher noch frueher ist als meines](Pritlove-1.png)

Na gut, ich sehe es ja ein...

## ... ich hab was vergessen.

Wer nur ein Gerät hat auf dem er Nachrichten ließt oder wem Sync allgemein auf den Keks geht oder keine Lust hat irgendwas in der Cloud zu machen oder was weiß ich - der kann auch einfach alles in einem Programm haben. Man installiere dazu einen Newsreader auf einem seiner Geräte.

### Der Workflow

* Ich finde eine interessante Website im Internet und aboniere sie in meinem Newsreader. Ab jetzt beobachtet mein Feedreader die Website.
* Ein Autor schreibt einen Artikel auf der Website.
* Wenn ich meinen Feedreader öffne erkennt er den neuen Inhalt, befreit ihn von dem Websitedesign (so gut wie möglich) und legt ihn zum lesen bereit.
* Ich lese die neuen Artikel von verschiedenen Websiten, alle in einem Programm gebündelt.

Der Zwischenschritt über den RSS Reader fällt also weg.

### Vorteile

* Die Feeds liegen nicht in der Cloud
* Ich hab alles an einer Stelle und muss nicht hoffen, dass alle Programme gut aufeinander abgestimmt sind (oder evtl sogar mit Plugins nachhelfen)
* Die Einstiegshürde ist niedriger.

### Nachteile

* Kein Sync. Sobald ich auf mehreren Geräten gleichzeitig lesen will, funktioniert es nicht mehr.
* Der Newsreader muss jede abonierte Website selbst besuchen (und verbraucht dabei evtl wertvolles mobiles Datenvolumen). Er kann sich nicht einfach mit einem (!) Server verbinden, der das schon für ihn gemacht hat.

Zusammen führt das dazu, dass man (zumindest wenn man ab und zu auf dem Telefon Feeds lesen will) sich lieber nach einer Serverseitigen Lösung umsehen sollte. (Meine Meinung)

### Clients

Wer das ganze mal testen will:

* __[NetNewsWire](https://www.netnewswireapp.com):__ für OSX, [freundliche Empfehlung](https://alpha.app.net/timpritlove/post/32900911) von [Tim Pritlove](https://tim.pritlove.org/) (der nach [eigener Aussage](https://alpha.app.net/timpritlove/post/32905276) zur Zeit kein RSS benutzt), sehr schick und wird gerade neu geschrieben (es gibt ne public Beta!), fühlt sich dabei sehr durchdacht an. NNW ist ein echtes Urgestein ([seit 2002 dabei](https://en.wikipedia.org/wiki/NetNewsWire)) und wird immernoch/wieder aktiv weiterentwickelt. Respekt. ![Support bis in alle Ewigkeit](NetNewsWire.png)
* __[Reeder](https://www.reederapp.com/ios/):__ für iOS kann das auch. Spricht ja auch nichts dagegen, dass auch noch zu unterstützem - ich finde es auf dem Telefon halt nur halbgar
* __Opera & Firefox:__ Browser können auch RSS Feeds verarbeiten. Es macht irgendwie Sinn, soetwas in den Browser einzuarbeiten, aber ich konnte mich nie damit anfreunden. Danke an [Fernsehmüll](https://alpha.app.net/fernsehmuell) für den [Tipp](https://alpha.app.net/fernsehmuell/post/32906426)

# Noch mehr Software

Meine Blogartikel erhaben eigentlich nie den Anspruch auf Vollständigkeit. Feedback ist natürlich sehr willkommen (wirklich, ich freue mich dann).

Eine sehr viel längere Liste für RSS-Software findet ihr [hier](https://en.wikipedia.org/wiki/List_of_feed_aggregators).
