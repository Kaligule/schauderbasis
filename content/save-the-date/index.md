---
date: 2017-06-10T11:02:16
expiryDate: 2018-07-14T23:59:59
draft: false
title: "Save the Date"
image: tauben.jpg
---

<!-- https://pixabay.com/de/photos/vogel-paar-schnabel-paris-2590901/ -->

Wir wagen endlich den nächsten Schritt.

Am Samstag den **14.07.2018** werden wir  in Nürnberg heiraten und möchten mit euch feiern. Bitte tragt das Datum in eure Kalender ein und seid dabei. 
Wir freuen uns auf euch!

Einladungen folgen...

Johannes & Sandra
