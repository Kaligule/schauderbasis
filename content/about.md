---
title: Über mich
---

Ich heiße Johannes Lippmann und finde Mathematik und freie Software spannend.

Die [Schauderbasis](https://schauderbasis.de) ist mein Blog.
Mehr über meine Arbeit findet man auf [https://cv.schauderbasis.de](https://cv.schauderbasis.de).
