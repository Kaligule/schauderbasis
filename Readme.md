# Personal Blog of Johannes Lippmann

Find it online at [schauderbasis.de](schauderbasis.de).

## Deployment

```mermaid
graph LR
  A[Markdown files, image files in git] -->| git push | B(Gitlab repo)
  B -->|gitlab-ci and hugo| C(website in directory)
  C -->|gitlab-ci and lftp| D(Netcup Webhosting)
```

## Theme

Themes are added as git submodules. If you just cloned the repository
and the themes directory is empty you should run the following:

```sh
git submodule init
git submodule update
```
